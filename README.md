# README #

### What is this repository for? ###

This repository is made for internal bookkeeping for the master thesis with the title:
"Dispatching and routing of automated guided vehicles".

Students: Diana Hooper & Thor Bendix

Supervisor: Marco Chiarandini

### Structure of repostory ###

Doc: Anything related to the report

Literature: Some of the literature used in making of this thesis

Src: source code for the project.



### Src structure ###

### Greedy 

root directory: src/Heuristic/Greedy

solver.cpp - main file

instance.cpp - instance representation

model.cpp - greedy model representation 

solution.cpp - solution representation

instances - contains all offline test instances



### CP

root directory: src/CP

/binpacking/bin-packing-propagation - binpacking model

/CP/instance.cpp - instance representation

/CP/model.cpp - CP model

/CP/solution.cpp - represents a solution

/CP/solver.cpp - main file for CP

instances - contains all offline test instances

In order to run the CP model, the Gecode binaries are needed, they can be downloaded from:

http://www.gecode.org/download.html



### InsertionHeuristic

root directory: src/Heuristic/InsertionHeuristic

main.cpp - main file for Insertion Heuristic

iTask.cpp - instance task representation

sTask.cpp - solution task representation

iVehicle.cpp - instance vehicle representation

sVehicle.cpp - solution vehicle representation

solution.cpp - solution representation

instance.cpp - instance representation

heuristic.cpp - heuristics reprenstaton

instances - contains all offline test instances



### MILP / SPH

root directory: src/MIP

-bp_main - Main file for SPH

-bp_preprocessing - contains the separation algorithm and the schedule generation phase

-bp_model - contains the set partitioning model for SPH

-rcpsp_newcallablelin - Main file for MILP, containing everything from model to outputters

-simulation - Main simulation file

-helperclasses and bp_helpers - Contain different classes and helper functions

-finstance and instance - contain instance reading and instance creation and processing

instances -Contains all offline and online instance files

Simulation Tests

-Simulation ouput

-Simulation plots

-Plot scripts

sph_tests

-offline test output for SPH

-plots for offline tests

tikz

-Various tikz output from tests