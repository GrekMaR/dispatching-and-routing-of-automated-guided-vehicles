Slides Jeg skal tage:
Problemformulation, slide 3-6,8
Methods, CP + IH

Notes:
1)
Hey and welcome to the defense of the master thesis with the title "Dispatching and Routing of Automated Guided Vehicles".

2)
We'll start by presenting the problem formulation of the thesis, then present what work already exist with regards to the problem.

Afterwards we will cover the methods we have worked with,
talk about the testing made with the methods as well as the results and sum up the work done as well as conclude on the project.

3)
Mobile industrial robots (MIR) is developing a fleet of Automated Guide Vehicles (AGV) for operation in a hospital environment, more specificly at Aabenraa hospital.
The tasks of the AGvs are transportation tasks of items such as Garbage, linen and misc.
We have around 200 linen and 400-500 Garbage collection tasks during a day.
All garbage tasks are released at midnight and must be done by 8:00.
Whereas linen and miscellanous tasks occur throughout the day.
The figure shows an example of a section at Aabenraa hospital where the depot is on the right and the left side has different rooms and garbage and linen tasks could occur here.

Aabenraa hospital has multiple floors, but we only consider a single floor.
The hallway also allow for bidirectional movement, so congestion is not an issue between AGVs.

4)
The fleet of AGVS all have a local CPU and asensors to perform routing, avoid obstacles and choose paths on their own.
They have a battery which needs to be recharged once in a while, thus each vehicle requires the scheduling of maintenance tasks in form of recharging.
This problem resembles the resource constrainted project scheduling problem with task order dependant setup times (RCPSP-ST)
The picture shows the MiR100 AGV developed by MIR.

5)
The scheduling problem can be formulated as follows:
Given a set of n tasks and m vehicles, produce a schedule detailing which performs what task and when.
We maximize throughput of tasks by minimizing the makspan.
routing is repesented as setup time beteen tasks.
The tasks has to be processed between their arrival time, and their deadlines.
The vehicles have resources repesented by battery capacity and recharge stations.
A solution should containt the task assignments for each AGV as well as start time and end time for the jobs. This also applies for the maintenaince tasks.

The scheduler that tries to do this assignment can be offline, online or dynamic.
Offline - all tasks are given at the start for scheduling once.
online- tasks may enter the system for schedling. but tasks alredaay scheduled may not be moved.
dynamic - same as online, but we can reschedule tassk which have not been processed yet.

6)
MIR currently uses a greedy approach, which we will cover in more detail later.
So our goal for this thesis were to design a method which performs better than their current greedy scheduler and is more robust.
Better is with runtime, objective value and feasibility.
This is due to the fact that the greedy approach cannot always produce a feasible solution.
We also then compare the performance of these methods using offline and online (simluation)/(dynamic) tests.

7) DIANA

8)
Here we see the greedy algorithm used by MIR, or at least our implementation which is based upon it.
it is developed by Novotek A/S, and made by some former IMADA students if I recall correcctly.
This approach is fast, but at times fails to dynamically schedule all tasks.

The algorithm function as follows:
While we have not schedueld all tasks,
Find the task with the earliest arrival time and first free vehicle.
If the task can be scheduel on vehcile v, such that the tasks start time is after its arrival and its end time is before its deadline.
And the vehicle has enough battery to execute the task.
And set the vehicle to be busy until the task is completed.
Else if the vehicle cannot execute the task since it needs rechage, then schedule a recharge task on the vehicle and recharge to max capacity. And set the vehicle to be busy until the battery recharge is completed.
Else if none of above holds, no feasible solution can be constructed, so terminate.

9 - 21

22)
The model is based on one presented by Berthold et al. 2010, which uses a hybrid approach to solve the resource constrained project scheduled problem. Using the CP's global cumulative constraint, which exist for multiple resources as cumulatives in Gecode.
Zhao et al. 2014 solves the elective surgries ith sequence-dependent setup times to multiple operating rooms with CP. This relates to our problem in which it also handles resource and sequence depenedent setup times.
In our model we create a Giant-Tour, which creates a single tour by introducing auxiliary tasks, a start and end task for each vehicle. Which serves for transitioning tasks between the different AGVs.
The picture shows an example of a giant tour, with 3 tasks and 3 recharges and two vehicles. Where S1 and S2 are the start tasks of the different vehicles and E1 and E2 are the end tasks of each vehicle. thus the route between task and S2 are the tasks executed by vehicle 1 and from S2 to E2 on vehicle 2.

23)
We use the following decision variables.
Where Vehicles is an array of length N, representing the assignment of tasks to vehicle, such that if v_1 = 2, then task 1 is scheduled on vehicle 2.
Start and process is the start and processing times of all tasks.
The successor array states the immediate successor to a task.
The Battery decision variables gives the battery level after executing a task, such that b_1 is the battery level at the vehicle which executed task 1. and makespan is pretty selv explainatory.

24)
As Berthold et. Al shows, we can easily model the RCPSP with the cumulative(s) constraint, which ensure that at each point in time, the cumulated demand of the set of tassk that overlap at that point, does not exceed a given limit.
The cumulatives constraint handles multiple resources, which here are the vehicles, thus the demand and the resource capacity are 1 for all tasks and vehicles.
To however make it possible to find the setup times between the tasks and calculate the battery levels. We need to keep track of the sucessors and predecessors. So we can set up the successor array with an element constraint, that ensures that V_x_j = v_j for all tasks that are normal task N, recharge tasks R or start tasks A_s. Thus only end tasks can have a successor that is not scheduled on the same vehicle.
We then setup a predecessor array in the same way using a channeling constraint.
The giant tour mentioned before can then easily be formed with the circuit global constarint, where ST is the setup times matrix.
T is the actual setup and CC is the total cost (not used in this model)

We handle recharges by introducing a battery cost for each task c_j. which depends on the task type. If its a normal task then its the cost as defiend by the input.
if its a recharge task, then its the recharge cost per time multiplied by the processing time. And zero if an auxliary task.

The cost is then calculated based on the predecessor. first we set up the bb_j, shich is the battery before task j, thus the predecessors battery. And then we add the demand for the recharges to the level at predecesor and else the demand is subtracted from the level.

25)
Since the model uses a specific number of recharges, we need to calculate the actualy number of recharged needed, in order to solve the problem optimally. this can be done by transforming the input into an instance of the binpacking.
Where the bins represent the vehicles and recharges and the objects the battery consumptions of tasks.
The capacity of the bins is the span between the lower and upper bounds, and we set the first m bins to have the capacity which is given by span from the the initial battery capacity to the lower bound. And then solve the problem.
The minimum number of needed recharges is then the max of number of bins used minus the number of vehicles and zero.

26)
We now introduce another approach to solve the RCPSP-ST, by using a heuristic approach. this is based on a sequential insertion heuristic presented by Solomon 1987, which solved the vehicle routing and scheduling problem with time window constraint (VRSPTW).
The algoritms works as follows:
We define a schedule of each vehicle to be a route. And let these routes be inialized based on an initial criteria.
And then we repeatedly insert tasks with the following scheme:
Select a route with another criteria cr, and then for all still unrouted tasks calculate the best insertion palce for each task with criteria cp..
When the best insertion place is calculated we then select the best task based on another criteria and then insert this.
When calculating the best isnertion place, we check for feasiblity of the new schedule, and only feasbile scheduled with regards to the time windows are considered. The recharge capacities are ignored at first, and is then handled afterwards.

27)
Example first initialize routes.
Insertion scheme:
  1. Select best route
28)
  2. Calculate best insertion places
  3. Select and insert task

29)
Repeat untill all tasks are inserted
Afterwards recharges are inseated to construct a feasible schedule.

30)
When inserting tasks, and we check for feasiblity, we check tasks affected by a push forward.
A push forward of a task is when another task is inserted into the schedule.
ex. for task 2, when task 3 is inserted, the insertion of task 3 causes a push forward of task 2. and such we have to check for feasiblity of task 2 in the new schedule (of course also for task 3).
This feasibility is the time windows of the tasks.
This is tested for all potential insertions, that means both in the insertion scheme when calculating the best insertion point and when fixing the afterwards schedule by inserting rechages.

31-32) Diana
33)
When solving a problem using CP, exploration of the search tree is just as important as modeling it.
We defiend the problem as a COP (constraint optimization problem), where we look for an assignment of the variables which satisfies all of the constraints, and then see the quality of the solution with respect to the objective function. Gecode does this with a propagation-based constraint solver, based on exhaustive search.
Propagators realize the constraints of a COP by pruning the variable domains. and by this propagation, localconsistency is restored on each constraitn. such that infeasible values from the variable domains are removed.
This is however not enough, so we need to split the problem up into smaller problems, and these are then solevd recursively. The order in which the smaller problems are created are determined by a search. and the recursion is implemented as a backtracking search.

As the table shows, we post branchers in different order to see the quality of branching on the different variables.
As we see the order greatly influences the time it takes to find the optimal solution or find a solution at all.
By branching on the successor first, we limit the search tree greatly, since having a total ordering of the tasks, and by selecting the first variable and lowest value in all branchings first, we can determine if a solution can be constructed fast.

Also symmetries exist in the tree, since the same solution occur if we swap two recharges, or vehicles who are completely alike.
Also Gecode offers the option of selecting multiple threads to work in parallel, this also improved the solution.
We found out that selecting a random vehicle for each task, yielded a better solution than the minimum one. But this was not the case with the successor array.

34)
With IH we have a huge amount of criteria combinations.
5ci x 3cr x 2cp x 3ct. And combined with options of arguments as well, we have to consider even more.

i-alpha_1 and i-alpha_2 are used in only the initialization.
whereas cr do not use any arguments.
The rest is used in CR and CT depending on the criteria selected.

35)


36)


37)


38)


39)


40)
Thank you !
