\chapter{Experimental Setup}
For our solution assessment we will initially simulate the arrival of jobs according to
reasonable assumptions. Input data regarding the environment will be synthetically generated.
In a later stage historical data provided by the company will be used for the assessment. Our main measure of quality when comparing synthetic data between methods will be the time it take to solve the problem to optimality and the solution quality (Makespan) if we do not find an optimal solution within the time limit.
We also assess our model, with respect to the defined greedy algorithm.\\

\section{Technologies}
The Greedy algorithm, the CP model as well as the insertion heuristic were implemented in $C++$.
The CP model uses a library called Gecode (Generic, constraint, development, environment), which is a toolkit for developing constraint-based systems and applications\cite{Gecode}.
MILP and SPH are both implemented in Python, IMP using Gurobi - A mathematical programming solver. (Need citation).

\section{Instance Generation}
%- Description of instances, offline vs. online
%- Model differences (CP vs. MILP)
In this subsection we will describe how the instances for the CP and MILP models are generated and for what purposes. Both models use the exact same instance generation program, but ignore or modify certain parameters based on their implementation. In order to determine the performance of each of the models we use two types of instances, offline and online.
\subsection{Offline}
Offline instances are instances that can be given directly to a model for solving. These instances can be generated for varying sizes and can then be used for testing the basic abilities of each of the models. We initially used these instances in order to visually assess the correctness of a solution. We also use these instances to determine how long it takes for each model to find the optimal solution for a given instance of a certain size. These instances are made of the parameters shown in figure (\ref{offlineparam}).
\begin{table}[H]
\centering
\caption{Parameters given in an offline instance}
\label{offlineparam}
\begin{tabular}{l|l}
\textbf {Vehicle} & \textbf{Task} \\
\tabitem Number of Vehicles $m$ & \tabitem Number of Tasks $n$\\
\tabitem Battery level (min  $q$/max $f$/current $l_i$) & \tabitem Processing time $p_j$\\
\tabitem Battery Power Charge time $e$/amount $b$& \tabitem Setup Time $s_{jk}$\\
 & \tabitem Start time $g_j$ \\
 & \tabitem Deadline $d_j$ \\
& \tabitem Power Consumption $c_j$ \\
\end{tabular}
\end{table}
Given the number of tasks and vehicles, we can generate an instance as follows. We first randomly generate the processing time for each type of task (Linen, garbage and transport). We work in minutes, because precision in seconds for task processing, transport time and other timed activities would not add any quality to the solution and would needlessly increase the domain of the schedule. The processing time will be the time it takes for any given vehicle to perform the given type of task. In the case of linen tasks, the processing time would be the time it takes any given vehicle to drive from the depot to the drop off point. This time will be generated differently for each type of task and it will be randomly selected between the values 10 and 30 minutes. These times were estimated based on a given map of the hospital environment.\\
We also generate the setup time based on a task position. This position is purely random as well, since we start by generating a positioning table with all the values, prior to assigning time to any tasks. Some types of tasks are more likely to start at certain positions. An example of this is the linen type task, which requires the vehicle to go to the depot and get fresh linen. Finally, we also generate setup times for going to/from the depot for use when scheduling recharging tasks. Since linen tasks begin at the depot, the setup time would be zero when going from recharging to a linen type task and the processing time of a linen task going to the depot. Any other task will have a differently generated value between based on their randomly assigned positioning.
\\
We also compute a \textsc{maxtime} constant, which is the max time period we schedule over. It is set to be 24-hours since our overall problem is to schedule over the time period of a day. Each task also has a start time and a deadline, though they are of little significance for the problem. The start time and deadline of a task is by default equal to zero and \textsc{maxtime} respectively. They can be set to different values randomly in order to test how the model performs under a more time constrained problem.
\\
Next, we have all the vehicle specific parameters. The max and min level of battery for a vehicle is by default set to 100 and 50 respectively. These are estimated values since we have no notion of the units or amount used in the real batteries used for the vehicles. Same goes for the charge time and amount. The amount is just the difference between max charge and min, which is in this case 50. The time it takes to charge such an amount is the charge amount minus the charge amount divided by $\frac{3}{4}$. Finally, we generate the consumption for each of the task types. This is the amount of battery power required to perform each given task. The consumption is set to be the processing time multiplied with 1.55 in order to get a consumption which would require a recharge after nearly each performed task.\\
\smallskip
\subsection{Online}
Online instances are plans over tasks entering the system spanning over a period of 24-hours. They are given to the simulation system in order to simulate the occurrence of tasks throughout a day by giving each model the subset of tasks entering at a given time step. The given subset is then converted into the same format as an offline problem and is solved as a part of an online by each model.\\
\smallskip
\\
In order to generate an online instance we only need the number of tasks and vehicles as parameters. Based on these we then determine the rest of the parameters such as: battery level, processing time, setup times, entry time and occurrence of each task type. The main idea behind the online instances is to get a distribution of task arrivals which is close to what we would see in practice. Linen and transport type tasks enter the system throughout the day, while garbage collection tasks enter the system at midnight and will be limited to be completed before 8:00 AM. Therefore the entry time of any garbage type task is zero. To get the entry time of the tasks other than garbage we used an exponential distribution, with a mean of $\lambda$ (Where $n_g$ described the size of the subset of garbage type tasks in the total set of size $n$).
\begin{equation}
\lambda = \frac{(24 \times 60-8\times 60)}{(n-n_g)} \label{eq:lambda}
\end{equation}
The computation of $\lambda$ is described in Eq. \ref{eq:lambda}. Given the index of the task we sample from the exponential distribution the delta time in minutes it enters the system after the previous task.\\
The number of tasks of each type is determined based on the total number of tasks given as parameter. About $\frac{1}{2.3}$ of the tasks are set to be garbage collection tasks, while another $\frac{1}{2.3}$ is set to be linen type task. The rest is set to be transport. An example of this would be given a total of 300 tasks, we get 131 garbage type tasks after rounding up. The same goes for linen tasks. As for transport tasks, the difference becomes 38 tasks of that type.\\
Processing times and setup times are determined exactly as in an offline instance, but with a lower times in order to be as close to the real scenario as possible.\\
For vehicle specific parameters we only generate the battery level, which by default is set to be the same as a full charge of 500 for task plans. Parameters such as consumption, charge time, amount and min level are determined based on the full charge and the processing times when they are needed.

\section{Comparison}
In order to analyse the performance of each of the models, we ran tests with a time limit of one hour.
The specs for the computer on which the test were ran are as follows:
\begin{itemize}[noitemsep,nolistsep]
\item OS: 64bit Ubuntu 14.04LTS
\item Processor: Intel Core i7-4790 CPU @3.60Ghz x8
\item Memory: 15.6GB
\item Graphics: Intel Haswell Desktop
\end{itemize}

Given the different models, there are limitations to what they can accomplish.\\ %E.g The CP mode has a hard time comming up with solutions for \\

We later provide some "fixed" versions of the instances, which use at most one recharge.\\
But we strive to test the capability of the models, being able to schedule recharging on the different vehicles in a battery constrained environment (In that only a few tasks can be scheduled, before a battery recharge is needed on a given vehicle).

We ran all the models, with $1$ battery recharge station, that is only one concurrent battery can happen at any given time. Also a timelimit of 1 hour was used.

\section{Greedy}
As a baseline the comparisons of all tests, we provide the results from running the greedy algorithm on the test instances:
\begin{table}[H]
\centering
	\caption{Table showing the results form running the Greedy algorithm on instances of varying size with unlimited battery recharge stations}
	\label{greedy_offline}
    \begin{tabular}{|l|l|l|} \hline 
    Vehicle & Tasks 	& Solution 	\\ \hline        
    5		& 10		& 115 		\\ \hline 
    5		& 15		& 228 		\\ \hline 
    5		& 20		& 294 		\\ \hline 
    5		& 25		& 407      	\\ \hline 
    10		& 15 		& 123 		\\ \hline 
    10		& 20		& 81 		\\ \hline 
    10		& 25 		& 157      	\\ \hline 
	\end{tabular}
\end{table}

\begin{table}[H]
\centering
	\caption{Table showing the results form running the Greedy algorithm on instances of varying size, with a single battery recharge stations}
	\label{greedy_offline}
    \begin{tabular}{|l|l|l|} \hline 
    Vehicle & Tasks 	& Solution 	\\ \hline        
    5		& 10		& 175 		\\ \hline 
    5		& 15		& 346 		\\ \hline 
    5		& 20		& 470 		\\ \hline 
    5		& 25		& 622      	\\ \hline 
    10		& 15 		& 190 		\\ \hline 
    10		& 20		& 166 		\\ \hline 
    10		& 25 		& 376      	\\ \hline 
	\end{tabular}
\end{table}

\begin{table}[H]
\centering
	\caption{Table showing the results form running the Greedy algorithm on instances of varying size, with battery recharges given by the instances}
	\label{greedy_offline}
    \begin{tabular}{|l|l|l|l|} \hline 
    Vehicle & Tasks 	& Solution 	\\ \hline        
    5		& 10		& 115 		\\ \hline 
    5		& 15		& 227 		\\ \hline 
    5		& 20		& 316 		\\ \hline 
    5		& 25		& 443      	\\ \hline 
    10		& 15 		& 123 		\\ \hline 
    10		& 20		& 90		\\ \hline 
    10		& 25 		& 148      	\\ \hline 
	\end{tabular}
\end{table}