\section{SPH Results}
In this chapter, we will present the assessment results based on the tests performed on the set partitioning heuristic approach described in Subsection \ref{sec:schedulegeneration}. These tests were performed in order to compare the performance of the SPH model to the other models, but also in order to determine which parameters would perform the best under different circumstances. Here, we see performance in the sense of producing a set of columns which can be used to find both a feasible and optimal solution. We also see performance in the time spent generating the columns, checking and possibly fixing feasibility for overlapping recharge tasks and actually solving the instance. The results presented in the tables are the best results with the given parameters from repeated runs, while the plots show the averaged results.
\medskip
\paragraph{Parameters}
The SPH model uses a few different parameters for the preprocessing phase and set partitioning phase. These parameters include parameters for creating all partial permutations of a size within a given range for exhaustive search. These will be known as task limits. We can also choose to use sampling instead, for a quicker preprocessing phase. If sampling is used, then we include parameters for the sample size and sample range. The sample size is how many samples we take from the complete list of tasks. The possible length of these samples is determined by the sample range. If this range is zero, then all samples will be of length $\ceil{\frac{n}{m}}$. Otherwise, if for example the range is between -1 and 1, then for each sample, we have a chance of getting a length between $\ceil{\frac{n}{m}}-1$ and $\ceil{\frac{n}{m}}+1$. There is also a parameter for always charging to full capacity. Otherwise, the default is only charging what is needed. For creating more varied schedules with regards to recharges, the parameter offsetting can be set. The number given as the offsetting parameter determines how far we are willing to offset a recharge task and its schedule. There is also a parameter for always ending a schedule with a trip to the depot and recharging to full. The preprocessing step can also include the construction of a start solution, which is more likely to be feasible than picking from the rest randomly. Next, in relation to the set partitioning phase we can choose between a total times and makespan objective. This chapter will not be presenting all tests we ran in order to determine parameters. We will mainly focus on sample sizing. For these tables, let \#\textit{St} represent the number of stations and let \#\textit{Ov} represent the number of overlaps. Finally, let \# \textit{Cols} represent the final number of columns generated. This means the total number of columns generated from the actual schedule generation phase and the optional offsetting phase afterwards. This number might be lower than the number of schedules distributed on all vehicles based on the sample size. This is because some schedules may discarded afterwards if they are infeasible with regards to deadlines. In the following tables we can see the results of adjusting the sampling size parameters for the given instances. We will also see what changes adding a start solution brings to the solutions. All the results are the best ones achieved from running the model repeatedly. Later we will show averaged results for the preprocessing time, the solving time and the objective value in a few plots.
\medskip
\iffalse
\subsection{Sample Size} First, let us look at the results for the small offline instances used in the other assessment chapters. These instances are characterised by the fact that they can be solved with at most one recharge task per vehicle.  The  
\begin{table}[H]
\caption{Table showing the results from running the heuristic set partitioning model on instances with a number of tasks and vehicles. For these tests we have a makespan objective function. The schedule generation phase used a sampling of 200 permutations with a size of between $\ceil{\frac{n}{m}} -1$ and $\ceil{\frac{n}{m}} + 1$. Additionally, this run had offsetting applied to a quarter of all recharges generated, with an offsetting distance of at most $p$ times three (Where $p$ is the time spent on recharging).}
\centering
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
Vehicles & Tasks & \#Cols & Tasklimits & \#St & \#Ov & CG Time & Solve Time & Obj.\\ \hline 
5 & 10 & 1255 & (1.0, 3.0) & 1 & 3 & 0.5815 & 0.0744 & 105.0\\ \hline 
5 & 15 & 1668 & (2.0, 4.0) & 1 & 3 & 2.0058 & 0.3895 & 207.0\\ \hline
5 & 20 & 1900 & (3.0, 5.0) & 1 & 3 & 2.8085 & 0.9163 & 282.0\\ \hline
5 & 25 & 2333 & (4.0, 6.0) & 1 & 3 & 4.9521 & 9.9024 & 452.0\\ \hline 
10 & 15 & 2636 & (1.0, 3.0) & 1 & 5 & 3.9737 & 0.2067 & 124.0\\ \hline 
10 & 20 & 2533 & (1.0, 3.0) & 1 & 5 & 0.7999 & 0.612 & 78.0\\ \hline 
10 & 25 & 3273 & (2.0, 4.0) & 1 & 5 & 6.9286 & 2.469 & 147.0\\ \hline 
\end{tabular}

\label{table:sphperf200makespan}
\end{table}
From Table \ref{table:sphperf200makespan} we can see that every instance was solvable with the given set of parameters. Using the sampling there is always the possibility of generating a set of schedules which 'double books' tasks or schedules which do not cover all tasks. Large variation in the range of sizes for the permutations can also affect feasibility and solution quality negatively. Whenever the sampling range is increased, the sampling size should increase as well, in order to improve the coverage of all tasks. 
%The solution quality is largely affected by the sampling range. If the processing times of the given tasks have little variability, then a large sampling range could produce an infeasible set of schedules. This infeasibility would be caused by the fact that such an instance would tend towards an evenly distributed result. The range in permutation sizes should be increased alongside the variability of processing times of the given tasks, as well as the sampling size, s. Minimizing makespan most often leads to schedules with an even distribution of tasks, especially if setup times and processing times of the tasks are mostly homogeneous.
The instance with 5 vehicles and 25 tasks would at times fall victim to infeasibility because of small sample sizes. This was most likely caused by the larger task to vehicle ratio compared to the other tasks. The larger distributions with small sample sizes may decrease the chance of task coverage in our generated schedule set. We tried increasing the sampling size for that particular instance to 300 (See Table \ref{table:sphperf300makespan}).

\begin{table}[H]
\centering
\caption{This figure shows the results from running the recharge heavy instances using a sampling size of 300.}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
Vehicles & Tasks & \#Cols & Tasklimits & \#St & \#Ov & CG Time & Solve Time & Obj.\\ \hline 
5 & 10 & 1865 & (1.0, 3.0) & 1 & 3 & 1.3808 & 0.0926 & 104.0\\ \hline 
5 & 15 & 2501 & (2.0, 4.0) & 1 & 3 & 4.7207 & 1.7125 & 207.0\\ \hline 
5 & 20 & 2806 & (3.0, 5.0) & 1 & 3 & 6.2683 & 2.6801 & 282.0\\ \hline 
5 & 25 & 3463 & (4.0, 6.0) & 1 & 3 & 9.7106 & 18.96 & 426.0\\ \hline 
10 & 15 & 3960 & (1.0, 3.0) & 1 & 5 & 9.3776 & 0.1924 & 124.0\\ \hline 
10 & 20 & 3706 & (1.0, 3.0) & 1 & 5 & 1.5829 & 0.3953 & 77.0\\ \hline 
10 & 25 & 4963 & (2.0, 4.0) & 1 & 5 & 16.7073 & 1.6091 & 146.0\\ \hline 
\end{tabular}
\label{table:sphperf300makespan}
\end{table}
Generally the instances showed to be solvable more often and produced a mostly the same objective value after the repeated runs. From the table we can observe that the preprocessing time only saw a small increase. The jump from 200 to 300 samples did not make much difference in regards to feasibility.
\medskip
\par
We also tried increasing the sampling to further to 500 and 1000 for all instances, with the results shown in Table \ref{table:sphperf500makespan} and \ref{table:sphperf1000makespan}.
\begin{landscape}
\begin{table}[H]
\centering
\caption{Table showing the results from running the heuristic set partitioning model with a sampling size of 500 across all instances.}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
Vehicles & Tasks & \#Cols & Tasklimits & \#St & \#Ov & CG Time & Solve Time & Obj.\\ \hline 
5 & 10 & 3170 & (1.0, 3.0) & 1 & 3 & 5.4101 & 0.1383 & 104.0\\ \hline 
5 & 15 & 4178 & (2.0, 4.0) & 1 & 3 & 13.5508 & 0.6371 & 207.0\\ \hline 
5 & 20 & 4616 & (3.0, 5.0) & 1 & 3 & 18.0126 & 1.9064 & 282.0\\ \hline 
5 & 25 & 5831 & (4.0, 6.0) & 1 & 3 & 26.9986 & 15.1397 & 386.0\\ \hline 
10 & 15 & 6566 & (1.0, 3.0) & 1 & 5 & 24.7457 & 0.3559 & 124.0\\ \hline
10 & 20 & 6260 & (1.0, 3.0) & 1 & 5 & 4.9306 & 0.588 & 74.0\\ \hline 
10 & 25 & 8316 & (2.0, 4.0) & 1 & 5 & 45.8697 & 2.8392 & 146.0\\ \hline
\end{tabular}
\label{table:sphperf500makespan}
\end{table}
\end{landscape}
\begin{table}[H]
\centering
\caption{Table showing the results from running small instances with a makespan objective and a sampling size of 1000 and some random offsetting.}
\label{table:sphperf1000makespan}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
Vehicles & Tasks & \#Cols & Tasklimits & \#St & \#Ov & CG Time & Solve Time & Obj.\\ \hline 
5 & 10 & 6299 & (1.0, 3.0) & 1 & 3 & 18.522 & 0.2667 & 104.0\\ \hline
5 & 15 & 8355 & (2.0, 4.0) & 1 & 3 & 55.1209 & 0.9974 & 207.0\\ \hline
5 & 20 & 9263 & (3.0, 5.0) & 1 & 3 & 70.6751 & 3.4718 & 282.0\\ \hline 
5 & 25 & 11651 & (4.0, 6.0) & 1 & 3 & 113.5154 & 65.5382 & 383.0\\ \hline 
10 & 15 & 13430 & (1.0, 3.0) & 1 & 5 & 106.348 & 1.3801 & 124.0\\ \hline 
10 & 20 & 12656 & (1.0, 3.0) & 1 & 5 & 23.2818 & 1.1189 & 73.0\\ \hline 
10 & 25 & 16550 & (2.0, 4.0) & 1 & 5 & 179.821 & 3.878 & 146.0\\ \hline
\end{tabular}
\end{table}
From the tables we can see how the instance with the largest task distribution (5 vehicles and 25 tasks) gets an improvement with regards to the objective when running with larger sample sizes. However, when doing the same for the smaller instances, we appropriately continue to see little to no difference in the objective. The only change for these instances is the change in time spent on preprocessing, which considerably worsens.
\medskip
\par
Next, we will present three of the tables for the recharge light instances, namely for 300 samples in Table \ref{}, 500 samples in Table \ref{table:sphperf300fixmakespan} and 1000 samples \ref{table:sphperf500fixmakespan} in order for us to get a sense of the time progression for these types of instances.
\begin{table}[H]
\centering
\caption{Table showing the results from running the heuristic set partitioning model on recharge light instances with sampling size 300.}
\label{table:sphperf300fixmakespan}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
Vehicles & Tasks & \#Cols & Tasklimits & \#St & \#Ov & CG Time & Solve Time & Obj.\\ \hline 
5 & 10 & 2005 & (1.0, 3.0) & 1 & 3 & 1.8953 & 0.1456 & 104.0\\ \hline 
5 & 15 & 2483 & (2.0, 4.0) & 1 & 3 & 4.4669 & 0.9254 & 207.0\\ \hline 
5 & 20 & 1671 & (3.0, 5.0) & 1 & 3 & 0.036 & 0.771 & 192.0\\ \hline 
5 & 25 & 1760 & (4.0, 6.0) & 1 & 3 & 0.0509 & 8.9716 & 264.0\\ \hline  
10 & 15 & 3900 & (1.0, 3.0) & 1 & 5 & 7.9403 & 0.1814 & 124.0\\ \hline 
10 & 20 & 3706 & (1.0, 3.0) & 1 & 5 & 1.5802 & 0.3969 & 77.0\\ \hline 
10 & 25 & 3310 & (2.0, 4.0) & 1 & 3 & 0.2988 & 0.2579 & 93.0\\ \hline 
\end{tabular}
\end{table}
\begin{table}[H]
\centering
\caption{Table showing the results from running the heuristic set partitioning model on recharge light instances with sampling size 500.}
\label{table:sphperf500fixmakespan}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
Vehicles & Tasks & \#Cols & Tasklimits & \#St & \#Ov & CG Time & Solve Time & Obj.\\ \hline 
5 & 10 & 3335 & (1.0, 3.0) & 1 & 3 & 5.659 & 0.1627 & 104.0\\ \hline 
5 & 15 & 4161 & (2.0, 4.0) & 1 & 3 & 14.3076 & 0.4704 & 207.0\\ \hline 
5 & 20 & 2791 & (3.0, 5.0) & 1 & 3 & 0.0881 & 1.7627 & 185.0\\ \hline 
5 & 25 & 2975 & (4.0, 6.0) & 1 & 3 & 0.1146 & 21.0715 & 251.0\\ \hline 
10 & 15 & 6636 & (1.0, 3.0) & 1 & 5 & 24.6081 & 0.3739 & 124.0\\ \hline 
10 & 20 & 6260 & (1.0, 3.0) & 1 & 5 & 4.9262 & 0.4 & 74.0\\ \hline 
10 & 25 & 5573 & (2.0, 4.0) & 1 & 3 & 0.4305 & 0.3476 & 93.0\\ \hline 
\end{tabular}
\end{table}
\begin{table}[H]
\centering
\caption{Table showing the results from running the heuristic set partitioning model on recharge light instances with sampling size 1000.}
\label{table:sphperf1000fixmakespan}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
Vehicles & Tasks & \#Cols & Tasklimits & \#St & \#Ov & CG Time & Solve Time & Obj.\\ \hline 
5 & 10 & 6583 & (1.0, 3.0) & 1 & 3 & 22.4051 & 0.2668 & 104.0\\ \hline 
5 & 15 & 8268 & (2.0, 4.0) & 1 & 3 & 55.7173 & 0.8753 & 207.0\\ \hline 
5 & 20 & 5585 & (3.0, 5.0) & 1 & 3 & 0.217 & 2.0664 & 180.0\\ \hline 
5 & 25 & 5911 & (4.0, 6.0) & 1 & 3 & 0.3207 & 9.8177 & 235.0\\ \hline 
10 & 15 & 13166 & (1.0, 3.0) & 1 & 5 & 96.7778 & 0.8857 & 124.0\\ \hline
10 & 20 & 12760 & (1.0, 3.0) & 1 & 5 & 25.0978 & 0.731 & 73.0\\ \hline
10 & 25 & 11116 & (2.0, 4.0) & 1 & 3 & 1.4263 & 0.6325 & 93.0\\ \hline 
\end{tabular}
\end{table}

Based on all of the sampling tables, we can see the plots of the development in time for both preprocessing and solving as the sample size increases in Fig. \ref{fig:plottimesample} and Fig. \ref{fig:plottimesamplefix}.
\begin{figure}
\centering
\label{fig:plottimesamplefix}
\caption{A plot showing the difference in time for each recharge light instance when the sample size is increased.}
\includegraphics[width=10cm]{plotallsphfinalfixtimes.png}
\end{figure}

\begin{figure}
\centering
\label{fig:plottimesample}
\caption{A plot showing the difference in time for each instance when the sample size is increased. These are the instances where more than one recharge may occur.}
\includegraphics[width=10cm]{plotallsphfinaltimes.png}
\end{figure}
Based on Fig. \ref{fig:plottimesample}, we can immediately observe that the preprocessing time has increased across most sample sizes for a select few instances. The instances in question are the ones with 5 vehicles and 20 tasks, 5 vehicles and 25 tasks and 10 vehicles and 25 tasks. The progression in time versus sample size for these instances reaffirm the conclusion that recharge heavy schedules are costly during preprocessing. The solving time seems generally stable low and stable, with the exception of the instance with 5 vehicles and 25 tasks. The reason for this break in the line is most likely caused by an unlucky run. With more tasks to be distributed across vehicles, as well as the added requirement of choosing schedules which do not fail the overlapping constraint, the time spent solving is very much based on luck of the draw during both the sampling phase and the offsetting. Fig. \ref{fig:plottimesamplefix} shows somewhat the same development in time, for a recharge light version, so it may just be the distribution of tasks. Generally, the time spent preprocessing is much lower for the recharge light instances, which shows how the feasibility checking for recharge tasks is the bottleneck for the whole process. 
\fi
\subsection{Sample Sizes} In this section we will present the best results for running with no offsetting, since the implementation of offsetting is bugged. Performing no recharging offsetting is not so much a problem for recharge light instances as sampling size can be. Therefore, the offsetting parameter can be kept rather low or off without particularly affecting feasibility. Tables \ref{table:sphperf500makespanoff0} and \ref{table:sphperf1000makespanoff0} are an example of how well we perform with no offsetting applied to recharge heavy instances. 
\begin{table}[H]
\centering
\caption{Table showing the results from running the heuristic set partitioning model on recharge heavy instances with sampling size 500 and no offsetting. }
\label{table:sphperf500makespanoff0}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
Vehicles & Tasks & \#Cols & Tasklimits & \#St & \#Ov & CG Time & Solve Time & Obj.\\ \hline 
5 & 10 & 2405 & (1.0, 3.0) & 1 & 3 & 1.2491 & 0.0377 & 162.0\\ \hline 
5 & 15 & 2500 & (2.0, 4.0) & 1 & 3 & 3.093 & 0.2396 & 207.0\\ \hline 
5 & 20 & 2500 & (3.0, 5.0) & 1 & 3 & 5.5143 & 1.6863 & 282.0\\ \hline 
5 & 25 & 2500 & (4.0, 6.0) & 1 & 3 & 8.0966 & 6.9257 & 411.0\\ \hline 
10 & 15 & 5000 & (1.0, 3.0) & 1 & 5 & 4.0749 & 0.0762 & 124.0\\ \hline 
10 & 20 & 5000 & (1.0, 3.0) & 1 & 5 & 3.9605 & 0.2086 & 79.0\\ \hline 
10 & 25 & 5000 & (2.0, 4.0) & 1 & 5 & 21.5907 & - & -\\ \hline 
\end{tabular}
\end{table}
\begin{table}[H]
\centering
\caption{Table showing the results from running the heuristic set partitioning model on recharge heavy instances with sampling size 1000 and no offsetting.}
\label{table:sphperf1000makespanoff0}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
Vehicles & Tasks & \#Cols & Tasklimits & \#St & \#Ov & CG Time & Solve Time & Obj.\\ \hline 
5 & 10 & 4840 & (1.0, 3.0) & 1 & 3 & 5.0435 & 0.054 & 162.0\\ \hline 
5 & 15 & 5000 & (2.0, 4.0) & 1 & 3 & 10.9839 & 0.1561 & 207.0\\ \hline
5 & 20 & 5000 & (3.0, 5.0) & 1 & 3 & 21.3935 & 0.7952 & 282.0\\ \hline 
5 & 25 & 5000 & (4.0, 6.0) & 1 & 3 & 30.47 & 15.2606 & 384.0\\ \hline 
10 & 15 & 10000 & (1.0, 3.0) & 1 & 5 & 16.5524 & 0.1548 & 124.0\\ \hline 
10 & 20 & 10000 & (1.0, 3.0) & 1 & 5 & 9.7247 & 0.3399 & 79.0\\ \hline 
10 & 25 & 10000 & (2.0, 4.0) & 1 & 5 & 71.322 & - & -\\ \hline 
\end{tabular}
\end{table}

%Between the Tables \ref{table:sphperf500makespan} and \ref{table:sphperf1000makespan} and Tables  \ref{table:sphperf500makespanoff0} and \ref{table:sphperf1000makespanoff0}, we can observe multiple things. 
When we look at the tables, we can observe that the instances are solved relatively fast. We do however fail some instances.
The instance 10 vehicles and 25 tasks cannot be solved without offsetting with the given sample range, seeing as increasing the sample size did little difference. With that particular instance, the distribution of tasks and recharge requirements cause recharges to mostly occur at the same time. Solvable instances such as 5 vehicles and 10 tasks can be used to illustrate how our solutions look when recharges are not offset. We can see the resulting plan in Figure \ref{fig:sphperf500makespanoff0v5t10}.
\begin{figure}
\caption{The best solution for 5 vehicles and 10 tasks when no offsetting is used.}
\label{fig:sphperf500makespanoff0v5t10}
\includegraphics[width=15cm]{sphperf500makespanoff0.pdf}
\end{figure}
We can see the averaged cost in time in Fig. \ref{fig:plottimesample0} and the averaged objective value in Fig. \ref{fig:plotobjsample0}.
% for the runs with no offsetting applied. Comparing the figures to \ref{fig:plottimesample} and \ref{fig:plotobjsample}


\begin{figure}
\centering
\label{fig:plottimesample0}
\caption{A plot showing the difference in time for each recharge heavy instance when the sample size is increased. No offsetting was applied.}
\includegraphics[width=10cm]{plotallsphoff0finaltimes.png}
\end{figure}

\begin{figure}
\centering
\label{fig:plotobjsample0}
\caption{A plot showing the objective value for each recharge heavy instance when the sample size is increased. No offsetting was applied.}
\includegraphics[width=10cm]{plotallsphoff0finalobj.png}
\end{figure}
\iffalse
\begin{figure}
\centering
\label{fig:plotobjsample}
\caption{A plot showing the objective value for each recharge heavy instance when the sample size is increased. Offsetting was applied.}
\includegraphics[width=10cm]{plotallsphfinalobj.png}
\end{figure}
\fi


\paragraph{Start Solution} In this section we will present the results for running SPH and constructing a start solution during preprocessing. We compare the results to the run with no offsetting or start solutions in order to see if we can improve our performance. These runs also increased in sample size over time.

\begin{table}[H]
\centering
\caption{Start solution runs with sample size 200 }
\label{sphperfstart}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
Vehicles & Tasks & \#Cols & Tasklimits & \#St & \#Ov & CG Time & Solve Time & Obj.\\ \hline 
5 & 10 & 975 & (1.0, 3.0) & 1 & 3 & 0.2147 & 0.0142 & 115.0\\ \hline 
5 & 15 & 1005 & (2.0, 4.0) & 1 & 3 & 0.5416 & 0.0411 & 207.0\\ \hline 
5 & 20 & 1005 & (3.0, 5.0) & 1 & 3 & 0.8999 & 0.4653 & 282.0\\ \hline
5 & 25 & 1005 & (4.0, 6.0) & 1 & 3 & 1.1445 & 0.0725 & 405.0\\ \hline 
10 & 15 & 2010 & (1.0, 3.0) & 1 & 5 & 0.6614 & 0.0344 & 124.0\\ \hline 
10 & 20 & 2010 & (1.0, 3.0) & 1 & 5 & 0.393 & 0.0858 & 82.0\\ \hline 
10 & 25 & 2010 & (2.0, 4.0) & 1 & 5 & 3.5528 & 0.1481 & 166.0\\ \hline 
\end{tabular}
\end{table}
In Table \ref{sphperfstart} we see some of the best results we could attain with a generated start solution. The tables shows the results for sampling size 200. Fig. \ref{fig:sphperf200makespanv5t10} shows one of the solutions for 5 vehicles and 10 tasks with sampling size 200. This example shows that atleast a few schedules from the constructed start solution was chosen for the final solution. It is evident from how some of the schedules are offset, even though the random offsetting phase was turned off.
\begin{figure}
\caption{The best solution for 5 vehicles and 10 tasks when creating a start solution.}
\label{fig:sphperf200makespanv5t10}
\includegraphics[width=15cm]{sphperf200makespanstart.pdf}
\end{figure}
In Figure \ref{fig:plotobjsamplestart} we can see the objective value when adjusting the sample size as well. We can compare this to Figure \ref{fig:plottimesamplestart} where we can see the total time spent preprocessing and solving increase as we increase the sampling size. 
\begin{figure}
\centering
\label{fig:plotobjsamplestart}
\caption{A plot showing the objective value for each recharge heavy instance when the sample size is increased. No offsetting was applied, but a start solution was generated.}
\includegraphics[width=10cm]{plotallsphfinalstartobj.png}
\end{figure}
\begin{figure}
\centering
\label{fig:plottimesamplestart}
\caption{A plot showing the difference in time for each recharge heavy instance when the sample size is increased. No offsetting was applied, but a start solution was constructed.}
\includegraphics[width=10cm]{plotallsphfinalstarttimes.png}
\end{figure}

What we can gather from the plot on objective value is that as long as there is no offsetting applied to the schedules, the start solution on its own provides a very good starting point or even the best solution from the beginning. Adjusting the sample size does very little difference. From \ref{fig:plottimesamplestart} we can also gather that we are in fact wasting time preprocessing and solving, since we see no improvement in any case. The results could also indicate that if the offsetting procedure could be reimplemented, a deterministic approach could prove more valuable in regards to time and solution quality.
\subsection{Comparison}
In this subsection, we will compare the resulting objective values and times of the set partitioning heuristic to the results of the mixed integer linear programming model and the greedy approach. The results presented will be the best performing sets from the tests presented in the previous subsection. Comparing the no offset result tables and the offset tables, we see that the former has issues solving certain instances as well as producing at times worse objective values. Therefore, going by feasibility and solution quality, the offset parameter ought to be set. 
\begin{table}[H]
\centering
\caption{Table comparing the results of the greedy approach and the mixed integer linear programming model to the set partitioning heuristic results.}
\label{table:sphcompare}
\begin{tabular}{l|l|l|l|l|l|l|l}
\hline
\multirow{2}{*}{Vehicles} & \multirow{2}{*}{Tasks} &\multicolumn{2}{c}{MILP} & \multicolumn{2}{c}{Greedy}& \multicolumn{2}{c}{SPH} \\  &  & Time & Obj & Time & Obj & Time & Obj\\ \hline 
5 & 10 & 9.3997 & 96.0* & $<$ 1sec & 115 & 0.2289 & 115.0 \\ \hline 
5 & 15 & 3600.0241 & 203.0 & $<$ 1sec & 227 & 0.5827 & 207.0  \\ \hline 
5 & 20 & 3600.0591 & 180.0 & $<$ 1sec & 316 & 1.3652 & 282.0\\ \hline 
5 & 25 &3600.2432 &  228.0& $<$ 1sec & 443 & 1.217 & 405.0\\ \hline 
10 & 15  & 105.109& 122.0* & $<$ 1sec & 124 & 0.6958 & 124.0\\ \hline 
10 & 20 & 2582.1851 & 59.0* & $<$ 1sec & 90 & 0.4788 & 82.0\\ \hline
10 & 25  & 3600.359& 93.0 & $<$ 1sec & 148  & 3.7009 & 166.0\\ 
\end{tabular}
\end{table}
The results presented in Table \ref{table:sphcompare} for SPH stem from the best runs with 200 samples and a generated start solution. We can observe that SPH performs very closely to greedy with regards to the objective value. However, if we compare the total time spent on generating a solution, then SPH flops. When comparing the two models, one should also take into account that the greedy approach is implemented in C++, while SPH is implemented in python. The performance of the methods could be compared more realistically if both models were implemented in the same language. We can try to even the playing field time wise for the larger instances by reducing the sample size.
\medskip
\par
The MILP model and the SPH model both implement recharge modes and are capable of both flexible partial recharges and flexible full recharges. Their main difference is that the MILP model does not deal with recharge station limitations, making the results for the model a lower bound for the other results. When we look at the MILP and SPH columns, we see that SPH comes very close to the results of MILP. The one instance where the optimal result of MILP is also optimal for the full problem is 10 vehicles 15 tasks. SPH does not reach this value in any of its runs with different sample sizes, which could indicate that sample size alone is not enough to get an optimal solution. When we consider the fact that most of the MILP results are a lower bound to solving the full problem and SPH is faster at generating solutions across all the instances while solving a considerably harder problem, we can observe that SPH is a great improvement over the MILP model.

\iffalse
\begin{landscape}
\begin{table}[H]
\centering
\label{sphperf-s200-o2-off2-m}
\begin{tabular}{|l|l|l|l|l|l|l|l|}
\hline
Vehicles & Tasks & \#Columns & Tasklimit & Iterations & Solve Time & Column Generation Time & Objective\\ \hline 
5 & 10 & 1268 & 3.0 & 445 & 0.381 & 1.9885 & 143.0 \\ \hline 
5 & 15 & 1507 & 4.0 & 2988 & 3.921 & 5.9154 & 331.0 \\ \hline 
5 & 20 & 1103 & 5.0 & 12482 & 0.6406 & 0.0477 & 227.0 \\ \hline 
5 & 25 & 1141 & 6.0 & 116867 & 2.9823 & 0.0897 & 400.0 \\ \hline 
10 & 25 & 2232 & 4.0 & 6840 & 0.8007 & 0.4004 & 110.0 \\ \hline 
\end{tabular}
\caption{This figure shows the results of a  schedule generation phase using a sampling of 200 permutations with a size of between $\ceil{n/m} -2$ and $\ceil{n/m} + 2$. }
\end{table}
\end{landscape}
\fi