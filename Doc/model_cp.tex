\chapter{Constraint Programming}\label{cp}
In this section, we first give a brief introduction to Constraint Programming (CP) and then proceed to calculate a lower and an upper bound to the makespan of a solution. We then propose lower and upper bounds to the number of battery recharges needed in order to solve a given instance. We present how the problem can be formulated as a bin packing problem, and how the number of battery recharges needed can be extracted from the solution to this formulation.
Afterwards we introduce a simplified CP model based on the RCPSP, and later extend this to the full CP model which also included setup times as well as battery recharges.
In this section we first give a lower and an upper bound to the makespan of a solution, which allows for both flexible and fixed battery recharges.
\newline
Let a constraint satisfaction problem (CSP) be defined by a finite set of decision variables representing the objects of the problem, and a set of finite set of constraints that forms the relationship among the objects. We then present a constraint optimization problem (COP), which is a constraint satisfaction problem (CSP) that together with an objective function, assigns a value to each assignment of values to the decision variables. An optimal solution to the COP is one that minimizes (or maximizes) the value of the objective function.\\
\newline
Note that the constraint programming model is implemented in Gecode, and thus the global constraints used are reflected by this. Gecode is a toolkit for developing constraint-based systems and applications implemented in C++. It provides a constraint solver with state-of-the-art performance, is free (distributed under the MIT licence and listed as free software by the FSF).\cite{Gecode}
The support for scheduling constraints is however still experimental (\cite{GecodeModeling2015} P. 88), as it still lacks modelling abstractions and specialized branching, thus the scheduling constraints used in this model may change in future versions.
Also note that no consistency levels are specified when posting constraints, this is due to the fact that we are using the default consistency level for the a given constraint.
\newpage
First we briefly summarize the parameters of the model:
\begin{align*}\label{cp:params}
n		&	\text{    Number of tasks}\\
r		& 	\text{    Number of battery tasks}\\
m		&	\text{    Number of vehicles}\\
N		&   \text{    Set of tasks } \{1,\ldots,n\}\\
M		&   \text{    Set of vehicles }\{1,\ldots,m\}\\
R		&	\text{    Set of recharges }\{n+1,\ldots,n+r\}\\
\text{For each } & \text{task } j \text{:}\\
pi_{j}	&	\text{    Processing time of task }j\\
set_{jk}	&	\text{    Setup of task } k, \text{ given task }j \text{ preceeds it}\\
d_{j}	&	\text{    Due date of task } j\\
bc_{j}  &   \text{    Battery consumption}\\
at_{j}	&	\text{    Arrival / release time of task } j\\
\text{For each } & \text{vehicle } i \text{:}\\
l_{i}	&	\text{    Initial battery level of vehicle } i\\
bt_{i}  &	\text{	  Time at which vehicle } i \text{ is busy until}\\
\text{Additiona} & \text{l parameters:}\\
f		&	\text{    Max battery capacity of vehicles}\\
\eta		&	\text{    Time for a vehicle to charge to full capacity}\\		
q		&	\text{    Battery capacity lowerbound, which a vehicle may not go below}\\
U		&	\text{    Upperbound for makespan}\\
\omega  &	\text{    Lowerbound for makespan}\\
\rho		&	\text{    Battery resources (number of concurrent recharges)}\\
\alpha  &	\text{	  Amount of battery recharge per unit of time }\frac{f-q}{\eta})
\end{align*}

\section{Calculating bounds}
In the full (extended) CP Model presented, all the decision variables for the battery recharges are mandatory to be scheduled, thus it is important to calculate bounds on just how many battery recharges are needed, in order to be able to execute all the tasks and achieve an optimal solution with respect to the objective function.
We first calculate an upper and lower bound to the makespan of a given problem instance.

\subsection{Makespan}
Let $U$ and $\omega$ denote the upper and lower bounds of makespan for a solution, respectively.
A naive upper bound to the makespan is the maximum due date of all tasks, thus:
\begin{equation}
U = \max(d_j) \qquad \forall j \in N{\cup}R
\end{equation}
However we might have a task with an arbitrary high deadline, so high that even if all tasks are scheduled on the same vehicle, in direct succession of each other without any idle time, it is not possible to overdue this. Thus a tighter upper bound to the solution, would be exactly this, namely scheduling all tasks on the same vehicle, using the worst case setup time for each task:
\begin{equation}
U = \sum_{j} (p_j + \max(s_{jk})) \qquad \forall j,k \in N{\cup}R
\end{equation}
Since the actual processing time of battery recharges are unknown before finding a solution, the processing time is given as the time it takes to recharge from minimum to maximum battery capacity $\eta$. Also the setup time for recharges are estimated to be the highest setup time between any task and the depot, since the recharge stations, on which the recharges occur, are placed at the depot.\\
A naive lower bound, would be the sum of processing times, divided by the number of vehicles:
\begin{equation}
\omega = \dfrac{\sum_{j} p_j}{m} \qquad \forall j \in N{\cup}R
\end{equation}
\newline

\subsection{Battery Recharges}
\label{battery}
When specifying the number of needed battery recharges, we need to consider the initial battery capacity of the vehicles, the minimum and maximum capacity and the battery consumption of all the tasks.
Let $r_u$ and $r_l$ denote the lower and upper bound to the number of needed battery recharges, in order for the vehicle fleet to execute all the tasks. Recall that $bc_j$ denotes the battery consumption of task $j$, $q$ and $f$ denotes the minimum and maximum battery capacity and $l_i$ is the initial battery level of vehicle $i$.
\newline
Let $bc_t$ and $l_t$ denote the total sum of all the tasks battery consumption and the sum of the vehicles initial battery capacity, respectively:
\begin{equation}
bc_t = sum(bc_j) \qquad \forall j \in N
\end{equation}
\begin{equation}
l_t = sum(l_i) \qquad \forall i \in M
\end{equation}
The total amount of battery $r_t$ needed in order to be able to execute all tasks, can then be calculated as:
\begin{equation}
r_t = bc_t + q \times m - l_t
\end{equation}
$l_t$ is subtracted, since this represents the amount of battery capacity initially available by the vehicles. Since each vehicle must end up with a battery capacity higher than the battery lower bound $q$, we add $q \times m$.\\
\newline
A naive upper bound to the number of needed battery recharges, would be to schedule a battery recharge after each executed task, and thus the number of needed battery recharges is equal to the number of tasks $n$:
\begin{equation}
r_u = n
\end{equation}
For a lower bound, we optimistically assume that each recharge takes a vehicle from the minimum battery capacity $q$ and to the maximum battery capacity $f$, thus the lower bound can then be calculated as:
\begin{equation}
r_l = \dfrac{r_t}{f-q}
\end{equation}
\newline
The number of needed battery recharges $r$ to solve a given instance, will therefore be:
\begin{equation}
\dfrac{bc_t + q \times m - l_t}{f-q} \leq r \leq n
\end{equation}
In practise, the number of battery recharges will lie somewhere in between, since a recharge rarely is executing on a vehicle a battery level being at the minimum capacity, but instead when the current battery level of a vehicle is not enough to execute any of the remaining tasks. Also it seems highly inefficient that a vehicle can only execute one task, before having to do a battery recharge.\\
We instead propose a formulating of an input to the bin packing problem, which then can be solved to give an optimal solution to the number of needed battery recharges.
\newline
A bin packing problem consists of packing objects of different volumes into a finite number of bins (containers) of the same volume, in a way that minimizes the number of bins used.
Here we let the objects be the battery consumption of the tasks, and the vehicles as well as battery recharges be containers.
The volume of the container is the amount of usable battery $q-f$. Since some vehicles might not have an initial capacity equal to maximum capacity $q$, we introduce extra objects equal to the difference between the initial capacity and the maximum battery capacity:
\begin{equation}
q-l_i \qquad \forall i \in M
\end{equation}
And then fix these objects to be put in different bins. Thus for each vehicle $i$, we create an object of volume $q-l_i$ and fix this to be placed in bin $i$.
Since we fix $m$ objects to be placed in different bins, we at least have a solution to the bin packing problem consisting of using $m$ bins, even though a bin might contain an objects with volume $0$.
A solution to the bin packing problem, can then be used to find the exact number of battery recharges needed. Any solution where the number of bins exceeds $m$, corresponds to the need of an additional battery recharge, since we need do a battery recharge in order to gain the additional battery capacity.
Let $r_bins$ be the number of needed bins for a solution to the bin packing problem. We can then extract the exact number of battery recharges needed, in order to solve the problem optimally as:
\begin{equation}
r = r_bins-m
\end{equation}
Assuming that the triangle inequality holds for the setup times between all tasks for the instance solved.


\section{Simplified Model}
The simplified solution is influenced by Berthold et al. \cite{Berthold2010} (2010), who solve the RCPSP problem with a mixture of integer programming (IP), constraint programming (CP) and satisfiability testing (SAT).\\ We can see the simplified model as a RCPSP, with vehicles being resources. We use a discretization of time, and define this, as being in minutes.\\
\newline
Let $S$ and $V$, denote the sets of decision variables $start$ and $vehicles$, respectively.
Where $s_j$ denotes start time of task $j$, and $v_j$ the assignment of task $j$ to a vehicle.
\begin{align*}
S &= [s_j], j \in N, &D(s_j) &= [at_j,d_j-pi_j]\\
V &= [v_j], j \in N, &D(v_j) &= [1,m]
\end{align*}
As stated, the resources in RCPSP are the vehicles.
The resource demand for each task is $1$ and each vehicle has a resource capacity of $1$. That is, each task is carried out by only one vehicle, and each vehicle is only doing one task at each point in time.
\newline
This can be modelled using the global $\mathtt{Cumulatives}$ constraint.
The constraint models a set of machines and a set of tasks to be assigned on these machines. The machines has a resource limit and the tasks a resource usage. The constraint is enforced at any given time for a machine with at least one task scheduled.\\
Let $P$ and $E$ be sets of decision variables denoting the processing and end times of all tasks.
\begin{align*}
\label{cp:sm:1}P &= [p_j], j \in N, &D(p_j) &= pi_j\\
E &= [e_j], j \in N, &D(e_j) &= [0,d_j]
\end{align*}
We can then post the constraint as follows:
\begin{equation}
\mathtt{Cumulatives(V,S,P,E,U,C,limit)}
\end{equation}
Here $V$ and $S$ are as defined above.
%Since the domain of $p_j$ is restricted to be $pi_j$, we do not need $p_j$ to be a %decision variable, but this is due to this being necessary in the extended model (section %\ref{cp:extend}).
$U$ = $\{u_1,\ldots,u_n\}$ refers to the resource usage of tasks and $C$  = $\{c_1,\ldots,c_m\}$ the resource capacity of the vehicles. Limit is a boolean that specifies if the resource limitation is a minimum ($false$) or maximum ($true$).
\newline
To support flexible tasks, i.e. tasks with a flexible duration, scheduling constraints in $Gecode$ does not automatically enforce
\begin{equation}
s_j + p_j = e_j \qquad \forall j \in N
\end{equation}
thus the constraint must be posted manually relation:
\begin{equation}
\label{cp:sm:2}\mathtt{rel(s_j+ p_j = e_j)} \qquad \forall j \in N
\end{equation}
To conclude the simplified model, we minimize the makespan $z$, which is defined as the max of all end times:
\begin{equation}
\label{cp:sm:3}\mathtt{rel(z = \max{E})}
\end{equation}
\begin{equation}
\label{cp:sm:4}\mathtt{\min{z}}
\end{equation}
\newline


\section{Extended Model} \label{cp:extend}
The extended model is based on one presented in Zhao et al. \cite{Zhao2014} (2014), that presents a CP model to schedule elective surgeries to multiple operating rooms.
The full model includes setup times, battery capacity and a limit to the maximum number of concurrent battery recharges. 
The model also considers arrival time of tasks $at_j$, vehicle busy time $bt_i$ as we ll as use lower and upper bounds for the makespan.
We start by introducing the model with fixed battery recharges, and then propose the changes needed in order to support flexible battery recharges instead.\\
\newline


\subsection{Fixed Recharging}
Let $A = \{n+r+1,\ldots,n+r+2m\}$ be a set of $2m$ auxiliary tasks, a $start$ and $end$ task per vehicle, all with zero processing time, setup time and battery cost, thus $\{a_1,\ldots,a_{2m}\}$ refers to tasks $\{n+r+1,\ldots,n+r+2m\}$.
$A_s$ refers to the $\{a_1,\ldots,a_m\}$ start tasks, and $A_e$ the $\{a_{m+1},\ldots,a_{2m}\}$ end tasks.
The auxiliary tasks are always the first $(start)$ and last $(end)$ tasks executed on a given vehicle, thus acts as transitioning tasks, for creating a circuit (Giant tour) of the successors between all vehicles.
The number of needed battery recharges be found as described in the section \ref{battery}.\\
Let $R$ be a set of $r$ battery recharge tasks, which is the number of battery recharges needed to complete all tasks and for all vehicles to have battery levels within $q$ and $f$ at all times.
\newline
$S$, $V$, $P$ and $E$ are extended to include these tasks.
\begin{align*}
S &= [s_j], j \in N{\cup}R{\cup}A, &D(s_j) &= [at_j,d_j-p_j]\\
V &= [v_j], j \in N{\cup}R{\cup}A, &D(v_j) &= [1,m]\\
P &= [p_j], j \in N{\cup}R{\cup}A, &D(p_j) &= [0,U]\\
E &= [e_j], j \in N, &D(e_j) &= [0,d_j]
\end{align*}
%Thus $P$ and $E$ are given by $\{p_1,\ldots,p_{n+r+m}\}$ and $\{e_1,\ldots,e_{n+r+m}\}$.
The processing times for all tasks in $N$ are as given by the simplified model.
The battery recharges in $R$ have a processing time equal to the time it takes to recharge to full capacity $\eta$ and auxiliary tasks have zero, they are imposed via the following relations:
% normal tasks have fixed processing time given by the input
\begin{equation}
\label{cp:ex:1}p_j = pi_j \qquad \forall j \in N
\end{equation}
% processing time of battery recharges is less or equal to the time it takes to recharge to max capacity
\begin{equation}
\label{cp:ex:2}p_j \leq \eta \qquad \forall j \in R
\end{equation}
% Auxiliary tasks has zero processing time
\begin{equation}
\label{cp:ex:3}p_j = 0 \qquad \forall j \in A
\end{equation}
Constraints \eqref{cp:ex:1} ensures that normal tasks have the processing times given by the input, constraints \eqref{cp:ex:2} ensures that processing time of battery recharges will be less than or equal to the time it takes to recharge to max capacity. Constraints \eqref{cp:ex:3} sets the auxiliary processing time to be zero.
\newline
In order to meet the requirements of task arrival times and deadlines, we impose the following constraints:
% Start times should be higher than release time of tasks
\begin{equation}
\label{cp:ex:11}s_j \geq at_j \qquad \forall j \in N
\end{equation}
% Each task must be finished before deadline
\begin{equation}
\label{cp:ex:14}s_j + p_j \leq d_j \qquad \forall j \in N{\cup}R{\cup}A
\end{equation}
For the initial battery level of the vehicles and their minimum start time, the following constraints are imposed:
% Start tasks battery is equal to initial
\begin{equation}
\label{cp:ex:12}b_j = l_{j-n-r} \qquad \forall j \in A_s
\end{equation}
% Each vehicle might have a earliest start date, after which all tasks must start
\begin{equation}
\label{cp:ex:13}s_j \geq bt_{j-n-r} \qquad \forall j \in A_s
\end{equation}
Constraints \eqref{cp:ex:11} ensures that tasks can only be executed after their arrival, constraints \eqref{cp:ex:14} makes sure that tasks are finished being executed before the deadline, and is thus an extension of constraints \eqref{cp:sm:2} in the simplified model. Constraints \eqref{cp:ex:12} ensures that the start auxiliary tasks on each vehicle is equal to the initial battery level.
Constraints \eqref{cp:ex:13} ensures that the first task on a vehicle has to be executed when the vehicle is free.\\
\newline
Let $X$ and $Y$ be the sets of decision variables $successor$ and $predecessor$.
\begin{align*}
X &= [x_{j}], j \in N{\cup}R{\cup}A, &D({x}_j) &= [1,n+r+m]\\
Y &= [x_{j}], j \in N{\cup}R{\cup}A, &D({y}_j) &= [1,n+r+m]
\end{align*}
$X$ defines the order that tasks are run on the vehicles, that is, if $x_j = k$, the immediate successor to task $j$ is task $k$.
$Y$ defines a predecessor array, which is linked with the successor array such that:
\begin{equation}
x_j = k \leftrightarrow y_k = j \qquad \forall j \in X
\end{equation}
This is equal to posting the following $\mathtt{channel}$ constraint:
% link predecessor and successor
\begin{equation}
\label{cp:ex:4}\mathtt{channel(X,Y)}
\end{equation}
Apart from the start auxiliary tasks, the immediate successor to all tasks must be run on the same vehicle:
\begin{equation}
v_{x_j} = v_j \qquad \forall j \in N{\cup}R{\cup}A_s
\end{equation}
which corresponds to imposing the following $\mathtt{element}$ constraints:
\begin{equation}
\mathtt{element(V,x_j,v_j)} \qquad \forall j \in N{\cup}R{\cup}A_s
\end{equation}
To ensure that each vehicle has a start and end task. we impose the following constraints:
% each vehicle has a start and end auxiliary task
\begin{equation}
v_j = j-n-r \qquad \forall j \in A_s
\end{equation}
% each vehicle has a start and end auxiliary task
\begin{equation}
v_j = j-n-r-m \qquad \forall j \in A_e
\end{equation}
To ensure that the successor of an end task, is the start task of the following vehicle
and that the successor of an end task,is the start task of the following vehicle, that is:
\begin{equation}
\label{cp:ex:aa}(x_{A_e1} =  A_{s_2}, x_{A_e2} = A_{s_3}, \ldots, x_{A_em} = A_{s_1})
\end{equation}
This is enforced via the two following relations:
% The successor to an end task is a start task on the 'next' vehicle (in a circular fasion such that the successor to A_e_m is A_s_1).
\begin{equation}
\label{cp:ex:5}
y_j = A_{e_m} \qquad j = A_{s_1}
\end{equation}
\begin{equation}
\label{cp:ex:6}
y_j = j+m-1 \qquad \forall j \in A_s, j > A_{s_1}
\end{equation}
%\begin{equation}
%\label{cp:ex:6}rel(x_j = (j-1)\%m+n+r+1) \qquad \forall j \in A_e
%\end{equation}
%Constraints \eqref{cp:ex:6} sets the successor of each end task to be the start task of %the 'next' vehicle as described above.\\
The first relation \eqref{cp:ex:5} ensures the crossover from auxiliary end task of the last vehicle to the start task of the first vehicle. Relations \eqref{cp:ex:6} ensures that the predecessor to a vehicles start task is the end task of the previous vehicle, as described above in equation \label{cp:ex:aa}.\\
\newline
$N$, $R$ and $A$ are used to build a giant tour (see figure \ref{fig:giant_tour}).
Recall that the first $n$ tasks are normal tasks, next $r$ tasks are battery recharges and the last $2m$ tasks are auxiliary tasks.
The giant tour therefore consists of: $|N|{\cup}|R|{\cup}|A| = n+r+2m$ tasks.
\begin{figure}[H]
\tikzstyle{arc}= [draw,thick,->,shorten >=1pt,>=stealth'] 
\tikzstyle{vertex}=[circle, draw, inner sep=0pt, minimum size=10pt]
\newcommand{\vertex}{\node[vertex]}
\begin{center}
\beginpgfgraphicnamed{giant_tour}%
\begin{tikzpicture}[scale=1]
	\vertex[label=$s_1$, fill=red](s1) at (0,0) {};
	\vertex[label=$e_1$, fill=red](e1) at (0,-1) {};	
	\vertex[label=$s_2$, fill=red](s2) at (1,0) {};
	\vertex[label=$e_2$, fill=red](e2) at (1,-1) {};	
	\vertex[label=$r_1$, fill=green](j11) at (0,3) {};
	\vertex[label=$t_1$](j12) at (-2,1) {};
	\vertex[label=$r_2$,fill=green](j13) at (-2,-2) {};
	\vertex[label=$t_2$](j14) at (-1,-3) {};
	\vertex[label=$r_3$,fill=green](j21) at (3,-2) {};
	\vertex[label=$t_3$](j22) at (3,0) {};		
\tikzset{EdgeStyle/.style={->}}
	\Edge(e2)(s1)
    \Edge(s1)(j11)
    \Edge(j11)(j12)
    \Edge(j12)(j13) 
    \Edge(j13)(j14)
    \Edge(j14)(e1)
    \Edge(e1)(s2)
    \Edge(s2)(j21)
    \Edge(j21)(j22)        
    \Edge(j22)(e2)
\end{tikzpicture}
\endpgfgraphicnamed
\caption{An example of a giant tour of an instance with two vehicles, three tasks and three battery recharges, where all nodes from $s_1$ to $e_1$ are scheduled on vehicle 1, and nodes between $s_2$ and $e_2$ on vehicle 2. \label{fig:giant_tour}}
\end{center}
\end{figure}
Let $ST$ be a matrix of size $(n+r+2m) \times (n+r+2m)$, containing the setup times from the input instance,such that $st_{j*(n+r+2m)+k}$ defines the setup time from task $j$ to task $k$. Let $T$ define the actual setup time, that is, $t_j$ maps the appropriate setup time between the task $j$ and the successor of task $j$. Such that $t_j = st_{jk}$ if task $k$ is the successor to task $j$.
Let $CC$ be the total circuit cost of the edges in the circuit. 
This above can be enforced using the $\mathtt{circuit}$ global constraint, and thus constructing a giant tour:
\begin{equation}
\label{cp:ex:10}\mathtt{circuit(ST,X,T,CC)}
\end{equation}
This constraints the values of $X$, such that their corresponding edges form a Hamiltonian circuit, where $T$ define the cost of the edge for each node, as defined by the $ST$ matrix.
\newline
To ensure that each vehicle can only schedule one task at a time, and to include setup times, we have to impose these constraints manually.
This is due to the $\mathtt{Cumulatives}$ constraint not supporting setup times between the tasks. We thus have to ensure:
\begin{equation}
\label{cp:ex:16}s_j \geq s_{y_j} + p{y_j} + st_{y_jj} \qquad \forall j \in N{\cup}R{\cup}A_e
\end{equation}
Let $B$ and $C$ be the sets of decision variables $battery$ and $battery cost$ respectively.
\begin{align*}
B &= [b_j], j \in N{\cup}R{\cup}A, &D(b_j) &= [q,f]\\
C &= [c_j], j \in N{\cup}R{\cup}A, &D(c_j) &= [0,f-q]
\end{align*}
$B$ defines the battery level after completion of a task, thus $b_j$ is the battery level after completion of task $j$ on the vehicle that executed the task.\\
$C$ specifies the battery cost of executing a task, such that $c_j$ is the battery cost for executing task $j$, this is positive for normal tasks and battery recharges and zero for auxiliary tasks.
Let $\alpha$ be the amount of battery recharge per unit of time.
The battery cost for tasks in $N$ have a fixed battery cost given by the input instance, battery recharges have a variable cost, which depends on the processing time of that task:
% normal tasks have fixed battery cost
\begin{equation}
\label{cp:ex:7}c_j = bc_j \qquad \forall j \in N
\end{equation}
% Variable battery recharge has a negative cost
% that depends on the length of the recharge
\begin{equation}
\label{cp:ex:8}c_j = \alpha \times p_j \qquad \forall j \in R
\end{equation}
% auxiliary tasks have zero battery cost
\begin{equation}
\label{cp:ex:9}c_j = 0 \qquad \forall j \in A
\end{equation}
Constraints \eqref{cp:ex:7} ensures that the battery cost for normal tasks are fixed as given by the instance. Constraints \eqref{cp:ex:8} sets the battery cost (recharge amount) to be depending on the length of the battery recharge. Lastly the constraints \eqref{cp:ex:9} ensures that auxiliary tasks has no battery cost.\\
\newline
We can then make the battery levels depend on the battery cost of doing the recharges:
\begin{equation}
\label{cp:ex:17}b_{j} = b_{y_j} + c_{y_j} \qquad \forall j \in N{\cup}R{\cup}A_e
\end{equation}
We can achieve both the relations \eqref{cp:ex:16} and \eqref{cp:ex:17} by posing linear constraints.
First let $sb_j$, $pb_j$, $stb_j$ and $bb_j$ be the starting time, processing time, setup time and battery level for the predecessor task $k$ to task $j$.
\begin{equation}
S_{y_j} = sb_j \qquad \forall j \in N{\cup}R{\cup}A_e
\end{equation}
\begin{equation}
P_{y_j} = pb_j \qquad \forall j \in N{\cup}R{\cup}A_e
\end{equation}
\begin{equation}
T_{y_j} = stb_j \qquad \forall j \in N{\cup}R{\cup}A_e
\end{equation}
\begin{equation}
B_{y_j} = bb_j \qquad \forall j \in N{\cup}R{\cup}A_e
\end{equation}
Which can be enforced via the following $\mathtt{element}$ constraints:
\begin{equation}
\label{cp:ex:18}\mathtt{element(S,y_j,sb_j)} \qquad \forall j \in N{\cup}R{\cup}A_e
\end{equation}
\begin{equation}
\label{cp:ex:19}\mathtt{element(P,y_j,pb_j)} \qquad \forall j \in N{\cup}R{\cup}A_e
\end{equation}
\begin{equation}
\label{cp:ex:20}\mathtt{element(T,y_j,stb_j)} \qquad \forall j \in N{\cup}R{\cup}A_e
\end{equation}
\begin{equation}
\label{cp:ex:21}\mathtt{element(B,y_j,bb_j)} \qquad \forall j \in N{\cup}R{\cup}A_e
\end{equation}
Constraints \eqref{cp:ex:18} enforces that $sb_j$ is equal to the start time of the predecessor of task $s_{x_j}$. Constraints \eqref{cp:ex:19}, \eqref{cp:ex:20} and \eqref{cp:ex:21} enforces the same for processing time, setup time and battery level respectively.
\newline
With these, we can now set up relation constraints to enforce the relations \eqref{cp:ex:16} and \eqref{cp:ex:17}:
\begin{equation} \label{cp:ex:22}
\mathtt{rel(sb_j + pb_j + stb_j <= s_j)} \qquad \forall j \in N{\cup}R{\cup}A_e
\end{equation}
\begin{equation} \label{cp:ex:23}
\mathtt{rel(bb_j - c_j = b_j)} \qquad \forall j \in N{\cup}A_e
\end{equation}
\begin{equation} \label{cp:ex:23_r}
\mathtt{rel(bb_j + c_j = b_j)} \qquad \forall j \in R
\end{equation}
To make sure that each battery recharge recharges to full capacity, we impose the following relation:
\begin{equation} \label{cp_ex_23_fix}
\mathtt{rel(c_j = f-bb-j)} \qquad \forall j \in R
\end{equation}
Constraints \eqref{cp:ex:23_r} adds the battery cost of the task instead, since a vehicle would have a higher battery level after completing a battery recharge task.
To ensure that the number of concurrent recharges is not larger than $\rho$, we can use a $\mathtt{cumulative}$ constraint, which use is similar to the $\mathtt{cumulatives}$,
but instead of having multiple resources, the constraint models one resource only.
$\mathtt{cumulative(\rho,S,P,E,U)}$ uses $S$, $P$, $E$ and $U$ as defined in the description of $\mathtt{cumulatives}$.
Since only the battery recharges are constrained by the number of available recharges at a given time, only battery recharge tasks are affected, thus:
\begin{equation}
\label{cp:ex:24}\mathtt{cumulative(\rho,S_R,P_R,E_R,U)}
\end{equation}
Where $S_R$, $P_R$ and $E_R$ denotes the start times, processing times and end times of all battery tasks, respectively.
\newline
We also constraint the makespan to be within the lower and upper bound:
\begin{equation}
\label{cp:ex:25}\mathtt{rel(z \geq \omega)}
\end{equation}
\begin{equation}
\label{cp:ex:26}\mathtt{rel(z \leq U)}
\end{equation}
Like the simplified model, we minimize the makespan $z$, which again is defined by max of all end times.
\begin{equation}
\label{cp:ex:27}\mathtt{rel(z = \max{E})}
\end{equation}
\begin{equation}
\label{cp:ex:28}\mathtt{\min{z}}
\end{equation}
Finally, we show the complete model in Fig. \ref{fig:cpfixed}, with fixed battery recharges:
\begin{figure}
\label{fig:cpfixed}
\caption{CP Model with fixed battery recharge}
\begin{equation*}
\mathtt{\min{z}}
\end{equation*}
\begin{align*}
\mathtt{rel(p_j = pi_j)} &\qquad \forall j \in N\\
\mathtt{rel(p_j \leq \eta)} &\qquad \forall j \in R\\
\mathtt{rel(p_j = 0)} &\qquad \forall j \in A\\
\mathtt{rel(s_j \geq at_j)} &\qquad \forall j \in N\\
\mathtt{rel(s_j + p_j \leq d_j)} &\qquad \forall j \in N{\cup}R{\cup}A\\
\mathtt{rel(b_j = l_{j-n-r})} &\qquad \forall j \in A_s\\
\mathtt{rel(s_j \geq bt_{j-n-r})} &\qquad \forall j \in A_s\\
\mathtt{channel(X,Y)}\\
\mathtt{element(V,x_j,v_j)} &\qquad \forall j \in N{\cup}R{\cup}A_s\\
\mathtt{rel(v_j = j-n-r)} &\qquad \forall j \in A_s\\
\mathtt{rel(v_j = j-n-r-m)} &\qquad forall j \in A_e\\
\mathtt{rel(y_j = A_{e_m})} &\qquad j = A_{s_1}\\
\mathtt{rel(y_j = j+m-1)} &\qquad \forall j \in A_s, j > A_{s_1}\\
\mathtt{circuit(ST,X,T,CC)} &\\
\mathtt{rel(c_j = bc_j)} &\qquad \forall j \in N\\
\mathtt{rel(c_j = \alpha \times p_j)} &\qquad \forall j \in R\\
\mathtt{rel(c_j = f-bb-j)} &\qquad \forall j \in R\\
\mathtt{rel(c_j = 0)} &\qquad \forall j \in A\\
\mathtt{element(S,y_j,sb_j)} &\qquad \forall j \in N{\cup}R{\cup}A_e\\
\mathtt{element(P,y_j,pb_j)} &\qquad \forall j \in N{\cup}R{\cup}A_e\\
\mathtt{element(T,y_j,stb_j)} &\qquad \forall j \in N{\cup}R{\cup}A_e\\
\mathtt{element(B,y_j,bb_j)} &\qquad \forall j \in N{\cup}R{\cup}A_e\\
\mathtt{rel(sb_j + pb_j + stb_j <= s_j)} &\qquad \forall j \in N{\cup}R{\cup}A_e\\
\mathtt{rel(bb_j - c_j = b_j)} &\qquad \forall j \in N{\cup}A_e\\
\mathtt{rel(bb_j + c_j = b_j)} &\qquad \forall j \in R\\
\mathtt{cumulative(\rho,S_R,P_R,E_R,U)} \\
\mathtt{rel(z \geq \omega)} &\\
\mathtt{rel(z \leq U)} &\\
\mathtt{rel(z = \max{E})} &\\
\end{align*}
\end{figure}
\subsection{Flexible Recharging}
In order to only allow flexible battery recharges, the only change needed to the model, is to remove the constraints we only need to remove constraints:
\begin{equation}
\mathtt{rel(c_j = f-bb-j)} \qquad \forall j \in R
\end{equation}
 \eqref{cp:ex:23_fixed}, which constraints the battery cost of all recharges to be equal to the amount needed to arrive at maximum battery capacity after execution of a battery recharge.