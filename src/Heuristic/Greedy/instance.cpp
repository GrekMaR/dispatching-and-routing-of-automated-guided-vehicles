/* 
 * File:   instance.cpp
 * Author: thor
 * 
 * Created on October 8, 2015, 1:43 PM
 */
using namespace std;

#include "instance.h"

void Instance::printProblem() {
    int n = num_tasks;
    int m = num_vehicles;
    cout << "---------------------------------------\n";
    cout << "----     Problem Instance:        ---\n";
    cout << "---------------------------------------\n";
    cout << "num_tasks: " << num_tasks << endl;
    cout << "num_vehicles: " << num_vehicles << endl;
    cout << "Durations: [";
    for (int i = 0; i < n; i++) {
        cout << durations[i] << ", ";
    }
    cout << "]" << endl;
    cout << "Starttimes: [";
    for (int i = 0; i < n; i++) {
        cout << starttimes[i] << ", ";
    }
    cout << "]" << endl;
    cout << "Deadlines: [";
    for (int i = 0; i < n; i++) {
        cout << deadlines[i] << ", ";
    }
    cout << "]" << endl;
    cout << "Setuptimes:" << endl;
    for (int i = 0; i < n+2; i++) {
        cout << "            [";
        for (int j = 0; j < n+2; j++) {
            cout << setuptimes[i*(n+2)+j] << ", ";
        }
        cout << "]" << endl;
    }
    cout << "]" << endl;
    cout << "Battery lower, upper and charge amount: ";
    cout << minmaxbattery[0] << ", ";
    cout << minmaxbattery[1] << ", ";
    cout << minmaxbattery[2] << endl;
    
    cout << "Initial battery: [";
    for (int i = 0; i < m; i++) {
        cout << initialbattery[i] << ", ";
    }
    cout << "]" << endl;
    cout << "Battery cost for each task: [";
    for (int i = 0; i < n; i++) {
        cout << batterycosts[i] << ", ";
    }
    cout << "]" << endl;
    cout << "timelimit: " << timelimit << endl;
    cout << "---------------------------------------\n";
    cout << "---------------------------------------\n";
    
}

Instance::Instance() {
    cout << "new Instance" << endl;
}

Instance::Instance(const char* fileToRead, bool dummyTasks, 
        const char* secondOut, int timelimit, int battery_resources, 
        int goToDepotAtEnd) {
    // read in the line
    cout << "\nReading Instance: " << fileToRead<< "\n";
    string line;
    ifstream a_file (fileToRead);
    vector<int> input;
    if (a_file.is_open())
    {
        string word;        
        while (a_file >> word) {
            if (word.find("-")==string::npos) {
                input.push_back(atoi(word.c_str()));
            }
        }
        a_file.close();
    }
    this->timelimit = timelimit;
    this->secondOut = secondOut;
    this->jsBestOut = jsBestOut;
    this->battery_resources = battery_resources;
    this->goToDepotAtEnd = goToDepotAtEnd;
    num_tasks = input[0];
    num_vehicles = input[1];
    int n = num_tasks;
    int m = num_vehicles;
    cout << "n: " << num_tasks << ", m: " << num_vehicles << endl;
    int total = n+3*m;
    // fill in task processing time
    durations = new int[n];             // duration for all tasks [0,...,n-1]
    setuptimes = new int[(n+2)*(n+2)];  // setuptimes for all combination of tasks
                                        // n x n - matrix
    starttimes = new int[n];
    batterycosts = new int[n];          // battery cost for each task [0,...,n-1]
    initialbattery = new int[m];        // initial battery for each vehicle [0,...,m-1]
    minmaxbattery = new int[3];         // [lowerbound, upperbound, battery recharge rate]
    deadlines = new int[n];             // deadline for each task [0,...,n-1]
    vehicleStartTimes = new int[m];     // min start times for each machine, 
    max = 0;
    // skip input[0,1,2]: Since 
    // 0 = num_tasks,
    // 1 = num_vehicles and
    // 2 = start dumym task duration.
    int index = 3;

    for (int i = 0; i < n; i++) {       // fill in durations
        durations[i] = input[index];
        index++;
    }
    index++;                            // skip past end dummy task duration
    
    if (input[index] > 2147483644) {    // fill in max time
        max = 2147483644;
    } else {
        max = input[index];
    }
    index+=2;                            // skip past start dummy task deadline
    for (int i = 0; i < n; i++) {       // fill in starttimes
        starttimes[i] = input[index];
        index++;
    }
    index+=2;                            // skip past end and start dummy task deadline
    for (int i = 0; i < n; i++) {       // fill in deadlines
        deadlines[i] = input[index];
        index++;
    }
    index++;                            // skip past end dummy task deadline
    int set_dim = (2+n)*(2+n);
    int setup_index = 0;
    for (int i = 0; i < set_dim; i++) { // fill in setuptimes
        setuptimes[setup_index++] = input[index];
        index++;
      
    }
    minmaxbattery[1] = input[index];
    index++;
    minmaxbattery[0] = input[index];
    index++;
    batSpan = minmaxbattery[1]-minmaxbattery[0];
    batteryRechargePerTimeUnit = input[index+1]/input[index];
    // fill in charge amount per charge unit
    minmaxbattery[2] = input[index+1]/input[index];
    chargeTime = input[index];
    index+=2; // skip past charge amount
    
    for (int i = 0; i < m; i++) {       // fill in initial battery
        initialbattery[i] = input[index];
        index++;
    }
    
    index++;                            // skip past start dummy task batterycost
    for (int i = 0; i < n; i++) {       // fill in batterycost for each task
        batterycosts[i] = input[index];
        index++;
    }
    index++;                            // skip past end dummy task batterycost

    for (int i = 0; i < m; i++) {
        if (input.size() > index) {
            vehicleStartTimes[i] = input[index]-1;
            index++;
        } else {
            vehicleStartTimes[i] = -1;
        }   
    }
}

// getters
const int Instance::getNumTasks() {
    return num_tasks;
}

const int Instance::getNumVehicles() {
    return num_vehicles;
}

const int Instance::getTotalTasks() {
    return total_tasks;
}

const int Instance::getMax() {
    return max;
}

const int Instance::getChargetime() {
    return chargeTime;
}

const int Instance::getBatteryResourceAmount() {
    return battery_resources;
}

int* Instance::getDurations() {
    return durations;
}

int* Instance::getSetuptimes() {
    return setuptimes;
}

int* Instance::getStarttimes() {
    return starttimes;
}

int* Instance::getBatterycost() {
    return batterycosts;
}

int* Instance::getInitialBattery() {
    return initialbattery;
}

int* Instance::getMinMaxBattery() {
    return minmaxbattery;
}

/**
 * @return the timelimit of the Instance in seconds
 */
int Instance::getTimelimit() {
    return timelimit;
}

void Instance::setSecondOut(char* output) {
    secondOut = output;
}

const char* Instance::getSecondOut() {
    return secondOut;
}

int* Instance::getVehicleStartTimes() {
    return vehicleStartTimes;
}

int* Instance::getDeadlines() {
    return deadlines;
}
