/* 
 * File:   model.h
 * Author: thor
 *
 * Created on October 8, 2015, 11:42 AM
 */

// This is start of the header guard.
#ifndef MODEL_H
#define	MODEL_H



// This is the content of the .h file, which is where the declarations go

//class Instance;
#include "instance.h"
#include "solution.h"
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <vector>       // std::vector
#include <stdlib.h>     // std::atoi
#include <string>
#include <algorithm>

using namespace std;

class Model {
    
public :
    // create the model
    Model();
    
    // solve it
    void solve();

    // read in a problem instance
    static Solution solveInstance(Instance insToRead);
    
    // getters for problem instance used in model
    int num_tasks();
    int num_machines();
    int total_tasks();
    int max_time();
    int getSetup(int pred, int succ);
    // The following indented variables are used during the solving stage of
    // the model. And changes from iteration (one time interval to the next)
    // indicates until which time interval the vehicle is busy
    int* vehicleBusyTime;
    // The current battery of the vehicle
    int* vehicleBattery;
    // States which task was previously scheduled on the machine
    int* vehiclePredecessorTask;
    int* vehiclePredecessorTaskBattery;
    // The number of scheduled tasks so far
    int numScheduledTasks;
    // If a task is scheduled or not
    bool* taskScheduled;
    // num tasks per machine
    int* vehicleTaskNum;
    // The timelimit for the model
    int timelimit;
    int* vehicleHasFullBattery;
};

// This is the end of the header guard
#endif	/* MODEL_H */

