/* 
 * File:   sTask.h
 * Author: thor
 *
 * Created on February 28, 2016, 11:49 AM
 */

#ifndef STASK_H
#define	STASK_H

using namespace std;

#include "iTask.h"

class STask {
private:
    int id;
    int startTime;
    int duration;
    int endTime;
    int setupTime;
    int batterycost;
    int vehicleId;
    int successorId; // -1 = no successor
    int successorIndex;
    int batteryLevelAfterExecution;
    int arrivalTime;
    int deadline;
    int setupFromDepot;
    int setupToDepot;
    
    vector<int> allSetupTimes;
    
    static int idCounter;
    
public: 
    int getId(){ return id; }
    int getStartTime(){ return startTime; }
    double getStartTimeAsDouble() { return startTime; }
    int getDuration(){ return duration; }
    double getDurationAsDouble() { return duration; }
    int getEndTime(){ return endTime; }
    double getEndTimeAsDouble() { return endTime; }
    int getSetupTime(){ return setupTime; }
    double getSetupTimeAsDouble() { return setupTime; }
    int getBatteryCost(){ return batterycost; }
    int getVehicleId(){ return vehicleId; }
    int getSuccessorId(){ return successorId; }
    int getBatteryLevelAfterExecution() { return batteryLevelAfterExecution; }
    int getArrivalTime() { return arrivalTime; }
    int getDeadline() { return deadline; }
    double getDeadlineAsDouble() { return deadline; }
    int getSuccessorIndex() { return successorIndex; }
    int getSetupFromDepot() { return setupFromDepot; }
    int getSetupToDepot() { return setupToDepot; }
    int getSetupTimeTo(int index);
    
    void setStartTime(int);
    void setDuration(int);
    void setEndTime(int);
    void setSetupTime(int);
    void setBatterycost(int);
    void setVehicleId(int);
    void setSuccessorId(int);
    void setId(int);
    void setBatteryLevelafterExecution(int);
    void setArrivalTime(int);
    void setSetupTimes(int, int);
    void setDeadline(int);
    void setSuccessorIndex(int);
    void setSetupFromDepot(int);
    void setSetupToDepot(int);
    void setSetupTimes(vector<int>);
    void printTask(ostream&);
    void printTaskHeader(ostream&);
    void printAllSetupTimes(ostream&);
    
    STask();
    STask(ITask);
    // start, duration, deadline, endtime, setuptime, batterycost, vehicleId, successorId
    STask(int, int, int, int, int, int, int, int);
    STask(const STask& orig);
    virtual ~STask();


};

#endif	/* STASK_H */

