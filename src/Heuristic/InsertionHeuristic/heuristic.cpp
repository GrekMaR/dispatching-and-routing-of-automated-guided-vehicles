/* 
 * File:   heuristic.cpp
 * Author: thor
 * 
 * Created on February 28, 2016, 11:50 AM
 */

#include "heuristic.h"

Heuristic::Heuristic(Instance ins) {
    this->ins = ins;
    Solution sol(this->ins);
    this->sol = sol;
    unroutedTasks = this->ins.getTasks();        
    init_a1 = 0.5;
    init_a2 = 0.5;
    mu = 0.5;
    a1 = 0.3;
    a2 = 0.5;
    a3 = 0.2;
    lambda = 0.5;
    b1 = 0.5;
    b2 = 0.5;
}

Heuristic::Heuristic() {
    Solution sol;
    //cout << "-constructing heuristic with default constructor" << endl;
    init_a1 = 0.5;
    init_a2 = 0.5;
    mu = 0.5;
    a1 = 0.3;
    a2 = 0.5;
    a3 = 0.2;
    lambda = 0.5;
    b1 = 0.5;
    b2 = 0.5;
}

Heuristic::Heuristic(const Heuristic& orig) {
    //cout << "-heuristic Copy constructor" << endl;
    this->ins = orig.ins;
    this->sol = orig.sol;
    unroutedTasks = orig.unroutedTasks;
}

Heuristic::~Heuristic() {
}

/**
 * returns the list of routes to be initialized in the order,
 * such that the first value has the highest value
 * computed by 'choose route'
 * @return 
 */
vector<int> Heuristic::getInitializationOrder() {
    vector<SVehicle> routes = sol.getVehicles();
    vector<int> routeOrder;
    for (int i = 0; i < sol.getVehicles().size(); i++) {
        int routeNum = chooseRoute(routes);
        routeOrder.push_back(routes[routeNum].getId());
        routes.erase(routes.begin()+routeNum);
    }
    return routeOrder;
}


void Heuristic::initializeRoutes() {
    vector<int> routeInitializationOrder = getInitializationOrder();
    switch(initHeuristic) {
        case 0: // highest setup time to routed tasks
            // initialize all routes
            for (int i = 0; i < routeInitializationOrder.size(); i++) {
                int bestTaskIndex = 0;
                int maxMinSetup = unroutedTasks[0].getSetupToDepot();
                // if first route
                if (i == 0) {
                    // then the only setup is from depot to task j
                    for (int j = 1; j < unroutedTasks.size(); j++) {
                        ITask task = unroutedTasks.at(j);
                        // if setup is lower then previous
                        if (maxMinSetup < task.getSetupFromDepot()) {
                            maxMinSetup = task.getSetupFromDepot();
                            bestTaskIndex = j;
                        }
                    }
                } else {
                    // find task with max min setup time to all
                    // other tasks in the route
                    for (int j = 0; j < unroutedTasks.size(); j++) {
                        ITask task = unroutedTasks[j];
                        int currentMinSetup = task.getSetupToDepot();
                        // find min setup
                        for (int k = 0; k < routedTasks.size(); k++) {
                            ITask routedTask = routedTasks[k];
                            if (currentMinSetup < task.getSetupTime(
                                    routedTask.getId())) {
                                currentMinSetup = task.getSetupTime(
                                    routedTask.getId());
                            }
                        }
                        if (currentMinSetup > maxMinSetup) {
                            maxMinSetup = currentMinSetup;
                            bestTaskIndex = j;
                        }
                    }
                }
                routedTasks.push_back(unroutedTasks[bestTaskIndex]);
                STask bestTask = STask(unroutedTasks.at(bestTaskIndex));
                sol.getVehicle(routeInitializationOrder[i]).insertTaskIntoSchedule(bestTask,0);
                unroutedTasks.erase(unroutedTasks.begin()+bestTaskIndex);
                if (unroutedTasks.size() == 0) {
                    return;
                }
            }
            break;
        case 1: // earliest deadline
            // initialize all routes
            for (int i = 0; i < routeInitializationOrder.size(); i++) {
                // set initial best task to be 0
                int bestTaskIndex = 0;
                int minDeadline = unroutedTasks[0].getDeadline();
                // find earliest deadline
                for (int j = 1; j < unroutedTasks.size(); j++) {
                    if (minDeadline > unroutedTasks[j].getDeadline()) {
                        minDeadline = unroutedTasks[j].getDeadline();
                        bestTaskIndex = j;
                    }
                }
                routedTasks.push_back(unroutedTasks[bestTaskIndex]);
                STask bestTask = STask(unroutedTasks.at(bestTaskIndex));
                sol.getVehicle(routeInitializationOrder[i]).insertTaskIntoSchedule(bestTask,0);
                unroutedTasks.erase(unroutedTasks.begin()+bestTaskIndex);
                if (unroutedTasks.size() == 0) {
                    return;
                }
            }
            break;
        case 2: // highest battery consumption
            // initialize all routes
            for (int i = 0; i < routeInitializationOrder.size(); i++) {
                // set initial best task to be 0
                int bestTaskIndex = 0;
                int highestBatteryConsumption = unroutedTasks[0].getBatteryCost();
                // find earliest deadline
                for (int j = 1; j < unroutedTasks.size(); j++) {
                    if (highestBatteryConsumption > unroutedTasks[j].getBatteryCost()) {
                        highestBatteryConsumption = unroutedTasks[j].getBatteryCost();
                        bestTaskIndex = j;
                    }
                }
                routedTasks.push_back(unroutedTasks[bestTaskIndex]);
                STask bestTask = STask(unroutedTasks.at(bestTaskIndex));
                sol.getVehicle(routeInitializationOrder[i]).insertTaskIntoSchedule(bestTask,0);
                unroutedTasks.erase(unroutedTasks.begin()+bestTaskIndex);
                if (unroutedTasks.size() == 0) {
                    return;
                }
            }
            break;
        case 3: // minimum weighted combination of highest setup time
                // to routed tasks and high battery consumption
            for (int i = 0; i < routeInitializationOrder.size(); i++) {
                int bestTaskIndex = 0;
                double maxMinSetup = init_a1*unroutedTasks[0].getSetupToDepot()
                    + init_a2 * unroutedTasks[0].getBatteryCost();
                double currentBest;
                // if first route
                if (i == 0) {
                    // then the only setup is from depot to task j
                    for (int j = 1; j < unroutedTasks.size(); j++) {
                        ITask task = unroutedTasks.at(j);
                        currentBest = init_a1*unroutedTasks[j].getSetupToDepot()
                    + init_a2 * unroutedTasks[j].getBatteryCost();
                        if (maxMinSetup < currentBest) {
                            maxMinSetup = currentBest;
                            bestTaskIndex = j;
                        }
                    }
                } else {
                    // find task with max min setup time to all
                    // other tasks in the route
                    for (int j = 0; j < unroutedTasks.size(); j++) {
                        ITask task = unroutedTasks[j];
                        int currentMinSetup = task.getSetupToDepot();
                        // find min setup
                        for (int k = 0; k < routedTasks.size(); k++) {
                            ITask routedTask = routedTasks[k];
                            if (currentMinSetup < task.getSetupTime(
                                    routedTask.getId())) {
                                currentMinSetup = task.getSetupTime(
                                    routedTask.getId());
                            }
                        }
                        currentBest = init_a1 * currentMinSetup
                                + init_a2 * task.getBatteryCost();
                        if (currentBest > maxMinSetup) {
                            maxMinSetup = currentBest;
                            bestTaskIndex = j;
                        }
                    }
                }
                routedTasks.push_back(unroutedTasks[bestTaskIndex]);
                STask bestTask = STask(unroutedTasks.at(bestTaskIndex));
                sol.getVehicle(routeInitializationOrder[i]).insertTaskIntoSchedule(bestTask,0);
                unroutedTasks.erase(unroutedTasks.begin()+bestTaskIndex);
                if (unroutedTasks.size() == 0) {
                    return;
                }
            }
            break;
    }
}

int Heuristic::chooseRoute(vector<SVehicle> routes) {
    int bestRouteIndex;
    int bestValue;
    int currentValue;
    vector<int> bestIndexes;
    switch(routeHeuristic) {
        case 0: // Highest battery capacity at end task
            bestRouteIndex = 0;
            bestValue = routes[0].getEndTask().getBatteryLevelAfterExecution();
            bestIndexes.push_back(0);
            for (int i = 1; i < routes.size(); i++) {
                currentValue = routes[i].getEndTask().getBatteryLevelAfterExecution();
                if (bestValue < currentValue) {
                    bestValue = currentValue;
                    bestRouteIndex = i;
                    bestIndexes.clear();
                    bestIndexes.push_back(i);
                } else if (bestValue == currentValue) {
                    bestIndexes.push_back(i);
                }
            }
            break;
        case 1: // Earliest end date of end task
            bestRouteIndex = 0;
            bestValue = routes[0].getEndTask().getEndTime();
            bestIndexes.push_back(0);
            for (int i = 1; i < routes.size(); i++) {
                currentValue = routes[i].getEndTask().getEndTime();
                if (bestValue > currentValue) {
                    bestValue = currentValue;
                    bestRouteIndex = i;
                    bestIndexes.clear();
                    bestIndexes.push_back(i);
                } else if (bestValue == currentValue) {
                    bestIndexes.push_back(i);
                }
            }
            break;
        case 2: // Route with least number of tasks
            bestRouteIndex = 0;
            bestValue = routes[0].getTasks().size();
            bestIndexes.push_back(0);
            for (int i = 0; i < routes.size(); i++) {
                currentValue = routes[i].getTasks().size();
                if (bestValue > currentValue) {
                    bestValue = currentValue;
                    bestRouteIndex = i;
                    bestIndexes.clear();
                    bestIndexes.push_back(i);
                } else if (bestValue == currentValue) {
                    bestIndexes.push_back(i);
                }
            }
            break;
    }
    int randValue = rand() % bestIndexes.size();
    int index = bestIndexes[randValue];
    return index;
}

vector<int> Heuristic::chooseInsertionPoint(SVehicle r) {
    vector<int> bestInsertionPoints;
    int bestInsertionPoint = -1;
    double bestValue = 0;
    double currentValue = -1;
    STask j = r.getStartTask();
    STask u(unroutedTasks[0]);
    switch(insertionHeuristic) {
        case 0: // Most benefit in setup time
            for (int i = 0; i < unroutedTasks.size(); i++) {
                u = STask(unroutedTasks[i]);
                for (int k = 0; k < r.getTasks().size(); k++) {
                    j = r.getTask(k-1);
                    if (j.getSuccessorIndex() >= 0) {
                        currentValue = a1*criteriaAux1(j,u,r.getTask(j.getSuccessorIndex()))
                                + a2*criteriaAux2(j,u,r);
                    } else {
                        currentValue = a1*criteriaAux1(j,u,r.getEndTask())
                                + a2*criteriaAux2(j,u,r);
                    }
                    if (currentValue > bestValue) {
                        bestValue = currentValue;
                        bestInsertionPoint = k;
                    }
                }
                bestInsertionPoints.push_back(bestInsertionPoint);
            }
            break;
        case 1: // consider deadlines
            for (int i = 0; i < unroutedTasks.size(); i++) {
                u = STask(unroutedTasks[i]);
                for (int k = 0; k < r.getTasks().size(); k++) {
                    j = r.getTask(k-1);
                    if (j.getSuccessorIndex() >= 0) {
                        currentValue = a1*criteriaAux1(j,u,r.getTask(j.getSuccessorIndex()))
                                + a2*criteriaAux2(j,u,r) + a3*criteriaAux3(u);
                    } else {
                        currentValue = a1*criteriaAux1(j,u,r.getEndTask())
                                + a2*criteriaAux2(j,u,r) + a3*criteriaAux3(u);
                    }
                    if (currentValue > bestValue) {
                        bestValue = currentValue;
                        bestInsertionPoint = k;
                    }
                }
                bestInsertionPoints.push_back(bestInsertionPoint);
            }
            break;
    }
    return bestInsertionPoints;
}

int Heuristic::chooseTask(SVehicle r, vector<int> insertionPlaces) {
    int bestTaskId = 0;
    double bestValue = 0;
    double currentValue = 0;
    STask j = r.getTask(insertionPlaces[0]);
    STask u(unroutedTasks.at(0));
    switch(taskHeuristic) {
        case 0: // savings in setup time
            bestValue = a1*criteriaAux1(j,u,r.getTask(j.getSuccessorIndex()))
                    + a2*criteriaAux2(j,u,r);
            for (int i = 1; i < unroutedTasks.size(); i++) {
                u = unroutedTasks.at(i);
                j = r.getTask(insertionPlaces[i]);
                currentValue = a1*criteriaAux1(j,u,r.getTask(j.getSuccessorIndex()))
                    + a2*criteriaAux2(j,u,r);
                if (currentValue > bestValue) {
                    bestValue = currentValue;
                    bestTaskId = i;
                }
            }
            break;
        case 1: // also saving in setup time
            bestValue = lambda * r.getStartTask().getSetupTimeTo(u.getId())
                    - (a1*criteriaAux1(j,u,r.getTask(j.getSuccessorIndex()))
                    + a2*criteriaAux2(j,u,r));
            for (int i = 1; i < unroutedTasks.size(); i++) {
                u = unroutedTasks.at(i);
                j = r.getTask(insertionPlaces[i]);
                currentValue = lambda * r.getStartTask().getSetupTimeTo(u.getId())
                    - (a1*criteriaAux1(j,u,r.getTask(j.getSuccessorIndex()))
                    + a2*criteriaAux2(j,u,r));
                if (currentValue > bestValue) {
                    bestValue = currentValue;
                    bestTaskId = i;
                }
            }
            break;
            
        case 2: // combination of min route distance and times
            bestValue = b1 * r.getTotalRouteDistance(j,u) + b2 * r.getTotalRouteTime(j,u);
            for (int i = 1; i < unroutedTasks.size(); i++) {
                u = unroutedTasks.at(i);
                j = r.getTask(insertionPlaces[i]);
                currentValue = b1 * r.getTotalRouteDistance(j,u) + b2 * r.getTotalRouteTime(j,u);
                if (currentValue < bestValue) {
                    bestValue = currentValue;
                    bestTaskId = i;
                }
            }
            break;
    }
    return bestTaskId;
}


Solution Heuristic::runHeuristic() {
    // initialize the routes at first
    if (initHeuristic >= 0) {
        initializeRoutes();
    }
    int route, taskIndex;
    vector<int> insertionPoints;
    while (unroutedTasks.size() > 0) {
        route = chooseRoute(sol.getVehicles());
        SVehicle v = sol.getVehicle(route);
        insertionPoints = chooseInsertionPoint(v);
        taskIndex = chooseTask(v,insertionPoints);
        if (route == -1 || taskIndex == -1) {
            // solution infeasible
            sol.setFeasibility(false);
            break;
        } else {
            routedTasks.push_back(unroutedTasks[taskIndex]);
            STask task = STask(unroutedTasks.at(taskIndex));
            sol.getVehicle(route).insertTaskIntoSchedule(task,insertionPoints[taskIndex]+1);
            unroutedTasks.erase(unroutedTasks.begin()+taskIndex);
        }
    }
    // insert battery rechages and try to make solution feasible
    int res = batteryRechargeInsertion();
    if (res != 0) { // not feasible
        sol.setFeasibility(false);
    } else { // feasible
        int ms = 0;
        for (int i = 0; i < sol.getVehicles().size(); i++) {
            if (ms < sol.getVehicle(i).getMakespan()) {
                ms = sol.getVehicle(i).getMakespan();
            }
        }
        sol.setMakespan(ms);
    }
    return sol;  
}


void Heuristic::setInitHeuristic(int heuristic) {
    initHeuristic = heuristic;
    if (heuristic > 3) {
        initHeuristic = 0;
    }
    
}

void Heuristic::setInsertHeuristic(int heuristic) {
    insertionHeuristic = heuristic;
    if (heuristic < 0 || heuristic > 2) {
        insertionHeuristic = 0;
    }
}

void Heuristic::setRouteHeuristic(int heuristic) {
    routeHeuristic = heuristic;
    if (heuristic < 0 || heuristic > 1) {
        routeHeuristic = 0;
    }    
}

void Heuristic::setTaskHeuristic(int heuristic) {
    taskHeuristic = heuristic;
    if (heuristic < 0 || heuristic > 2) {
        taskHeuristic = 0;
    }    
}

/**
 * -1 = no feasible solution
 * 1 = feasible solution (Need to check for deadlines afterwards
 * @return 
 */
int Heuristic::batteryRechargeInsertion() {
    for (int k = 0; k < sol.getVehicles().size(); k++) {
        SVehicle& i = sol.getVehicle(k);
        if (i.getTasks().size() == 0) {
            continue;
        }
        STask j = i.getTask(0);
        STask prevTask = i.getStartTask();
        int q = i.getMinBattery();
        int lastRechargeIndex = -1;
        int jIndex = 0;
        while (i.isInfeasible() && j.getId() != i.getEndTask().getId()) {
            if (j.getBatteryLevelAfterExecution() < q) {
                // insert r after j
                // update start, end time, battery capacities of all
                // successive tasks
                lastRechargeIndex = jIndex;
                STask r = getNewBatteryTask(j,
                        i.getEndTask().getBatteryLevelAfterExecution(), 
                        prevTask.getEndTime()+prevTask.getSetupToDepot(), 0);
                i.insertTaskIntoSchedule(r,jIndex);
                STask s = i.getTask(jIndex);
                rechargeDuration rDur;
                rDur.startTime = s.getStartTime();
                rDur.endTime = s.getEndTime();
                recharges.push_back(rDur);
            }
            prevTask = j;
            // go to end task if needed
            if (j.getSuccessorIndex() > 0) {
                jIndex = j.getSuccessorIndex();
                j = i.getTask(j.getSuccessorIndex());
            } else {
                j = i.getEndTask();
            }
        }
        jIndex = i.getTasks().size();
        

        if (ins.getGoToDoDepotAtEnd() == 1 && i.getTasks().size() > 0) {
            cout << endl;
            STask r = getNewBatteryTask(j,
                    i.getEndTask().getBatteryLevelAfterExecution()-(i.getMaxBattery()-i.getMinBattery()), 
                    prevTask.getEndTime()+prevTask.getSetupToDepot(), 1);
            i.insertTaskIntoSchedule(r,jIndex);
            STask s = i.getTask(jIndex);
            rechargeDuration rDur;
            rDur.startTime = s.getStartTime();
            rDur.endTime = s.getEndTime();
            recharges.push_back(rDur);
        }
        if (i.isInfeasible()) {
            return -1; // terminate with no feasible solution
        }
    }
    return 0;
    
}
double Heuristic::weightedSetupAndBatteryConsumption(ITask& task) {
    return task.getSetupFromDepot() * init_a1 + task.getBatteryCost() * init_a2;
}


double Heuristic::criteriaAux1(STask& j, STask& u, STask& k) {
    return j.getSetupTimeTo(u.getId()) + u.getSetupTimeTo(k.getId()) -mu * j.getSetupTimeTo(k.getId());
}

/**
 * Try to minimze push forward for an insertion of task u between task j and k
 * @param u
 * @param j
 * @param i
 * @return 
 */
double Heuristic::criteriaAux2(STask& j, STask& u, SVehicle& i) {
    int start_succ_j;
    int start_succ_u;
    // j is end task, set start of next to be its end time
    if (j.getSuccessorIndex() < 0) {
        start_succ_j = j.getEndTime();
    } else {
        start_succ_j = i.getTask(j.getSuccessorIndex()).getStartTime();
    }
    // u should start when j and setup is done
    start_succ_u = j.getEndTime()+j.getSetupTimeTo(u.getId());
    return start_succ_u-start_succ_j;
}

/**
 * calculate span between start time of u and deadline u.
 * @param u
 * @return 
 */
double Heuristic::criteriaAux3(STask& u) {
    return u.getDeadline()-u.getStartTime();
}

bool Heuristic::FeasibleInsertion(STask& j, STask& u) {
    int earliestPossibleStartOfU = j.getEndTime()+j.getSetupTimeTo(u.getId());
    return ((earliestPossibleStartOfU - u.getArrivalTime())
            >= 0) && (earliestPossibleStartOfU + u.getDuration() <= u.getDeadline());
}

bool Heuristic::isInputFeasible() {
    int batterySpan = ins.getVehicle(0).getMaxBattery()-ins.getVehicle(0).getMinBattery();
    for (int i = 0; i < ins.getTasks().size(); i++) {
        ITask t = ins.getTask(i);
        if (t.getBatteryCost() > batterySpan // task cost too much
                // not possible to process in time
                || (t.getMinStartTime() + t.getDuration()) > t.getDeadline())
        {
            return false;
        }
    }
    return true;
}

/**
 * 
 * @param j - the task in which we insert the recharge after
 * @param batteryLevelAtEndTask - battery level at end task, such that we only recharge the minimum amount needed at the end
 * @param minStartWithSetup - the minimum start time of current successor to task j
 * @return r - the battery recharge task constructed based on params
 */
STask Heuristic::getNewBatteryTask(STask& j, int batteryLevelAtEndTask, int minStartWithSetup, int ToMax) {
    STask r;
    int rechargeAmount = sol.getVehicle(0).getMaxBattery()-j.getBatteryLevelAfterExecution()-j.getBatteryCost();
    int rechargeNeeded = sol.getVehicle(0).getMinBattery()-batteryLevelAtEndTask;
    // if variable battery recharge and we can recharge less than to max
    if ((rechargeNeeded < rechargeAmount && rt == VARIABLE_BATTERY_RECHARGE) || ToMax == 1) {
        rechargeAmount = rechargeNeeded;
    }

    r.setDeadline(ins.getUpperbound());
    r.setBatterycost(-rechargeAmount);
    double rechargeTime = rechargeAmount/ins.getRechargePerTimeUnit();
    r.setDuration(rechargeTime);
    r.setId(-4);
    r.setSetupFromDepot(0);
    r.setSetupToDepot(0);
    r.setSetupTimes(ins.getSetupTimesForTask(0));

    int minStart = j.getStartTime()-j.getSetupTime();
    if (minStart < minStartWithSetup) {
        minStart = minStartWithSetup;
    }
    int numOverLaps = recharges.size();
    int prevMinEnd = 0;
    int minEndTime = ins.getUpperbound();
    // consider overlaps between recharges
    while (numOverLaps >= ins.getNumBatteryResources()) {
        numOverLaps = 0;
        for (int i = 0; i < recharges.size(); i++) {
            rechargeDuration rd = recharges[i];
            if ((rd.startTime <= minStart && rd.endTime > minStart)
                    || (rd.startTime >= minStart && rd.startTime <  minStart + rechargeTime)) {
                if (minEndTime > rd.endTime || rd.endTime > prevMinEnd) {
                    minEndTime = rd.endTime;
                }
                numOverLaps++;
            }
        }
        if (numOverLaps >= ins.getNumBatteryResources()) {
            prevMinEnd = minEndTime;
            minStart = minEndTime;
        }
    }
    r.setArrivalTime(minStart);
    return r;
}

void Heuristic::setElapsedTime(double time) {
    sol.setElapsedTime(time);
}


void Heuristic::setMu(double value) {
    mu = value;
}

void Heuristic::setAlphas(double a1, double a2, double a3) {
    this->a1 = a1;
    this->a2 = a2;
    this->a3 = a3;
}

void Heuristic::setLambda(double l) {
    lambda = l;
}

void Heuristic::setBetas(double b1, double b2) {
    this->b1 = b1;
    this->b2 = b2;
}

void Heuristic::setInsertionParams(double ia1, double ia2) {
    this->init_a1 = ia1;
    this->init_a2 = ia2;
}
/**
 * 0 = variable battery recharge
 * 1 = always recharge to max
 * @param type
 */
void Heuristic::setRechargeType(int type) {
    if (type == 0) {
        this->rt = VARIABLE_BATTERY_RECHARGE;
    } else {
        this->rt = FIXED_BATTERY_RECHARGE;
    }
}