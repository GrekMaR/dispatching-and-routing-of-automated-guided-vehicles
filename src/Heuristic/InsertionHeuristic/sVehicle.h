/* 
 * File:   sVehicle.h
 * Author: thor
 *
 * Created on February 28, 2016, 11:49 AM
 */

#ifndef SVEHICLE_H
#define	SVEHICLE_H

#include <vector>
#include <iostream>

#include "sTask.h"

using namespace std;

class SVehicle {
private:
    int id;
    int makespan;
    int initBattery;
    vector<int> successorList;
    vector<STask> tasks;
    STask startTask;
    STask endTask;
    int busyTime; // arrival time of vehicle
    int minBattery;
    int maxBattery;
    int goToDepotAtEnd;
    
    static int idCounter;
    
public:
    int getId(){ return id; }
    int getMakespan(){ return endTask.getEndTime(); }
    vector<int>& getSuccessorList(){ return successorList; }
    vector<STask>& getTasks(){ return tasks; }
    STask& getTask(int index);
    int getSuccessor(int index){ return successorList.at(index); }
    int getInitBattery() { return initBattery; }
    int getBusyTime() { return busyTime; }
    STask& getStartTask() { return startTask; }
    STask& getEndTask() { return endTask; }
    int getMinBattery() { return minBattery; }
    int getMaxBattery() { return maxBattery; }
    
    int getTotalRouteDistance(STask&, STask&);
    int getTotalRouteTime(STask&, STask&);
    void setMakespan(int);
    void addSuccessor(int);
    void setSuccessor(int, int);
    void addTask(STask);
    void setTask(int, STask);
    void setId(int);
    void setInitBattery(int);
    void setEndTask(STask);
    void setStartTask(STask);
    void insertTaskIntoSchedule(STask task, int afterPos);
    void printVehicle(ostream&);
    void printTasks(ostream&);
    bool isInfeasible();
    void setMinBattery(int);
    void setMaxBattery(int);
    void setGoToDepotAtEnd(int);
    
    SVehicle();
    SVehicle(int);
    SVehicle(const SVehicle& orig);
    virtual ~SVehicle();


};

#endif	/* SVEHICLE_H */

