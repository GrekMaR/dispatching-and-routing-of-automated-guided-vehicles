/* 
 * File:   iVehicle.cpp
 * Author: thor
 * 
 * Created on February 28, 2016, 11:44 AM
 */

#include "iVehicle.h"


int IVehicle::idCounter = 0;

IVehicle::IVehicle() 
{
    id = idCounter++;
}

// copy constructor
IVehicle::IVehicle(const IVehicle& orig) 
{
    this->id = orig.id;
    this->minStart = orig.minStart;
    this->initialBattery = orig.initialBattery;
    this->minBattery = orig.minBattery;
    this->maxBattery = orig.maxBattery;
}

IVehicle::~IVehicle() 
{
    //this->id = idCounter++;
}
// min start, initial battery, min battery, max battery
IVehicle::IVehicle(int start, int initialBattery, int minBattery, int maxBattery) 
{
    this->id = idCounter;
    this->minStart = start;
    this->initialBattery = initialBattery;
    this->minBattery = minBattery;
    this->maxBattery = maxBattery;
}

void IVehicle::setMinStart(int time) 
{
    minStart = time;
}

void IVehicle::setInitialBattery(int capacity)
{
    initialBattery = capacity;
}

void IVehicle::setMinBattery(int min)
{
    minBattery = min;
}

void IVehicle::setMaxBattery(int max)
{
    maxBattery = max;
}

