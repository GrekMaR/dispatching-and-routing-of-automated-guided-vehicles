/* 
 * File:   sVehicle.cpp
 * Author: thor
 * 
 * Created on February 28, 2016, 11:49 AM
 */

#include <stdlib.h>

#include "sVehicle.h"

int SVehicle::idCounter = 0;

SVehicle::SVehicle() 
{
    this->id = idCounter++;
}

SVehicle::SVehicle(int) 
{
    this->id = idCounter++;
}
SVehicle::SVehicle(const SVehicle& orig) {
    this->id = orig.id;
    this->busyTime = orig.busyTime;
    this->endTask = orig.endTask;
    this->initBattery = orig.initBattery;
    this->makespan = orig.makespan;
    this->startTask = orig.startTask;
    this->successorList = orig.successorList;
    this->tasks = orig.tasks;
    this->minBattery = orig.minBattery;
    this->maxBattery = orig.maxBattery;
    this->goToDepotAtEnd = orig.goToDepotAtEnd;
}

SVehicle::~SVehicle() 
{
}

void SVehicle::setMakespan(int makespan) 
{
    this->makespan = makespan;
}

void SVehicle::addSuccessor(int successor) 
{
    this->successorList.push_back(successor);
}

void SVehicle::setSuccessor(int index, int successor) 
{
    this->successorList.at(index) = successor;
}

void SVehicle::addTask(STask task) 
{
    this->tasks.push_back(task);
}

void SVehicle::setTask(int index, STask task) 
{
    this->tasks.at(index) = task;
}

void SVehicle::setId(int id) 
{
    this->id = id;
}

void SVehicle::setInitBattery(int batteryCapacity) 
{
    this->initBattery = batteryCapacity;
}

void SVehicle::setEndTask(STask sTask) 
{
    this->endTask = sTask;
}

void SVehicle::setStartTask(STask sTask) 
{
    this->startTask = sTask;
}

void SVehicle::insertTaskIntoSchedule(STask task, int insertPos) {
    STask beforeTask = getStartTask();
    if (insertPos > 0){
        beforeTask = tasks.at(insertPos-1);
    }
    STask afterTask = getEndTask();
    if (beforeTask.getSuccessorIndex() >= 0) {
        afterTask = tasks.at(beforeTask.getSuccessorIndex());
    } else {
        afterTask = getEndTask();
    }
    // ------- Update predecessor task --------
    beforeTask.setSetupTime(beforeTask.getSetupTimeTo(task.getId()));
    beforeTask.setSuccessorId(task.getId());
    beforeTask.setSuccessorIndex(insertPos);
    if (beforeTask.getId() == -1) {
        beforeTask.setSetupTime(task.getSetupFromDepot());
        startTask = beforeTask;
    } else {
        tasks[insertPos-1] = beforeTask;
    }
    // ------- Update Inserted task --------
    // start time
    int startTime = beforeTask.getEndTime()+beforeTask.getSetupTime();
    if (task.getArrivalTime() > beforeTask.getEndTime()+beforeTask.getSetupTime()) {
        startTime = task.getArrivalTime();
    }
    task.setStartTime(startTime);
    // batteryLevel
    task.setBatteryLevelafterExecution(
    beforeTask.getBatteryLevelAfterExecution()-task.getBatteryCost());
    // end time
    task.setEndTime(task.getStartTime()+task.getDuration());
    // setup time to next task
    task.setSuccessorId(afterTask.getId());
    if (afterTask.getId() != -2) { // normal task
        task.setSuccessorIndex(insertPos+1);
        task.setSuccessorId(afterTask.getId());
        task.setSetupTime(task.getSetupTimeTo(afterTask.getId()));
    } else { // end task
        task.setSuccessorIndex(-2);
        task.setSuccessorId(-2);
        if (goToDepotAtEnd == 1) {
            task.setSetupTime(task.getSetupToDepot());    
        } else {
            task.setSetupTime(0);
        }
        
    }
    // Now insert the task
    if (tasks.size() <= insertPos) {
        tasks.push_back(task);
    } else {
        tasks.insert(tasks.begin()+insertPos, task);
    }    
    // ------- Update Successors ---------
    // Check if it causes a push forward
    int pushForward = (task.getEndTime()+task.getSetupTime())
            -afterTask.getStartTime();
    int next = task.getSuccessorIndex();
    int current = insertPos;
    beforeTask = task;
    while(afterTask.getId() != -2) {
        // fix start, end and battery
        afterTask.setStartTime(afterTask.getStartTime()+pushForward);
        afterTask.setEndTime(afterTask.getEndTime()+pushForward);
        afterTask.setBatteryLevelafterExecution(
                beforeTask.getBatteryLevelAfterExecution()
                -afterTask.getBatteryCost());
        if (afterTask.getSuccessorIndex() == next) {
            afterTask.setSuccessorIndex(next+1);
        }
        tasks.at(next) = afterTask;
        next++;
        beforeTask = afterTask;
        if (next < tasks.size()) { // still not end task
            afterTask = tasks.at(next);
        } else { // next is end task
            afterTask = getEndTask();
        }
        pushForward = (beforeTask.getEndTime()+beforeTask.getSetupTime())
                -afterTask.getStartTime();
    }
    afterTask.setStartTime(afterTask.getStartTime()+pushForward);
    afterTask.setEndTime(afterTask.getEndTime()+pushForward);
    afterTask.setBatteryLevelafterExecution(
            beforeTask.getBatteryLevelAfterExecution()
            -afterTask.getBatteryCost());
    setEndTask(afterTask);
}

/**
 * returns total route time, which is end time of end task
 * minus busy time of vehicle
 * @return 
 */
int SVehicle::getTotalRouteTime(STask& j, STask& u) {
    return endTask.getEndTime()-busyTime;
}

/**
 * Returns the total distance of the route, which is sum of setup times
 * @return 
 */
int SVehicle::getTotalRouteDistance(STask& j, STask& u) {
    int totalDistance = 0;
    if (j.getId() == -1) {
        totalDistance += startTask.getSetupTimeTo(u.getId());
        totalDistance += u.getSetupTimeTo(startTask.getSuccessorId());
    } else {
        totalDistance += startTask.getSetupTime();
    }
    for (int i = 0; i < tasks.size(); i++) {
        if (tasks[i].getId() == j.getId()) {
            totalDistance += tasks[i].getSetupTimeTo(u.getId());
            totalDistance += u.getSetupTimeTo(tasks[i].getSuccessorId());
        } else {
            totalDistance += tasks.at(i).getSetupTime();
        }
    }
    return totalDistance;
}

bool SVehicle::isInfeasible() {
    return endTask.getBatteryLevelAfterExecution() < minBattery;
}

void SVehicle::printVehicle(ostream& os) {
    os << id << "\t";
    os << initBattery << "\t";
    os << busyTime << "\t";
    os << startTask.getId() << "\t";
    os << endTask.getId() << "\t";
    os << makespan << "\t";
    os << tasks.size() << endl;
}

void SVehicle::setMaxBattery(int capacity) {
    maxBattery = capacity;
}

void SVehicle::setMinBattery(int capacity) {
    minBattery = capacity;
}

STask& SVehicle::getTask(int index) {
    if (index == -1) {
        return startTask;
    } else if (index == -2 || index >= tasks.size()) {
        return endTask;
    } else {
        return tasks.at(index);
    }
}

void SVehicle::printTasks(ostream& os) {
        startTask.printTaskHeader(os);
        STask task = startTask;
        STask end = endTask;
        task.printTask(os);
        for (int i = 0; i < tasks.size(); i++) {
            tasks[i].printTask(os);
        }
        end.printTask(os);
}

void SVehicle::setGoToDepotAtEnd(int goToDepotAtEnd) {
    this->goToDepotAtEnd = goToDepotAtEnd;
}