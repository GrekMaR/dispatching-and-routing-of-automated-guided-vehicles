/* 
 * File:   main.h
 * Author: thor
 *
 * Created on February 28, 2016, 11:22 AM
 */

#ifndef MAIN_H
#define	MAIN_H



#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <ostream>

#include "instance.h"
#include "solution.h"
#include "heuristic.h"

void parseInputToInstance(string);

#endif	/* MAIN_H */

