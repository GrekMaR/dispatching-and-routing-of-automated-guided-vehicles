/* 
 * File:   iTask.cpp
 * Author: thor
 * 
 * Created on February 28, 2016, 11:45 AM
 */

#include "iTask.h"


int ITask::idCounter = 0;

ITask::ITask() {
    this->id = idCounter++;
    this->batteryCost = 0;
    this->deadline = 0;
    this->duration = 0;
    this->minStartTime = 0;
    this->setupFromDepot = 0;
    this->setupToDepot = 0;
    this->taskType = 'n';
}

// min start, duration, deadline, battery cost, task type
ITask::ITask(int start, int duration, int deadline, int batterycost, 
        int numTasks, char type) 
{
    this->id = idCounter++;
    this->minStartTime = start;
    this->duration = duration;
    this->deadline = deadline;
    this->batteryCost = batterycost;
    this->taskType = type;
    this->setupTimes = vector<int>(numTasks);
    for (int i = 0; i < numTasks; i++) {
        setupTimes.push_back(0);
    }
}

// copy constructor
ITask::ITask(const ITask& orig) {
    this->id = orig.id;
    this->batteryCost = orig.batteryCost;
    this->deadline = orig.deadline;
    this->duration = orig.duration;
    this->minStartTime = orig.minStartTime;
    this->setupFromDepot = orig.setupFromDepot;
    this->setupTimes = orig.setupTimes;
    this->setupToDepot = orig.setupToDepot;
    this->taskType = orig.taskType;
}

ITask::~ITask() {
}

void ITask::setMinStartTime(int time)
{
    this->minStartTime = time;
}

void ITask::setDuration(int time)
{
    this->duration = time;
}

void ITask::setDeadline(int time)
{
    this->deadline = time;
}

void ITask::setBatteryCost(int cost) 
{
    this->batteryCost = cost;
}

void ITask::setCharType(char type)
{
    if (type == 'b') {
        this->taskType = 'b'; // battery task
    } else {
        this->taskType = 'n'; // else a normal task
    }
}

void ITask::setSetupTime(int index, int value) 
{
    setupTimes.at(index) = value;
}

void ITask::resizeSetupTime(int size)
{
    vector<int> setup(size);
    setupTimes = setup;
            
}

void ITask::setSetupFromDepot(int setupTime) 
{
    setupFromDepot = setupTime;
}

void ITask::setSetupToDepot(int setupTime) 
{
    setupToDepot = setupTime;
}

void ITask::printTask(ostream& os) {
    os << id << "\t";
    os << minStartTime << "\t";
    os << duration << "\t";
    os << deadline << "\t";
    os << taskType << "\t";
    os << batteryCost << "\t";
    os << setupFromDepot << "\t";
    os << setupToDepot << "\t";
    os << setupTimes.size() << endl;
}