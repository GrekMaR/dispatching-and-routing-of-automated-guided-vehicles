/* 
 * File:   instance.cpp
 * Author: thor
 * 
 * Created on February 28, 2016, 11:49 AM
 */
#include <iostream>


#include "instance.h"

using namespace std;

Instance::Instance() 
{
    numTasks = 0;
    numVehicles = 0;
    this->lowerbound = 0;
    this->upperbound = 0;
}


Instance::Instance(const Instance& orig) 
{
    this->lowerbound = orig.lowerbound;
    this->maxRechargeAmount = orig.maxRechargeAmount;
    this->numTasks = orig.numTasks;
    this->numVehicles = orig.numVehicles;
    this->numBatteryResources = orig.numBatteryResources;
    this->rechargePerTimeUnit = orig.rechargePerTimeUnit;
    this->rechargeTimeToMax = orig.rechargeTimeToMax;
    this->setupTimes = orig.setupTimes;
    this->tasks = orig.tasks;
    this->upperbound = orig.upperbound;
    this->vehicles = orig.vehicles;
    this->goToDepotAtEnd = orig.goToDepotAtEnd;
}

Instance::~Instance() 
{
}

/**
 * Sets the number of tasks in the instance and alters the tasks vector
 * to reflect this
 * @param numTasks
 */
void Instance::setNumTasks(int numTasks)
{
    // if the number of tasks is higher
    // add tasks
    if (this->numTasks < numTasks) {
        for (int i = this->numTasks; i < numTasks; i++) {
            addTask(*new ITask());
        }
    } else { // equal or lower, remove until it fits
        for (int i = numTasks; i > this->numTasks; i--) {
            this->tasks.pop_back();
        }
    }
    this->numTasks = numTasks;
}

/**
 * Sets the number of vehicles in the instance, and alters the vehicles vector
 * to reflect this
 * @param numVehicles
 */
void Instance::setNumVehicles(int numVehicles) 
{
    // if new vehicle number if higher
    // add vehicles
    if (this->numVehicles < numVehicles) {
        for (int i = this->numVehicles; i < numVehicles; i++) {
            addVehicle(*new IVehicle());
        }
    } else {
        for (int i = numVehicles; i > this->numVehicles; i--);
    }
    this->numVehicles = numVehicles;
}

/**
 * Sets a lowerbound for the instance, thus all solutions must be above this
 */
void Instance::setLowerbound(int lowerbound)
{
    this->lowerbound = lowerbound;
}

/**
 * Sets an upper bound for the instance, thus all solution must be below this
 * @param upperbound
 */
void Instance::setUpperbound(int upperbound)
{
    this->upperbound = upperbound;
}

/**
 * Adds a task to the end of the task vector
 * @param task
 */
void Instance::addTask(ITask task)
{
    this->tasks.push_back(task);
}

/**
 * Adds a vehicle to the end of the vehicle vector
 * @param vehicle
 */
void Instance::addVehicle(IVehicle vehicle)
{
    this->vehicles.push_back(vehicle);
}

/**
 * Sets a task in the tasks vector
 * @param index - the index at which to insert the task
 * @param task - the task to insert
 */
void Instance::setTask(int index, ITask task)
{
    this->tasks.at(index) = task;
}

/**
 * Sets a vehicle in the vehicles vector
 * @param index - the index at which to insert the vehicle
 * @param vehicle - the vehicle to insert
 */
void Instance::setVehicle(int index, IVehicle vehicle)
{
    this->vehicles.at(index) = vehicle;
}

/**
 * Resizes the setup time matrix to be of size i x j
 * @param i - number of vectors in setup times (from setup time)
 * @param j - number of values in the vectors (to setup time)
 */
void Instance::resizeSetuptime(int i, int j) {
    Matrix matrix(i,Row(j));
    setupTimes = matrix;
}

void Instance::setSetuptime(int i, int j, int value) {
    if (setupTimes.size() <= i || setupTimes[i].size() <= j) {
        return;
    }
    setupTimes[i][j] = value;
    if (i-1 < tasks.size() && i > 0 && j > 0 && j-1 < tasks.size()) {
        tasks[i-1].setSetupTime(j-1, value);
    } else if (i-1 < tasks.size() && i > 0 && j-1 == tasks.size()) {
        tasks[i-1].setSetupToDepot(value);
    } else if (i == 0 && j > 0) {
        tasks[j-1].setSetupFromDepot(value);
    }
    
}

void Instance::setMaxRechargeAmount(int amount) {
    maxRechargeAmount = amount;
}

void Instance::setRechargePerTimeUnit(int amount) {
    rechargePerTimeUnit = amount;
}

void Instance::setRechargeTimeToMax(int time) {
    rechargeTimeToMax = time;
}

void Instance::printSetupTimes(ostream& os) {
    os << "Setup times matrix: " << endl;
    for (int i = 0; i < setupTimes.size(); i++) {
        os << i << ": ";
        for (int j = 0; j < setupTimes[i].size(); j++) {
            os << setupTimes[i][j] << ", ";
        }
        os << endl;
    }
}


void Instance::printInstance(ostream& os){
    os << "----------------------------" << endl;
    os << "----------Instance----------" << endl;
    os << "----------------------------" << endl;
    os << "Num Tasks: " << numTasks << endl;
    os << "Num Vehicles: " << numVehicles << endl;
    os << "Num Battery Resources: " << numBatteryResources << endl;
    os << "Battery min / max capacity " 
            << vehicles[0].getMinBattery() << " /" 
            << vehicles[0].getMaxBattery() << endl;
    printSetupTimes(os);
    os << "solution lower / upper bound: " 
            << lowerbound << " / "  << upperbound << endl;
    os << "recharge time to max capacity: " << rechargeTimeToMax << endl;
    os << "recharge per time unit: " << rechargePerTimeUnit << endl;
    os << "span between min and max: " << maxRechargeAmount << endl;
    os << "Tasks:" << endl;
    os << "taskNum\tstart\tprocess\tdeadline\tbatteryCost\tfromDepot\ttoDepot" << endl;
    for (int i = 0; i < numTasks; i++) {
        os << tasks[i].getId() << "\t";
        os << tasks[i].getMinStartTime() << "\t";
        os << tasks[i].getDuration() << "\t";
        os << tasks[i].getDeadline() << "\t\t";
        os << tasks[i].getBatteryCost() << "\t\t";
        os << tasks[i].getSetupFromDepot() << "\t\t";
        os << tasks[i].getSetupToDepot() << endl;
    }
    os << "SetupTime" << endl;
    for (int i = 0; i < numTasks; i++) {
        os << "taskNum: " << i << ": ";
        for (int j = 0; j < numTasks; j++) {
            os << tasks[i].getSetupTime(j) << ", ";
        }
        os << endl;
    }
    os << "vehicles:" << endl;
    os << "VehicleNum\tInitBat\tMinStart\tMinBat\tMaxBat" << endl;
    for (int i = 0; i < numVehicles; i++) {
        os << vehicles[i].getId() << "\t";
        os << vehicles[i].getInitialBattery() << "\t";
        os << vehicles[i].getMinStart() << "\t";
        os << vehicles[i].getMinBattery() << "\t";
        os << vehicles[i].getMaxBattery() << endl;
    } 
}

void Instance::setNumBatteryResources(int amount) {
    numBatteryResources = amount;
}

void Instance::setGoToDepotAtEnd(int goToDepotAtEnd) {
    this->goToDepotAtEnd = goToDepotAtEnd;
}