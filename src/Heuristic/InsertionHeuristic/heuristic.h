/* 
 * File:   heuristic.h
 * Author: thor
 *
 * Created on February 28, 2016, 11:50 AM
 */

#ifndef HEURISTIC_H
#define	HEURISTIC_H

//#include <iostream>
#include <cstdlib>
#include <limits>

#include "instance.h"
#include "solution.h"
#include "iTask.h"
#include "iVehicle.h"
#include "sTask.h"
#include "sVehicle.h"

enum RechargeType {VARIABLE_BATTERY_RECHARGE, FIXED_BATTERY_RECHARGE};

using namespace std;

class Heuristic {
public:
    // getters
    int getInitHeuristic();
    int getRouteHeuristic() { return routeHeuristic; }
    int getInsertionHeuristic() { return insertionHeuristic; }
    int getTaskHeuristic() { return taskHeuristic; }
    Solution& getSolution() { return sol; }
    
    // setters
    void setInitHeuristic(int heuristic);
    void setRouteHeuristic(int heuristic);
    void setInsertHeuristic(int heuristic);
    void setTaskHeuristic(int heuristic);
    void setElapsedTime(double time);
    bool isInputFeasible();
    void setInsertionParams(double, double);
    void setMu(double);
    void setAlphas(double, double, double);
    void setLambda(double);
    void setBetas(double, double);
    void setRechargeType(int);
    void setIfGoToDepotAtEnd(int);
    // methods
    Solution runHeuristic();
    
    // constructors and destructors
    Heuristic();
    Heuristic(Instance ins);
    Heuristic(const Heuristic& orig);
    virtual ~Heuristic();
private:
    struct rechargeDuration {
        int startTime;
        int endTime;
    };
    Instance ins;
    Solution sol;
    int initHeuristic;
    int routeHeuristic;
    int insertionHeuristic;
    int taskHeuristic;
    vector<ITask> unroutedTasks;
    vector<ITask> routedTasks;
    vector<rechargeDuration> recharges;
    double init_a1;
    double init_a2;
    double mu;
    double a1;
    double a2;
    double a3;
    double lambda;
    double b1;
    double b2;
    RechargeType rt;
    //int rechargeType;
    
    void initializeRoutes();
    vector<int> getInitializationOrder();
    int chooseRoute(vector<SVehicle>);
    vector<int> chooseInsertionPoint(SVehicle);
    int chooseTask(SVehicle, vector<int>);
    int batteryRechargeInsertion();
    void RechargeIfWithoutCost();
    double weightedSetupAndBatteryConsumption(ITask&);
    double criteriaAux1(STask& j, STask& u, STask& k);
    double criteriaAux2(STask& j, STask& u, SVehicle& i);
    double criteriaAux3(STask& u);
    bool FeasibleInsertion(STask& j, STask& u);
    STask getNewBatteryTask(STask& j, int batteryLevelAtEndTask, int minStartWithSetup, int ToMax);
    
};

#endif	/* HEURISTIC_H */

