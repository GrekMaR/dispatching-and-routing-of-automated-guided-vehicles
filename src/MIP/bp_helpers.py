'''
helpers
all classes and functions which could be
potentially shared between modules.

-schedule

'''

class Problem():
        def __init__(self,mp, params,x,cover,restrict):
            self.master = mp
            self.params = params
            self.x = x #from mp
            self.cover = cover
            self.restrict = restrict

class Params():
    def __init__(self,instance,columns,a,b,c,r,trees,runtime,startsol):
        self.a = a #coverage
        self.b = instance.stationlimit #max conflicts
        self.columns = columns
        self.c = c #cost of column
        self.P = [len(columns[i]) for i in range(instance.m)]
        self.T = instance.maxtime
        self.instance = instance
        self.r = r #needed battery charges
        self.trees = trees
        self.runtime = runtime
        self.epsilon = []
        self.beta = []
        self.gamma = []
        self.B = b #B phi containing tau containing i,j
        self.startsolution = startsol
        #fills out as we find new cuts
    def getDuals(self,duals):
        n = self.instance.n
        m = self.instance.m
        gamma = duals[0:n+m]
        # skip betas for vehicles
        beta = duals[n+m: n+m]
        #Check if we can get epsilons
        if len(duals)> n+m:
            epsilon = duals[n+m:]
        self.epsilon = epsilon
        self.beta = beta
        self.gamma = gamma
        return


class bp_conflicts():
        def __init__(self,interval,schedules,station):
                self.interval = interval
                self.schedules = schedules
                self.station = station
        def __str__(self):
                return "Conflict(interval:{},schedules:{})".format(self.interval,self.schedules)
        

#Schedule specific
class bp_schedule():#currently not used
    def __init__(self,order,starttimes, completions,machine):
        self.order = order#list
        self.starttimes = starttimes
        self.completions = completions
        self.machine = machine
        #self.battery = battery forgot its use >_<
    def __repr__(self):
        s = "v: {}[".format(self.machine)
        for i in self.order:
            s = s+ " "+str(i)+" "
        s = "]"
        return s

class bp_task():
    def __init__(self,index,ptime,setup):
        self.index = index
        self.ptime = ptime
        self.setup = setup
    def __repr__(self):
        return "i:{}(p:{},s:{})".format(self.index,self.ptime,self.setup)
    def __str__(self):
        return "i:{}(p:{},s:{})".format(self.index,self.ptime,self.setup)

#convert from list/dictionary mode to actual class
def class_schedule(schedule, vehicleindex):
    tasks = []
    starttimes = []
    completions = []
    currenttime = 0
    for x in schedule:
        starttimes.append(x['start'])
        t = bp_task(x['index'],x['ptime'],x['setup'])
        tasks.append(t)
        currenttime += t.setup + t.ptime
        completions.append(currenttime)
    s = bp_schedule(tasks,starttimes,completions,vehicleindex)
    return s


def extractSolution(problem):
    schedules = []
    x = problem.x
    for i,v in enumerate(x):
        schedules.append([])
        for j,w in enumerate(v):
            if w.getAttr("x")==1:
                schedules[i] =problem.params.columns[i][j]
                break
    
    return schedules


        

def extractTestInfo(problem):
    #vehicles tasks nrcolumns objective iterations time
        m= problem.params.instance.m
        n = problem.params.instance.n
        stations = problem.params.instance.stations
        overlaps = problem.params.instance.stationlimit
        total = 0
        for i in problem.params.columns:
                total+= len(i)
        genruntime = problem.params.runtime
        if problem.master.getAttr("SolCount")  >0:
            o=round(problem.master.getAttr("ObjVal"),4)
            it=int(problem.master.getAttr("IterCount"))
            runtime =round(problem.master.getAttr("Runtime"),4)
            
            line = bp_results(m,n,total,0,o,it,runtime,genruntime,stations,overlaps)
        else:
                line = bp_results(m,n,total,0,"-","-","-",genruntime,stations,overlaps)   
        return line

class bp_results():
    def __init__(self,m,n,nrcols,tasklimit,objval,iters,runtime,genruntime,stations,overlaps):
        self.m = m
        self.n = n
        self.nrcols = nrcols
        self.tasklimit = tasklimit
        self.objval = objval
        self.iters = iters
        self.runtime = runtime
        self.genruntime = genruntime
        self.stations = stations
        self.overlaps = overlaps
    def getLine(self):
        s = "{} & {} & {} & {} & {} & {} & {} & {} & {}".format(self.m,self.n,self.nrcols,
                                          self.tasklimit,self.stations,self.overlaps,self.genruntime,self.runtime,self.objval)
        return s

#-----
#For finding cliques in graphs (seems unnecessary)
def all_subsets(ss):
  return chain(*map(lambda x: combinations(ss, x), range(0, len(ss)+1)))

class inode():
    def __init__(self,interval,neighbours,schedule, vehicle):
        self.interval = interval
        self.schedule = schedule
        self.vehicle = vehicle
        self.neighbours = neighbours
class igraph():
    def __init__(self,vertices):
        self.vertices = vertices

#http://stackoverflow.com/questions/13904636/implementing-bron-kerbosch-algorithm-in-python
def bron_kerbosch(R,P,X,G):
    if not any((P,X)):
        yield R
    for v in P[:]:
        R_v = R +[v]
        P_v = [x for x in P if x in v.neighbours]
        X_v = [x for x in X if x in v.neighbours]
        for r in bron_kerbosch(R_v,P_v,X_v,G):
            yield r
        P.remove(v)
        X.append(v)

def N(v,G):
    return [i for i, n_v in enumerate(G[v]) if n_v]


