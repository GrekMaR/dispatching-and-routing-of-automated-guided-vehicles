\begin{table}[]
\centering
\caption{Table showing the results from running the MIP model on instances of varying size.}
\label{mipperf}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|l|l|l|l|l|}
\hline
\multirow{2}{*}{Vehicles} & \multirow{2}{*}{Tasks} & \multicolumn{2}{c}{\# Vars} & \multicolumn{2}{c}{\# Constr.} & \multirow{2}{*}{First UB} & \multirow{2}{*}{Time First UB} & \multirow{2}{*}{UB} & \multirow{2}{*}{LB} & \multirow{2}{*}{Gap} & \multirow{2}{*}{Nodes} & \multirow{2}{*}{Time}\\  &  & presol & after & presol & after &  &  &  &  &  &  & \\ \hline 
5 & 10 & 1638 & 1296 & 5827 & 4340 & 320.0 & 0.0032 & 95.4999999903 & 95.5 & 0.0 & 13678.0 & 9.3997\\ \hline 
5 & 15 & 3168 & 2721 & 12452 & 10290 & 406.0 & 0.0055 & 202.99999998 & 85.8 & 57.7107 & 463496.0 & 3600.0241\\ \hline 
5 & 20 & 5198 & 4626 & 21577 & 18225 & 384.5 & 0.0097 & 179.499999874 & 69.3 & 61.3919 & 21804.0 & 3600.0591\\ \hline 
5 & 25 & 7728 & 7031 & 33202 & 28405 & 380.0 & 0.0134 & 227.5 & 34.5 & 84.8365 & 21360.0 & 3600.2432\\ \hline 
10 & 25 & 15428 & 14036 & 66277 & 56705 & 233.0 & 0.0215 & 93.0 & 31.0 & 66.6666 & 21596.0 & 3600.359\\ \hline 
10 & 15 & 6318 & 5426 & 24827 & 20535 & 265.0 & 0.0088 & 121.999999988 & 122.0 & 0.0 & 22434.0 & 105.109\\ \hline 
10 & 20 & 10373 & 9231 & 43052 & 36380 & 155.0 & 0.5695 & 58.999999994 & 59.0 & 0.0 & 33703.0 & 2582.1851\\ \hline 
\end{tabular}
 \end{table}