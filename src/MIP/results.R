#!/usr/bin/Rscript
## Get them args :)
#args <- commandArgs(trailingOnly = TRUE)

resultPlot <-function(pyout,gout){
  #Header Current
  # nrtasks makespan solvetime timestep
  
  #Header OLD
  #index entry start completion solvetime assignedTo makespan nrvehicles 
  #iterations 
  args = c(pyout,gout)
  
  #read in results for mip 
  resultsp <- read.csv(file=args[1],header=TRUE, sep=" ")
  #read in results for greedy
  resultsg <- read.csv(file=args[2],header=TRUE, sep=" ")
  
  # Plot the results!
  library(ggplot2)
  svg(filename="pyplotg.svg", 
      width=5, 
      height=4, 
      pointsize=12)
  p <-ggplot(data = resultsp, aes(x=timestep, y=nrtasks) )+ 
    geom_bar(stat="identity", width=1)+
    xlab("Timestep") + ylab("#Tasks")+
    scale_x_continuous(breaks = seq(0, 1500, 100))+
    scale_y_continuous(breaks = seq(0,50,2))+
    ggtitle("Waiting Time for Tasks(MIP) with Garbage Tasks.")
  print(p)
  dev.off()
  #No starting
  svg(filename="pyplotnog.svg", 
      width=5, 
      height=4, 
      pointsize=12)
  p <-ggplot(data = resultsp[-1,], aes(x=timestep, y=nrtasks) )+ 
    geom_bar(stat="identity", width=1)+
    xlab("Timestep") + ylab("#Tasks")+
    scale_x_continuous(breaks = seq(0, 1500, 50))+
    scale_y_continuous(breaks = seq(0,50,2))+
    ggtitle("Waiting Time for Tasks(MIP)")
  print(p)
  dev.off()
  #mean tasks pr iteration ALL
  iterationsp <- resultsp[["nrtasks"]]
  resultsp.nrtaskmeang <- mean(iterationsp)
  print(resultsp.nrtaskmeang)
  #mean without first
  iterationsp <- iterationsp[2:length(iterationsp)]
  resultsp.nrtaskmean <- mean(iterationsp)
  print(resultsp.nrtaskmean)
  
  #mean tasks pr vehicle in iteration
  #taskvehicle <- resultsp.frame(table(resultsp$assignedTo,resultsp$timestep))
  
  #resultsp.nrtaskvehiclemean <- mean(count(taskvehicle$nrtasks))
  
  #Get entry for a task
  #Subset to each task, then get first row
  resultsp.entryrows <- resultsp[!duplicated(resultsp$index)]
  #get completion rows 
  resultsp.completiontasks <-resultsp[ !duplicated(resultsp$index,fromLast=TRUE),]
  
  #GREEDY
  svg(filename="greedyplotg.svg", 
      width=5, 
      height=4, 
      pointsize=12)
  p <-ggplot(data = resultsg, aes(x=timestep, y=nrtasks) )+ 
    geom_bar(stat="identity", width=1)+
    xlab("Timestep") + ylab("#Tasks")+
    scale_x_continuous(breaks = seq(0, 1500, 100))+
    scale_y_continuous(breaks = seq(0,50,2))+
    ggtitle("Waiting Time for Tasks(GREEDY) with Garbage Tasks")
  print(p)
  dev.off()
  #No starting
  svg(filename="greedyplotnog.svg", 
      width=5, 
      height=4, 
      pointsize=12)
  p <-ggplot(data = resultsg[-1,], aes(x=timestep, y=nrtasks) )+ 
    geom_bar(stat="identity", width=1)+
    xlab("Timestep") + ylab("#Tasks")+
    scale_x_continuous(breaks = seq(0, 1500, 50))+
    scale_y_continuous(breaks = seq(0,50,2))+
    ggtitle("Waiting Time for Tasks(GREEDY)")
  print(p)
  dev.off()
  
  #mean tasks pr iteration ALL
  iterationsg <- resultsg[["nrtasks"]]
  resultsg.nrtaskmeang <- mean(iterationsg)
  print(resultsg.nrtaskmeang)
  #mean without first
  iterationsg <- iterationsg[2:length(iterationsg)]
  resultsg.nrtaskmean <- mean(iterationsg)
  print(resultsg.nrtaskmean)
  
}