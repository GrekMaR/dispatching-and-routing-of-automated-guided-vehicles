from gurobipy import *
import math
import sys



def initMPR_LR(params):
    a = params.a #coverage of task j on vehicle i on sched p
    ac = params.ac #battery starts
    b = params.b #battery resource limit
    c = params.c #Cost of schedule
    I = params.instance.m #nr of vehicles
    P = params.P #nr of schedules per vehicle
    J = params.instance.n #nr tasks
    T = params.T #time indices
    

    model = Model("MPR_LR")

    x = []
    for i in range(I):
        at = []
        for p in range(P[i]):
            varname = "x({}{})".format(i,p)
            at.append(model.addVar(vtype=GRB.CONTINUOUS, lb=0, ub=1,name=varname))
        x.append(at)

    model.update()

    #set objective
    
    terms = []
    for i in range(I):
        for p in range(P[i]):
            terms.append(c[i][p]*x[i][p])
    model.setObjective(quicksum([c[i][p]*x[i][p] for i in range(I) for p in range(P[i])]),GRB.MINIMIZE)
    model.update()

    
    #Cover tasks
    cover = []
    for j in range(J):
        terms = []
        for i in range(I):
            for p in range(P[i]):
                terms.append(a[i][p][j]*x[i][p])
        cover.append(model.addConstr(quicksum(terms), GRB.EQUAL, 1, "Coverage of tasks for task {}".format(j)))

    #Restrict number of schedules
    restrict = []
    for i in range(I):
        terms = []
        for p in range(P[i]):
            terms.append(x[i][p])
        restrict.append(model.addConstr(quicksum(terms), GRB.LESS_EQUAL, 1,
                                        "Number of schedules per vehicle {}".format(i)))


    #model.cbCut(model._cars[0]+ model.vars[1]<=1)
    #http://www.gurobi.com/documentation/6.5/refman/py_model_cbcut.html

    return model,x,cover,restrict


def initMP(params,delay,makespan,timelimit):
    a = params.a #coverage of task j on vehicle i on sched p
    b = params.b #battery resource limit
    c = params.c #cost of schedule
    I = params.instance.m #nr of vehicles
    P = params.P #nr of schedules per vehicle
    N = params.instance.n #nr tasks
    T = params.T #time indices
    J = range(1,N+1)
    B = params.B
    #print c
    model = Model("MP")
    model.setParam('OutputFlag', False )
    x = []
    for i in range(I):
        at = []
        for p in range(P[i]):
            varname = "x({}{})".format(i,p)
            at.append(model.addVar(vtype=GRB.BINARY,name=varname))
        x.append(at)
    #Add start solution
    
        
    model.update()
    for i in params.startsolution:
        x[i[0]][i[1]].start = 1.0
    #set objective

    '''
    terms = []
    for i in range(I):
        for p in range(P[i]):
            terms.append(c[i][p]*x[i][p])
    '''
    
    if not makespan:
        obj = [c[i][p]*x[i][p] for i in range(I) for p in range(P[i])]
        model.setObjective(quicksum(obj),GRB.MINIMIZE)
    else:
        cmax = model.addVar(vtype=GRB.CONTINUOUS,name="Cmax")
        model.update()
        model.setObjective(cmax,GRB.MINIMIZE)
        #makespan
        for i in range(I):
            terms = []
            for p in range(P[i]):
                terms.append(c[i][p]*x[i][p])
            model.addConstr(quicksum(terms), GRB.LESS_EQUAL, cmax, "makespan{}".format(i))
    model.params.timeLimit = timelimit      
    model.update()

    #Cover
    cover = []
    for j in J:
        terms = []
        for i in range(I):
            for p in range(P[i]):
                terms.append(a[i][p][j]*x[i][p])
        cover.append(model.addConstr(quicksum(terms), GRB.EQUAL, 1, "Coverage of tasks for task {}".format(j)))

    #Restrict number of schedules
    restrict = []
    for i in range(I):
        terms = []
        for p in range(P[i]):
            terms.append(x[i][p])
        restrict.append(model.addConstr(quicksum(terms), GRB.LESS_EQUAL, 1,
                                        "Number of schedules per vehicle {}".format(i)))

    '''
    for t in range(T):
        for j in range(J):
            terms = []
            for i in range(I):
                for p in range(P[i]):
                    terms.append(ac[i][p][j][t]*x[i][p])
            model.addConstr(quicksum(terms), GRB.LESS_EQUAL, b, "Battery conflicts {}".format(i))
    '''

    conflict = []
    if not delay:
        for bphi in B:
            for tau in bphi:
                tt= []
                for sched in tau.schedules:#(i,j)
                    i,j = sched[2]
                    try:
                        tt.append(x[i][j])
                    except:
                        print len(x)
                        print len(x[i])
                        print i
                        print j
                        sys.exit()
                conflict.append(model.addConstr(quicksum(tt),GRB.LESS_EQUAL,b,"Conflict for period {} ".format(tau.interval)))
    model.update()

    return model,x,cover,restrict,conflict



def initSPP_SingleOLD(params):
    c = params.c #task cost i in N
    #cr = params.cr
    f = params.instance.f #max 
    q = params.instance.q #min
    g = params.instance.g #start i in N
    d = params.instance.d #deadline i in N
    l = params.instance.l #starting battery level k in M
    o = params.instance.o #consumption i in N
    N = params.instance.n #nr tasks
    R = params.R #nr recharge tasks
    #I = params.I #nr vehicles
    N_ = range(1,N+1)
    N_s = range(0,N+2)
    N_r = range(N+2,N+2+R)
    N_sr = N_s + N_r
    epsilon = params.epsilon #time pr charge
    s = params.s #setup time
    mst = params.mst
    
    
    model = Model("SPP")
        


    x = []
    for i in N_sr:
        at = []
        for j in N_sr:
            varname = "x({}{})".format(i,j)
            at.append(model.addVar(vtype=GRB.BINARY,name=varname))
        x.append(at)

    bt = []
    for i in N_sr:
        varname = "bt({})".format(i)
        bt.append(model.addVar(vtype=GRB.CONTINUOUS,name=varname))

    br = []
    for i in N_sr:
        varname = "br({})".format(i)
        br.append(model.addVar(vtype=GRB.CONTINUOUS,name=varname))        

    cr = []
    for i in N_r:
        varname = "cr({})".format(i)
        cr.append(model.addVar(vtype=GRB.CONTINUOUS,name=varname))
        
    #objective
    terma = quicksum([c[i][j]*x[i][j] for i in N_sr for j in N_s])
    termb = quicksum([i for i in cr])
    model.setObjective((terma + termb),GRB.MINIMIZE)
    model.update()

    #constraints
    #Coverage constraint
    for i in N_:
        terma = quicksum([x[i][j] for j in N_sr])
        model.addConstr(terma, GRB.EQUAL, 1, "Coverage t{}".format(i))
    #Flow constraints
    terma = quicksum([x[0][j] for j in N_sr if j != 0])
    model.addConstr(terma, GRB.EQUAL, 1, "Flow Source")
    terma = quicksum([x[i][N_s[-1]] for i in N_sr if i != N_s[-1]])
    model.addConstr(terma, GRB.EQUAL, 1, "Flow Sink")

    for h in N_sr:
        terma = quicksum([x[i][h] for i in N_sr if i != h])
        termb = quicksum([x[h][j] for j in N_sr if j != h])
        model.addConstr(terma-termb, GRB.EQUAL, 0, "Flow tasks t{}".format(h))
    #Resources and Time
    for i in N_sr:
        for j in N_s:
            model.addConstr(bt[i]+ c[i][j] - P(1-x[i][j]),
                               GRB.LESS_EQUAL, bt[j], "Time var t{}".format(i))
    for j in N_s:
        model.addConstr(mst+ c[0][j] - P(1-x[0][j]),
                            GRB.LESS_EQUAL, bt[j], "Time var source t{}".format(j))
    for j in N_r:
        model.addConstr(mst+ s[0][N_s[-1]] + cr[j] - P(1-x[0][j]),
                            GRB.LESS_EQUAL, bt[j], "Time var source recharge t{}".format(j))

    model.addConstr(bt[0],GRB.EQUAL, 0, "Time var source set")
    
    for i in N_ + N_r:
        for j in N_:
               model.addConstr(br[i]+ o[j] - P(1-x[i][j]),
                               GRB.GREATER_EQUAL, br[j], "Resource var t{}".format(i))
    for i in N_r:
        model.addConstr(br[i],
                            GRB.EQUAL, f, "Resource var recharge t{}".format(i))
    for i in N_s:
        for j in N_r:
            model.addConstr(bt[i]+s[i][j]+(f-br[i])*epsilon - P(1-x[i][j]),
                               GRB.LESS_EQUAL, bt[j], "Time var recharge t{}".format(i))
    for i in N_s:
        for j in N_r:
            model.addConstr(s[i][j]+(f-br[i])*epsilon - P(1-x[i][j]),
                               GRB.LESS_EQUAL, cr[j], "Time c var recharge t{}".format(j))
    for j in N_r:
        model.addConstr(s[0][N_s[-1]](f-l)*epsilon - P(1-x[0][j]),
                               GRB.LESS_EQUAL, cr[j], "Time c var recharge source t{}".format(j))
    #resource constraints and time windows
    for i in N_:
        model.addConstr(bt[i]-p[i], GRB.GREATER_EQUAL, g[i], "Time window arrival t{}".format(i))
    for i in N_:
        model.addConstr(bt[i], GRB.LESS_EQUAL, d[i], "Time window deadline t{}".format(i))

    model.addConstr(br[0], GRB.EQUAL, l, "Starting battery level")
    for i in N_ + N_r:
        model.addConstr(br[i], GRB.GREATER_EQUAL, q, "Resource min t{}".format(i))
    for i in N_ + N_r:
            model.addConstr(br[i], GRB.LESS_EQUAL, f, "Resource max t{}".format(i))
    return model   


def initSPPOLD(params):
    c = params.c #task cost i in N
    #cr = params.cr
    f = params.instance.f #max 
    q = params.intance.q #min
    g = params.instance.g #start i in N
    d = params.instance.d #deadline i in N
    l = params.instance.l #starting battery level k in M
    o = params.instance.o #consumption i in N
    N = params.instance.n #nr tasks
    R = params.R#nr recharge tasks
    I = params.instance.m #nr vehicles
    N_ = range(1,N+1)
    N_s = range(0,N+2)
    N_r = range(N+2,N+2+R)
    N_sr = N_s + N_r
    epsilon = params.instance.epsilon #time pr charge
    s = params.s #setup time
    mst = params.mstarttimes #machine start times
    
    model = Model("SPP")
    x = []
    for i in N_sr:
        at = []
        for j in N_sr:
            varname = "x({}{})".format(i,j)
            at.append(model.addVar(vtype=GRB.BINARY,name=varname))
        x.append(at)

    bt = []
    for i in N_sr:
        varname = "bt({})".format(i)
        bt.append(model.addVar(vtype=GRB.CONTINUOUS,name=varname))
        
    br = []
    for i in N_sr:
        at = []
        for k in range(I):
            varname = "br({}{})".format(i,k)
            at.append(model.addVar(vtype=GRB.CONTINUOUS,name=varname))
        br.append(at)

    cr = []
    for i in N_r:
        varname = "cr({})".format(i)
        cr.append(model.addVar(vtype=GRB.CONTINUOUS,name=varname))
        
    #objective
    terma = quicksum([c[i][j]*x[i][j][k] for i in N_sr for j in N_s for k in range(I)])
    termb = quicksum([i for i in cr])
    model.setObjective((terma + termb),GRB.MINIMIZE)
    model.update()

    #constraints
    #Coverage constraint
    for i in N_:
        terma = quicksum([x[i][j][k] for j in N_sr for k in range(I)])
        model.addConstr(terma, GRB.EQUAL, 1, "Coverage t{}".format(i))
    #Flow constraints
    for k in range(I):
        terma = quicksum([x[0][j][k] for j in N_sr if j != 0])
        model.addConstr(terma, GRB.EQUAL, 1, "Flow Source v{}".format(k))
    for k in range(I):
        terma = quicksum([x[i][N_s[-1]][k] for i in N_sr if i != N_s[-1]])
        model.addConstr(terma, GRB.EQUAL, 1, "Flow Sink v{}".format(k))
    for k in range(I):
        for h in N_sr:
            terma = quicksum([x[i][h][k] for i in N_sr if i != h])
            termb = quicksum([x[h][j][k] for j in N_sr if j != h])
            model.addConstr(terma-termb, GRB.EQUAL, 0, "Flow tasks v{} t{}".format(k,h))
    #Resources and Time
    for i in N_sr:
        for j in N_s:
            for k in range(I):
               model.addConstr(bt[i]+ c[i][j] - P(1-x[i][j][k]),
                               GRB.LESS_EQUAL, bt[j], "Time var t{}".format(i))
    for j in N_s:
        for k in range(I):
            model.addConstr(mst[k]+ c[0][j] - P(1-x[0][j][k]),
                            GRB.LESS_EQUAL, bt[j], "Time var source t{}".format(j))
    for j in N_r:
        for k in range(I):
            model.addConstr(mst[k]+ s[0][N_s[-1]] + cr[j] - P(1-x[0][j][k]),
                            GRB.LESS_EQUAL, bt[j], "Time var source recharge t{}".format(j))

    model.addConstr(bt[0],GRB.EQUAL, 0, "Time var source set")
    
    for i in N_ + N_r:
        for j in N_:
            for k in range(I):
               model.addConstr(br[i][k]+ o[j] - P(1-x[i][j][k]),
                               GRB.GREATER_EQUAL, br[j][k], "Resource var v{} t{}".format(k,i))
    for i in N_r:
        for k in range(I):
            model.addConstr(br[i][k],
                            GRB.EQUAL, f, "Resource var recharge v{} t{}".format(k,i))
    for i in N_s:
        for j in N_r:
            for k in range(I):
               model.addConstr(bt[i]+s[i][k]+(f-br[i][k])*epsilon - P(1-x[i][j][k]),
                               GRB.LESS_EQUAL, bt[j], "Time var recharge v{}".format(i))
    for i in N_s:
        for j in N_r:
            for k in range(I):
               model.addConstr(s[i][j]+(f-br[i][k])*epsilon - P(1-x[i][j][k]),
                               GRB.LESS_EQUAL, cr[j], "Time c var recharge v{}".format(j))
    for j in N_r:
        for k in range(I):
            model.addConstr(s[0][N_s[-1]](f-l[k])*epsilon - P(1-x[0][j][k]),
                               GRB.LESS_EQUAL, cr[j], "Time c var recharge source v{}".format(j))
    #resource constraints and time windows
    for i in N_:
        model.addConstr(bt[i]-p[i], GRB.GREATER_EQUAL, g[i], "Time window arrival t{}".format(i))
    for i in N_:
        model.addConstr(bt[i], GRB.LESS_EQUAL, d[i], "Time window deadline t{}".format(i))
    for k in range(I):
        model.addConstr(br[0][k], GRB.EQUAL, l[k], "Starting battery level v{}".format(k))
    for i in N_ + N_r:
        for k in range(I):
            model.addConstr(br[i][k], GRB.GREATER_EQUAL, q, "Resource min v{} t{}".format(k,i))
    for i in N_ + N_r:
        for k in range(I):
            model.addConstr(br[i][k], GRB.LESS_EQUAL, f, "Resource max v{} t{}".format(k,i))

    model.update()
    return model



def initSPP_Single(params,k):
    p = params.instance.ptimes #task processing i in N
    p_r =params.instance.charget
    #cr = params.cr
    f = params.instance.fullb #max 
    q = params.instance.minb #min
    g = params.instance.starttimes #start i in N
    d = params.instance.duedates #deadline i in N
    l = params.instance.blevels[k] #starting battery level
    om = params.instance.consumption #consumption i in N
    op = params.instance.chargea #param production 
    N = params.instance.n #nr tasks
    R = params.instance.stations #Stations
    s = params.instance.stimes #setup time
    b = params.b #conflict limit (each station has b spots)
    B = params.B #stations to penalize
    T = params.instance.maxtime
    #modes = params.modes
    #sigma = params.sigma #thresholds i in modes
    #eta = params.eta #y-intercept of some i in modes
    #Eta = params.Eta #Slope of some i in Modes
    mst = params.instance.mstarttimes[k]
    #I = params.I #nr vehicles
    N_ = range(1,N+1)
    N_s = range(0,N+2)
    N_r = range(N+2,N+2+R*b)
    N_sr = N_s + N_r
    #Sort into stations, eachs tation gets b recharges
    N_rphi = []
    for i in range(R):
        N_rphi.append([j for j in range(b*i,b*(i+1))])
    P = params.instance.M
    
    #Dual values
    gamma = params.gamma 
    epsilon = params.epsilon
    beta = params.beta[k]
    def convert_indice(index,start):
        if index >= N+2:
            if start:
                return 0
            else:
                return N+1
        return index
    
    model = Model("SPP-Single")



    x = []
    for i in N_sr:
        at = []
        for j in N_sr:
            tt = []
            for t in range(T):
                varname = "x({},{},{})".format(i,j,t)
                tt.append(model.addVar(vtype=GRB.BINARY,name=varname))
            at.append(tt)
        x.append(at)
    '''
    et = []
    for i in N_sr:
        varname = "endttime({})".format(i)
        et.append(model.addVar(vtype=GRB.CONTINUOUS,name=varname))
    '''
    br = []
    for t in range(T):
        varname = "batteryresource({}{})".format(i,t)
        br.append(model.addVar(vtype=GRB.CONTINUOUS,name=varname))
    model.update()
    '''
    cr = []
    for i in N_r:
        varname = "completionrecharges({})".format(i)
        cr.append(model.addVar(vtype=GRB.CONTINUOUS,name=varname))

    pr = []
    for i in N_r:
        varname = "processingrecharges({})".format(i)
        pr.append(model.addVar(vtype=GRB.CONTINUOUS,name=varname))
    o_r = []
    for i in N_r:
        varname = "generatedrecharge({})".format(i)
        
    #Applied dual variable values to recharge times
    epsilon_phir = []
    for i in N_r:
        varname = "appliedepsilonrecharge({})".format(i)
        epsilon_phir.append(model.addVar(vtype=GRB.CONTINUOUS,name=varname))

    #Mode
    z = []
    for i in N_r:
        at = []
        for om in modes:
            varname = "mode({},{})".format(om,i)
        at.append(model.addVar(vtype=GRB.BINARY,name=varname))
    '''  
    #objective
    #regular
    print [(p[j]+s[convert_indice(i,True)][j]-gamma[j-1]) for i in N_sr for j in N_ ]
    print ["{} {}".format(i,j)for i in N_sr for j in N_ ]
    terma = quicksum([(p[j]+s[convert_indice(i,True)][j]-gamma[j-1])*x[i][j][t] for i in N_sr for j in N_ for t in range(T) if i!=j])
    #recharge events
    #termb = quicksum([i for i in epsilon_phir])
    termb = quicksum([(p_r+s[convert_indice(i,True)][convert_indice(j,False)]-epsilon[phi])*x[i][j][t] for i in N_sr for phi in B for j in N_rphi[phi] for t in range(T) if i != j])
    model.setObjective((terma + termb+beta),GRB.MINIMIZE)
    model.update()

    #constraints
    #Coverage constraint
    '''
    for i in N_:
        terma = quicksum([x[i][j] for j in N_sr])
        model.addConstr(terma, GRB.EQUAL, 1, "Coverage t{}".format(i))
    '''
    #Flow constraints
    terma = quicksum([x[0][j][t] for j in N_sr for t in range(T) if j != 0])
    model.addConstr(terma, GRB.EQUAL, 1, "Flow Source")
    terma = quicksum([x[i][N_s[-1]][t] for i in N_sr for t in range(T) if i != N_s[-1]])
    model.addConstr(terma, GRB.EQUAL, 1, "Flow Sink")

    for h in N_sr:
        terma = quicksum([x[i][h][t] for i in N_sr for t in range(T) if i != h])
        termb = quicksum([x[h][j][t] for j in N_sr for t in range(T) if j != h])
        model.addConstr(terma-termb, GRB.EQUAL, 0, "Flow tasks t{}".format(h))

    #time index
    for t in range(T):
        for i in N_ + N_r:
            for j in N_ + N_r:
                if i == j:
                    continue
                if j > len(N_):
                    tmptime = p_r
                else:
                    tmptime = p[j]
                #generate range
                trange = range(0,min(T,t+s[convert_indice(i,True)][convert_indice(j,False)]+tmptime))
                terma = quicksum([x[i][j][tt] for tt in range(t,T)])
                termb = quicksum([x[j][h][ttt] for h in N_sr for ttt in trange if j != h])
                model.addConstr(terma-termb, GRB.EQUAL, 0, "Flow tasks t{}".format(h))
    '''
    #Resources and Time
    for i in N_sr:
        for j in N_s:
            model.addConstr(et[i]+ s[convert_indice(i,True)][j]+p[j] - P*(1-x[i][j][t]),
                               GRB.LESS_EQUAL, et[j], "Time var t{}".format(i))
    
    #constant recharge update time
    for i in N_sr:
        for j in N_sr:
            model.addConstr(et[i]+ s[convert_indice(i,True)][convert_indice(j,False)]+p_r - P*(1-x[i][j]),
                               GRB.LESS_EQUAL, et[j], "Time var t{}".format(i))
    for j in N_s:
        model.addConstr(mst+ s[0][j] + p[j] - P*(1-x[0][j]),
                            GRB.LESS_EQUAL, et[j], "Time var source t{}".format(j))
    #constant
    for j in N_r:
        model.addConstr(mst+ s[0][convert_indice(j,False)] + p_r - P*(1-x[0][j]),
                            GRB.LESS_EQUAL, et[j], "Time var source recharge t{}".format(j))

    model.addConstr(et[0],GRB.EQUAL, 0, "Time var source set")
    '''
    for i in N_sr:
        for j in N_r:
            if i == j:
                continue
            for t in range(1,T):
                model.addConstr(br[t-1]+ op + P*(x[i][j][t]),GRB.GREATER_EQUAL, br[t], "Resource var prod i{}t{}".format(i,t))
    print 4
    for i in N_sr:
        for j in N_s:
            if i == j:
                continue
            for t in range(1,T):
                model.addConstr(br[t-1]- om[j] + P*(1-x[i][j][t]),GRB.GREATER_EQUAL, br[t], "Resource var con i{}t{}".format(i,t))

    
    for t in range(1,T):
        sumij = [x[i][j][t] for j in N_sr for i in N_sr if i != j]
        model.addConstr(br[t-1] + P*(quicksum(sumij)),GRB.GREATER_EQUAL, br[t], "Resource var con i{}t{}".format(i,t))
    
    '''

    #Get accumulated time for recharge
    for i in N_s:
        for j in N_r:
            if i == j:
                continue
            model.addConstr(et[i]+s[i][j]+pr[j] - P*(1-x[i][j]),
                      GRB.LESS_EQUAL, et[j], "Time var recharge t{}".format(j))
    #Get recharge completion time
    for i in N_s + N_r:
        for j in N_r:
            for h in N_s:
                if i == j or j == h or i == h:
                    continue
                model.addConstr((et[h]-s[N_s[-1]][h]+p[h])-et[i] -P*(1-x[j][h]) - P*(1-x[i][j]),
                                   GRB.LESS_EQUAL, cr[j], "Time c var recharge t{}".format(j))
    for j in N_r:
        at = []
        att = []
        for i in N_sr:
            if i == j:
                continue
            at.append(s[i][N_s[-1]]*x[i][j])
            att.append(x[i][j])
        model.addConstr(cr[j]-quicksum(at) - P*(1-quicksum(att)),
                               GRB.LESS_EQUAL, pr[j], "Time p var recharge  t{}".format(j))
    '''
    #resource constraints and time windows
    for i in N_sr:
        for j in N_sr:
            if i == j:
                continue
            ttt = []
            tt = []
            if j < N_s[-1]:
                tt = [t for t in range(mst,g[j])]
                ttt = [t for t in range(d[j]+1,T)]
            
            trange = [t for t in range(0,mst)]+tt+ttt
            for t in trange:
                model.addConstr(x[i][j][t],GRB.EQUAL, 0, "Time window j{}t{}".format(j,t))
    '''
    for i in N_:
        model.addConstr(et[i]-p[i], GRB.GREATER_EQUAL, g[i], "Time window arrival t{}".format(i))
    for i in N_:
        model.addConstr(et[i], GRB.LESS_EQUAL, d[i], "Time window deadline t{}".format(i))
    '''
    model.addConstr(br[0], GRB.EQUAL, l, "Starting battery level")
    for t in range(T):
        model.addConstr(br[t], GRB.GREATER_EQUAL, q, "Resource min t{}".format(t))
    '''
    for i in N_ + N_r:
            model.addConstr(br[i], GRB.LESS_EQUAL, f, "Resource max t{}".format(i))
            '''
    model.update()
    return model, x, br  




def get_duals(model):
    for c in model.getConstrs():
        print c.pi
