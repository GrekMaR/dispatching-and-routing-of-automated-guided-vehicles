import os.path, random, sys, math
#import matplotlib.pyplot as plt
import rcpsp_newcallable as rcpsp
import bp_main
import bp_helpers
import bp_output
import instance as inst
from helperclasses import *
import subprocess
from MessageHandler import *





def processResults(taskoverview, tasks, results):
    addition = ""
    j = 0
    iteration = taskoverview.currenttime
    for i in results.machines:
        #get machine, waiting time, makespan...
        mtasks =taskoverview.machines[j].tasks
        rtasks = i['tasks']
        for i in mtasks:
            index = i.index
            addition += "{} {} {} {} ".format(index,entry,i.started+i.ptime,results.runtime)
            addition += "{} {} {} {}\n".format(j,makespan,len(results.machines),iteration)
        j = j +1


    return addition

def outputResults(taskoverview, tasks, results, res):
    #write data to res file
    
    line = processResults(taskoverview, tasks,results)
    try:
        #if empty output header
        if os.stat(res).st_size == 0:
            line = "index entry start completion solvetime assignedTo makespan nrvehicles iterations\n"+ line
        with open(res,"a") as f:
            f.write("\n"+line)
    except IOError:
        print "Error: cant find file or read data :("
        return False
        
    return True

def outputResultsSimple(taskoverview, tasks, results, res):
    #write data to res file
    
    line = ""
    try:
        #if it does not exist
        if taskoverview.currenttime == 0:
            f = open(res, 'w+')
            f.close()
            line = "nrtasks makespan solvetime timestep\n"
        line+="{} {} {} {}\n".format(len(tasks),results.makespan,results.runtime,taskoverview.currenttime)
        with open(res,"a") as f:
            f.write(line)
    except IOError:
        print "Error: cant find file or read data (outputResultsSimple) :("
        return False
        
    return True

def generateWaiting(taskoverview):
    s = "task entry start wait\n"
    s2 = "task entry start wait\n"
    for i,v in enumerate(taskoverview.tasks):
        #start is the actual start, without setup time
        entry = v.entry
        start = v.started
        s += "{} {} {} {}\n".format(i,entry,start,start-entry)
        #with setup
        start -= v.setup
        s2 += "{} {} {} {}\n".format(i,entry,start,start-entry)
    return (s,s2)


                                                  
    

def parseCPResults(CPresults, n):
    lines = Content(CPresults)
    machines = []
    line = lines.nextLine()
    solvetime = float(lines.line)
    makespan = int(round(float(lines.line)))
    mindex = -1
    while True:
        line = lines.nextLine()
        if line == "":
            break
        tokens = line.split(" ")
        if tokens[0] == "-":
            sched = []
            setups = []
            ptimes = []
            starts = []
            resultlist = [sched,ptimes,setups,starts]
            mindex = mindex +1
            indexing = 1
            for i in resultlist:
                line = lines.nextLine()
                tokens = line.split(" ")
                for j in tokens:
                    try:
                        intj = int(j)
                        if intj < 0:
                            intj = n
                        i.append(intj+indexing)
                    except ValueError:
                        continue
                indexing = 0
            machines.append({'index':mindex,'setups':setups,
                             'tasks':sched,'ptimes':ptimes,'starts':starts})
    #print machines
    return Results(machines,makespan,solvetime,0,0)


    

def readCPResults(fname, n):
    if not os.path.isfile(fname):
        print "File '"+fname+"' does not exist!"
        return False
    try:
        with open(fname) as f:
            content = f.readlines()
            #clean input
            for i,s in enumerate(content):
                content [i] = s.replace("\n","")
        results = parseCPResults(content, n)
        return results
    except IOError:
        print "Something went wrong opening the file '"+fname+"'..."
        sys.exit(0)



def outputTests(taskoverview, res, resfile,nrtasks, nrmachines):
    line = ""
    line = "{} {} {} {} {}".format(nrtasks,res.upper,res.lower,res.makespan,res.runtime)
    try:
        #if empty output header
        if os.stat(resfile).st_size == 0:
            line = "nrtasks upper lower makespan solvetime\n"+ line
        with open(resfile,"a") as f:
            f.write("\n"+line)
    except IOError:
        print "Error: cant find file or read data :("
        return False
        
    return True

'''

Uses simulation instances to run tests. Modifies them if needed too
'''
def runTests(fname,gfile,instancecpfile,resp,resc,nrtasks,nrmachines,cpprogram):
    runmip = True
    runcp = True
    f = open(resp,"w+")
    f.close()
    f = open(resc,"w+")
    f.close()
    #read instance file
    print "Reading instance files..."
    taskoverview = inst.readInstance(fname, gfile)
    tasks = taskoverview.tasks
    for j in nrmachines:
        diff = len(taskoverview.machines) - j
        if diff < 0:
            for k in range(abs(diff)):
                template = taskoverview.machines[random.randint(0,len(taskoverview.machines)-1)]
                v = Vehicle(len(taskoverview.machines),template.pos,template.batterylevel)
                taskoverview.machines.append(v)
        if diff > 0:
            for k in range(diff):
                del taskoverview.machines[-1]
        for i in nrtasks:
            tasksp = tasks[:i]
            instance = inst.preprocessInstance(tasksp, taskoverview,False)
            #print instance.instanceToString()
            
            if runmip:
                print "Calling MIP Model nr machines "+str(j)+" nr tasks "+str(i)
                resultsmip,gan,gantikz = rcpsp.runModel(instance)
                if not outputTests(taskoverview, resultsmip, resp,i, j):
                    print "Something went wrong writing the results "+resp
                    break
            if runcp:
                inst.overwriteFile(instancecpfile,instance.instanceToString())
                print "Calling CP Model nr machines "+str(j)+" nr tasks "+str(i)
                print cpprogram
                subprocess.call([cpprogram, instancecpfile, "tempcp.txt"])
                resultscp = readCPResults("tempcp.txt")
                if not outputTests(taskoverview, resultscp, resc,i, j):
                    print "Something went wrong writing the results "+resc
                    break
            

    
def runSimulation(fname, gfile, instancecpfile,resp, resc,cpprogram):
    '''
    if os.path.isfile(resp):
        print "File '"+resp+"' already exists!"
        return False
    if os.path.isfile(resc):
        print "File '"+resc+"' already exists!"
        return False
    '''
    f = open(resp,"w+")
    f.close()
    f = open(resc,"w+")
    f.close()
    #read instance file
    print "Reading instance files..."
    taskoverviewp = inst.readInstance(fname, gfile)
    #taskoverviewc = inst.readInstance(fname, gfile)
    print "Done reading instance files..."
    #preprocess setups and stuff
    while True:
        tasksp = taskoverviewp.getNextTasks()
        #tasksc = taskoverviewc.getNextTasks()
        #print tasksp
        if tasksp:
            instance = inst.preprocessInstance(tasksp, taskoverviewp,True)
            results = rcpsp.runModel(instance)
            inst.updateOverview(taskoverviewp,results,tasksp)
            if not outputResultsSimple(taskoverviewp, tasksp, results, resp):
                break
        else:
            break
        #if tasksc:
        if False:
            instance = inst.preprocessInstance(tasksc, taskoverviewc,True)
            inst.overwriteFile(instancecpfile,instance.instanceToString())
            #call cp model with instancecpfile
            subprocess.call([cpprogram, instancecpfile, "tempcp.txt"])
            results,gan,gantikz = readCPResults("tempcp.txt")
            inst.updateOverview(taskoverviewc,results,tasksc)
            if not outputResultsSimple(taskoverviewc, tasksc, results, resc):
                break
            #Assign tasks to machines in structures
        
def runRandomSimulation(fname,instancecpfile, instancegfile,instancehfile,resp, resc,cpprogram, resg,gprogram,ressph,resh,hprogram):
    '''
    if os.path.isfile(resp):
        print "File '"+resp+"' already exists!"
        return False
    if os.path.isfile(resc):
        print "File '"+resc+"' already exists!"
        return False
    '''
    '''
    f = open(resh,"w+")
    f.close()
    f = open("temph.txt","w+")
    f.close()

    f = open(resc,"w+")
    f.close()
    
    f = open(ressph,"w+")
    f.close()
    '''
    #read instance file
    print "Reading instance files..."
    taskoverviewp = inst.readInstance(fname)
    taskoverviewg = inst.readInstance(fname)
    taskoverviewsph = inst.readInstance(fname)
    taskoverviewh = inst.readInstance(fname)
    print taskoverviewsph.machines[0].full
    taskoverviewc = inst.readInstance(fname)
    
    oldcap = []
    '''
    for i in taskoverviewc.machines:
        oldcap.append(i.batterylevel)
        i.batterylevel = i.batterylevel*5'''
    print "Done reading instance files..."
    #preprocess setups and stuff
    timelimit = 60*60
    '''
    header = "\n nrtasks makespan solvetime timestep\n"
    overwriteFile(resp,header)
    for i in taskoverviewp.machines:
        i.batterylevel = 500
    '''
    runc = False
    runsph = True
    runp = False
    runh = True
    rung = True
    
    sampling = 5000
    dooutput = True
    while True:
        if runp:
            tasksp = taskoverviewp.getNextTasks()
        else:
            tasksp = False
        if runc:
            tasksc =taskoverviewc.getNextTasks()
        else:
            tasksc = False
        if runsph:
            taskssph = taskoverviewsph.getNextTasks()
        else:
            taskssph = False
        if runh:
            tasksh = taskoverviewh.getNextTasks()
        else:
            tasksh = False
        if rung:
            tasksg = taskoverviewg.getNextTasks()
        else:
            tasksg = False
        #print tasksp
        m = 8
        if tasksp:
            #print "nrtasks "+str(len(tasksp))
            tmp = ""
            for i in tasksp:
                tmp = tmp +" "+i.toString()
            
            n = len(tasksp)
            time = taskoverviewp.currenttime
            instance = inst.preprocessRandomInstance(tasksp, taskoverviewp,True)
            print "Running mip"
            results,gan,gantikz = rcpsp.runModel(instance,timelimit,"temptikz.txt",1,True)
            '''
            if taskoverviewp.currenttime in set([884,895,1025,1044,1088,1334,1336,1316]):
                overwriteFile("mip"+str(taskoverviewp.currenttime)+".test",instance.instanceToString()+"\n\n"+str(results.output()))
            '''
            if not results == False:
                 
                inst.updateOverview(taskoverviewp,results,tasksp)#only mark as busy<<<<
                if dooutput:
                    '''
                    if not appendToFile(resp,str(len(tasksp))+" "+str(results.makespan)+" "+str(results.runtime)+" "+str(taskoverviewp.currenttime)+"\n"):
                        break
                    '''
                    addTikzHeader(os.path.join("tikz","dtikzgantt_mip"+str(taskoverviewp.currenttime)+fname),
                              "Plan for online instance with "+str(m)+" vehicles and "+str(n)+" tasks",
                              "online step"+str(time))
                    appendToFile(os.path.join("tikz","dtikzgantt_mip"+str(taskoverviewp.currenttime)+fname),gantikz)
                    addTikzFooter(os.path.join("tikz","dtikzgantt_mip"+str(taskoverviewp.currenttime)+fname))
                    if not outputResultsSimple(taskoverviewp, tasksp, results, resp):
                        break
            else:
                tasksp = False
        #SPH
        if taskssph:
            #print "nrtasks "+str(len(tasksp))
            tmp = ""
            for i in taskssph:
                tmp = tmp +" "+i.toString()
            
            n = len(taskssph)
            time = taskoverviewsph.currenttime
            instance = inst.preprocessRandomInstance(taskssph, taskoverviewsph,True)
            print "Running sph"
            #results,gan,gantikz
            tasklimit = [int(math.ceil(instance.n/instance.m)+1),int(math.ceil(instance.n/instance.m)+1)]
            samplerange = [-1,1]
            fullrecharge = False
            inst.overwriteFile(instancegfile,instance.instanceToString())
            #print instance.instanceToString()
            startsol = True
            if time != 0:
                sampling = False
                startsol = False
            sol,problem = bp_main.runPartitioning(instance,tasklimit,True,sampling,samplerange,fullrecharge,3,True,startsol,15,timelimit)
            
            #print sol
            if problem.master.getAttr("SolCount")  >0:
                tikzoutsph = os.path.join("tikz","dtikzgantt_sph"+str(taskoverviewsph.currenttime)+fname)
                f = open(tikzoutsph,"w+")
                f.close()
                print bp_helpers.extractTestInfo(problem).getLine()
                bp_output.tikz_sol(sol,problem.params,tikzoutsph)

                if dooutput:
                    results = bp_output.solToSim(sol,problem)
                    #print results.machines
                    inst.updateOverview(taskoverviewsph,results,taskssph)
                    '''
                    if not appendToFile(resp,str(len(tasksp))+" "+str(results.makespan)+" "+str(results.runtime)+" "+str(taskoverviewp.currenttime)+"\n"):
                        break
                    '''
                    if not outputResultsSimple(taskoverviewsph, taskssph, results, ressph):
                        break
            else:
                taskssph = False
                print "Failed"
            
                
        
        #GREEDY
        
        if tasksg:
            '''
            tmp = ""
            for i in tasksg:
                tmp+= " {} ;".format(i.toString())
            print "tasks at time {}: \n {} ".format(taskoverviewg.currenttime,tmp)'''
            print "Running Greedy"
            n = len(tasksg)
            instance = inst.preprocessRandomInstance(tasksg, taskoverviewg,True)
            inst.overwriteFile(instancegfile,instance.instanceToString())
            #call greedy with instancegfile
            if dooutput:
                tikzoutg = os.path.join("tikz","tikzgantt_greedy"+str(taskoverviewg.currenttime)+fname)
            else:
                tikzoutg = "temptikzg.out"
            
            #program, input, output, best, jsout, tikzout, timelimit, battery resources
            print subprocess.call([gprogram,instancegfile,"tempg.txt",'tempbestg.txt',tikzoutg,str(timelimit),str(instance.stationlimit),"1","1337"])
            results = readCPResults("tempg.txt", len(tasksg))
            inst.updateOverview(taskoverviewg,results,tasksg)
            if dooutput:
                if not outputResultsSimple(taskoverviewg, tasksg, results, resg):
                    print "sorry"
                    break
            #Assign tasks to machines in structures


        if tasksc:
            instance = inst.preprocessRandomInstance(tasksc, taskoverviewc,True)
            inst.overwriteFile(instancecpfile,instance.instanceToString())
            #call cp model with instancecpfile
            
            print subprocess.call([cpprogram, instancecpfile, "tempcp.txt","temp.tikz",str(timelimit),str(instance.stationlimit),"1","0"])
            print "ran cp"
            results = readCPResults("tempcp.txt",len(tasksc))
            inst.updateOverview(taskoverviewc,results,tasksc)
            if not outputResultsSimple(taskoverviewc, tasksc, results, resc):
                break
            #Assign tasks to machines in structures
	#tasksc = False
        #HEURISTIC
        
        if tasksh:
            '''
            tmp = ""
            for i in tasksg:
                tmp+= " {} ;".format(i.toString())
            print "tasks at time {}: \n {} ".format(taskoverviewg.currenttime,tmp)'''
            print "Running Heuristics"
            n = len(tasksh)
            time = taskoverviewh.currenttime
            instance = inst.preprocessRandomInstance(tasksh, taskoverviewh,True)
            inst.overwriteFile(instancehfile,instance.instanceToString())
            #call greedy with instancegfile
            if dooutput:
                tikzouth = os.path.join("tikz","par1tikzgantt_ins_heuristic"+str(taskoverviewh.currenttime)+fname)
            else:
                tikzouth = "temptikzh.out"
            
            '''
            ./exe/heuristic instance/randinst_20_10.txt results/out.txt 1 3 1 1 0 1 results/tikz.txt 0 0 1 1 1 0 0 0 0

            ./exe/heuristic instance/randinst_20_10.txt results/out.txt 1 1 2 2 0 1 results/tikz.txt 0 0 1 1 0.5 0.5 0 0 0

            ./exe/heuristic instance/randinst_20_10.txt results/out.txt 3 3 2 3 0 1 results/tikz.txt 0 0 1 1 0.4 0.4 0.2 0.5 0.5

            '''
            seeout = subprocess.call([hprogram,instancehfile,"temph.txt","1","1","1","1","0","1",tikzouth,"0.5","0.5","1","1","0",str(instance.stationlimit),"0","0","0","0","1","1337"])
            print seeout
            results = readCPResults("temph.txt", len(tasksh))
            inst.updateOverview(taskoverviewh,results,tasksh)
            if dooutput:
                if not outputResultsSimple(taskoverviewh, tasksh, results, resh):
                    print "sorry"
                    tasksh = False
            #Assign tasks to machines in structure
            

        if not taskssph and not tasksp and not tasksg and not tasksh:
            break
        #if tasksc:
        timelimit = 60

    if runp:
        waitstats,waitstatssetup = generateWaiting(taskoverviewp)
        overwriteFile("wait"+resp,waitstats)
        overwriteFile("waitsetup"+resp,waitstatssetup)
    if runc:
        waitstats,waitstatssetup = generateWaiting(taskoverviewc)
        overwriteFile("wait"+resc,waitstats)
        overwriteFile("waitsetup"+resc,waitstatssetup)
    if rung:
        waitstats, waitstatssetup = generateWaiting(taskoverviewg)
        overwriteFile("wait"+resg,waitstats)
        overwriteFile("waitsetup"+resg,waitstatssetup)
    if runsph:
        waitstats, waitstatssetup = generateWaiting(taskoverviewsph)
        overwriteFile("wait"+ressph,waitstats)
        overwriteFile("waitsetup"+ressph,waitstatssetup)
    if runh:
        waitstats, waitstatssetup = generateWaiting(taskoverviewh)
        overwriteFile("wait"+resh,waitstats)
        overwriteFile("waitsetup"+resh,waitstatssetup) 
    
    '''
        for i in taskoverviewc.machines:
            i.batterylevel = i.batterylevel/5'''
runRandomSimulation("taskplan-new600.txt", "inpcp1.txt","inpg.txt","inph.txt","dpyout600.txt", "cppout600.txt", "./solver","greedyout600.txt","./solverg","sphout600.txt","heurout600.txt","./heuristic")        
#runTests("tinstance6.txt", "graph2.txt", "inpcp1.txt","pyout2.txt", "cppout2.txt",range(25,9,-5),range(10,4,-5),'./solver')
