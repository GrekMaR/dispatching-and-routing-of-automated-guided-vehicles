#rcpsp instans generator
import os.path, sys
from helperclasses import *



def parseInstance(fname):
    if not os.path.isfile(fname):
        print "File '"+fname+"' does not exist!"
        return
    try:
        with open(fname) as f:
            content = f.readlines()
        instance = parseContents(content)
        f.close()
        return instance
    except IOError:
        print "Something went wrong opening the file..."
        sys.exit(0)

def parseContents(content):
    
    i = 0
    line = content[i]
    cc = Content(content)
    ptimes = []
    stimes = []
    starttimes = []
    duedates =[]
    blevels = []
    consumption = []
    fullb = 0
    minb = 0
    charget = 0
    chargea = 0
    nrstations = 1
    stationlimit = 5
    modes =[]
    if "#" in line:
        line = cc.nextLine()
        
        j = 0
        while True:
            try:
                #n then m
                int(line)
                line = cc.nextLine()
            except ValueError:
                break
    if "-p" in line:
        line = cc.nextLine()
        ptimes = []
        while True:
            try:
                ptimes.append(int(line))
                line = cc.nextLine()
            except ValueError:
                break
    if "-M" in line:
        line = cc.nextLine()
        j = 0
        try:
            int(line)
            line = cc.nextLine()
        except ValueError:
            print "Expected an integer..."
    if "-st" in line:
        line = cc.nextLine()
        starttimes = []
        while True:
            try:
                starttimes.append(int(line))
                line = cc.nextLine()
            except ValueError:
                break

    if "-d" in line:
        line = cc.nextLine()
        dtimes = []
        while True:
            try:
                dtimes.append(int(line))
                line = cc.nextLine()
            except ValueError:
                duedates = dtimes
                break
    if "-s" in line:
        line = cc.nextLine()
        setups = True
        allsetups = []
        n = len(ptimes)
        while setups:
            
            eachs = line.split(" ")
            sett = []
            for k in eachs:
                try:
                    sett.append(int(k))
                except ValueError:
                    setups = False
            if setups:
                allsetups.append(sett)
                line = cc.nextLine()
        stimes = allsetups
    if "-b" in line:
        line = cc.nextLine()
        j = 0
        index = ['f','q','e','b']
        collector = {} 
        while True:
            try:
                # b then q
                collector[index[j]] = int(line)
                j = j +1 
                line = cc.nextLine()
            except ValueError:
                fullb = collector['f']
                minb = collector['q']
                charget = collector['e']
                chargea = collector['b']
                break
    if "-l" in line:
        line = cc.nextLine()
        l = []
        while True:
            try:
                l.append(int(line))
                line = cc.nextLine()
            except ValueError:
                blevels = l
                break
    if "-c" in line:
        line = cc.nextLine()
        c = []
        while True:
            try:
                c.append(int(line))
                line = cc.nextLine()
            except ValueError:
                consumption = c
                break
    mst = []
    if "-mst" in line:
        line = cc.nextLine()
        
        while True:
            try:
                mst.append(int(line))
                line = cc.nextLine()
            except ValueError:
                break
    if "-re" in line:
        line = cc.nextLine()
        try:
            nrstations = int(line)
            line = cc.nextLine()
            stationlimit = int(line)
            line = cc.nextLine()
        except ValueError:
            line = cc.nextLine()
    if "-mo" in line:
        line = cc.nextLine()
        while True:
            try:#x,y x,y H eta
                mode = line.split(" ")
                for i,v in enumerate(mode):
                    mode[i] = int(v)
                modes.append(mode[i])
                line = cc.nextLine()
            except ValueError:
                break
    instance = Instance(ptimes,
                             stimes,starttimes,
                             duedates,blevels,
                             consumption,fullb,
                             minb,charget, chargea,modes)
    instance.mstarttimes = mst
    instance.stations = nrstations
    instance.stationlimit = stationlimit
    #instance.modes = modes #startlim endlim slope intercept
    return instance
        
                
                
            

#createInstance(12,5,False,False,True,"test2.txt")
#createMapping("aabenraa_stue.png")
