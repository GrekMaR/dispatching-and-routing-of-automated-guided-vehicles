from gurobipy import *
from helperclasses import *
import random
from MessageHandler import *
import math
from decimal import Decimal

#Model for battery and setup times

#fname = "instances/testbasic2.txt"
#instance.createInstance(random.randint(3,10),random.randint(2,3),True,True,True,fname)


class MIPStats():
    def __init__(self, tasks, vehicles):
        self.n = tasks
        self.m = vehicles
        self.ub = []
        self.lb = -1
        self.gap = 0
        self.nodes = 0
        self.pre_rows = 0
        self.pre_cols = 0
        self.rows = 0
        self.cols = 0
        self.time = 0
        self.obj = 0
    def output(self):
        if len(self.ub) <1:
            self.ub.append([-1,-1])
        s = str(self.m)+" & "+str(self.n)+" & "+str(self.pre_cols)+" & "
        s = s +str(self.cols)+" & "+ str(self.pre_rows)+" & "+str(self.rows)+" & "
        s = s +str(self.ub[0][0]) + " & "+str(decimal(self.ub[0][1],4))+" & "+str(self.ub[-1][0])
        s = s +" & "+str(self.lb)+" & "+str(decimal(self.gap,4))+" & "+str(self.nodes)+" & "+str(decimal(self.time,4))
        s = s + "\\\ \hline \n"
        
        return s
        

def runModel(*inp):
    instance = inp[0]
    
    n = instance.n
    m = instance.m
    M = instance.M
    p = instance.ptimes

    s = instance.stimes
    f = instance.fullb
    q = instance.minb
    l = instance.blevels
    e = instance.charget
    con = instance.consumption
    #con.append(0)
    #con.insert(0,0)
    b = instance.chargea
    d = instance.duedates
    st = instance.starttimes
    mst = instance.mstarttimes
    modes = instance.modes
    ftime = modes[-1].end[1]
    if len(mst) == 0:
        mst = [0]*m
    ch = e/float(b)
    
    model = Model("rcpsp")
    #model.params.ConcurrentMIP = 2
    fullcharge = 0 #0 full, 1 constant, 2 flexible
    if len(inp)>1:
        model.params.timeLimit = inp[1]
        if len(inp)>2:
            fullcharge = inp[3]
            depotend = inp[4]
    else:
        model.params.timeLimit = 60
    #model.setParam('OutputFlag', False )

    #variables for predecessors/successors, activitya,activityb,machine
    x = []
    for k in range(m):
        at = []
        for i in range(n+2):
            bt = []
            for j in range(n+2):
                predname = "x"+str(k)+" "+str(i)+" "+str(j)
                bt.append( model.addVar(vtype=GRB.BINARY,name=predname))
            at.append(bt)
        x.append(at)

    c = []
    for i in range(n+2):
        c.append(model.addVar(vtype=GRB.CONTINUOUS,name="C"+str(i)))
    #charge time
    ct = []
    for i in range(n+2):
        temp = []
        for k in range(m):
            temp.append(model.addVar(vtype=GRB.CONTINUOUS,name="chargetime"+str(i)))
        ct.append(temp)

    #charge amount
    ca = []
    if fullcharge!=0:
        for i in range(n+2):
            ttemp = []
            for k in range(m):
                ttemp.append(model.addVar(vtype=GRB.CONTINUOUS,name="charge amount t{} k{}".format(i,k)))
            ca.append(ttemp)

    #charge mode
    cm = []
    if fullcharge != 0:
        for j in range(len(modes)):
            temp = []
            for i in range(n+2):
                ttemp = []
                for k in range(m):
                    tttemp = []
                    for oo in range(2):
                        tttemp.append(model.addVar(vtype=GRB.BINARY,name="charge mode t{} m{} k{} o{}".format(i,j,k,oo)))
                    ttemp.append(tttemp)
                temp.append(ttemp)
            cm.append(temp)
    else:
        for j in range(len(modes)):
            temp = []
            for i in range(n+2):
                ttemp = []
                for k in range(m):
                    ttemp.append(model.addVar(vtype=GRB.BINARY,name="charge mode t{} m{} k{} ".format(i,j,k)))
                temp.append(ttemp)
            cm.append(temp)    
    
    o = []
    for i in range(n+2):
        ttemp = []
        for k in range(m):
            ttemp.append(model.addVar(vtype=GRB.CONTINUOUS,name="O"+str(i)))
        o.append(ttemp)
    '''
    u = []
    for i in range(n+2):
        u.append(model.addVar(vtype=GRB.INTEGER,name="U"+str(i)))
    '''
    lindex = n+1
    if depotend:
        #If we drive to the depot at the end of our schedule adn recharge
        lindex = n+2
    z = []
    for k in range(m):
        bt = []
        for i in range(lindex):
            at= []
            for j in range(lindex):
                at.append(model.addVar(vtype=GRB.BINARY,name="Z"+str(k)+"_"+str(i)+"_"+str(j)))
            bt.append(at)
        z.append(bt)

    cmax = model.addVar(vtype=GRB.CONTINUOUS,name="Cmax")
    model.update()
    #Set objective 
    model.setObjective(cmax, GRB.MINIMIZE)
    model.update()

    #constraint one
    '''
    Each job only has one predecessor on a machine
    '''
    for i in range(1,n+1):
        terms = []
        for k in range(m):
            for j in range(1,n+2):
                if i == j:
                    continue
                terms.append(x[k][i][j])
        model.addConstr(quicksum(terms),GRB.EQUAL,1,"allonce1")

    #constraint two
    '''
    Each job can only have one successor on one machine.
    '''
    for i in range(1,n+1):
        terms = []
        for k in range(m):
            for j in range(0,n+1):
                if i == j:
                    continue
                terms.append(x[k][j][i])
        model.addConstr(quicksum(terms),GRB.EQUAL,1,"allonce2")

    #constraint 3
    '''
    Makes sure the first dummy job only has one successor for each machine
    '''
    for k in range(m):
        terms =[]
        termb = []
        for i in range(1,n+2):
            terms.append(x[k][0][i])
            #model.addConstr(x[k][i][i],GRB.EQUAL,0)
        model.addConstr(quicksum(terms),GRB.EQUAL,1)
        #model.addConstr(x[k][0][0],GRB.EQUAL,0)

    #constraint 4
    '''
    Makes sure the last dummy job only has one predecessor for each machine
    '''
    for k in range(m):
        terma = []
        termb = []
        for i in range(0,n+1):
            terma.append(x[k][i][n+1])
        model.addConstr(quicksum(terma),GRB.EQUAL,1,"lastonce")
        #Ensure only one pred to n+1
        model.addConstr(x[k][n+1][n+1],GRB.EQUAL,0)
        
    #constraint 5
    '''
    Ensure that if a predecessor is selected, a successor is also selected
    '''
    for k in range(m):
        for i in range(1,n+1):
            terma = []
            termb = []
            for j in range(1,n+2):
                if i == j:
                    continue
                terma.append(x[k][i][j])
            terma = quicksum(terma)
            for h in range(0,n+1):
                if i == h:
                    continue
                termb.append(x[k][h][i])
            termb = quicksum(termb)
            model.addConstr(terma - termb,GRB.EQUAL,0,"bothselected")

    #constraint 6
    '''
    Set the completion time with recharge time
    '''
    for k in range(m):
        for i in range(1,n+1):
            for j in range(1,n+1):
                if j == i:
                    continue
                model.addConstr(c[i]-c[j]+M*(1-x[k][j][i])+M*(1-z[k][j][i]),
                                GRB.GREATER_EQUAL,p[i]+
                                s[n+1][i]+s[j][n+1] + ct[i][k],"complete1")
    #regular completion time no charge   
    for k in range(m):
        for i in range(1,n+1):
            for j in range(1,n+1):
                if j == i:
                    continue
                model.addConstr(c[i]-c[j]+M*(1-x[k][j][i]),
                                GRB.GREATER_EQUAL,p[i]+s[j][i],"complete2")

    #first added machine starttime(busy time) forsource recharge
    
    for k in range(m):
        for j in range(1,lindex):
            model.addConstr(c[j]-mst[k]+M*(1-x[k][0][j])+M*(1-z[k][0][j]),
                                GRB.GREATER_EQUAL,p[j]+
                                s[0][j] + ct[j][k],"complete1")

                
    #source no recharge, busy
        
    for k in range(m):
        for j in range(1,n+1):
            model.addConstr(c[j]-mst[k]+M*(1-x[k][0][j]),
                                GRB.GREATER_EQUAL,p[j]+
                                s[0][j] ,"complete1")

    
    if fullcharge== 0:
        #If we alaways charge to full capacity
        #set charge time
        for omm in range(len(modes)):
            for i in range(1,n+1):
                for j in range(0,n+1):
                    if i == j:
                        continue
                    for k in range(m):
                        model.addConstr(ct[i][k]+M*(1-cm[omm][j][k])+M*(1-z[k][j][i]),
                                    GRB.GREATER_EQUAL,ftime-
                                            modes[omm].slope*o[j][k]+modes[omm].intercept,"charge time set")
    else:
        #set charge time
        for om in range(len(modes)):
            for omm in range(len(modes)):
                for i in range(1,n+1):
                    for j in range(0,n+1):
                        if i == j:
                            continue
                        for k in range(m):
                            model.addConstr(ct[i][k]+M*(1-cm[om][i][k][1])+M*(1-cm[omm][j][k][0])+M*(1-z[k][j][i]),
                                    GRB.GREATER_EQUAL,modes[om].slope*ca[i][k]+modes[om].intercept-
                                            modes[omm].slope*o[j][k]+modes[omm].intercept,"charge time set")
        for om in range(len(modes)):
            for omm in range(len(modes)):
                for i in range(1,n+1):
                    for j in range(0,n+1):
                        if i == j:
                            continue
                        for k in range(m):
                            model.addConstr(ct[i][k]-M*(1-cm[om][i][k][1])-M*(1-cm[omm][j][k][0])-M*(1-z[k][j][i]),
                                    GRB.LESS_EQUAL,modes[om].slope*ca[i][k]+modes[om].intercept-
                                            modes[omm].slope*o[j][k]+modes[omm].intercept,"charge time set")



    #
    
    if depotend:
        #set charge time depotend f-u[j] regular to sink to force 
        for k in range(m):
            for j in range(1,n+1):
                model.addConstr(c[n+1]-c[j]+M*(1-x[k][j][n+1])+M*(1-z[k][j][n+1]),
                                    GRB.GREATER_EQUAL,s[j][n+1] + ct[n+1][k],"complete1")
        if fullcharge != 0:
            #use recharge variable if not full for depotend recharge
            for k in range(m):
                for j in range(1,n+1):
                    model.addConstr(ca[n+1][k]-M*(1-z[k][j][n+1]),
                                        GRB.LESS_EQUAL,f,"charge amount last")

            for k in range(m):
                for j in range(1,n+1):
                    model.addConstr(ca[n+1][k]+M*(1-z[k][j][n+1]),
                                        GRB.GREATER_EQUAL,f,"charge amount last")
            
            #set charge time
            for omm in range(len(modes)):
                for j in range(1,n+1):
                    for k in range(m):
                        model.addConstr(ct[n+1][k]+M*(1-cm[omm][j][k][0])+M*(1-z[k][j][n+1]),
                                        GRB.GREATER_EQUAL,ftime-
                                                modes[omm].slope*o[j][k]+modes[omm].intercept,"charge time set last")
        else:
            #set charge time
            for omm in range(len(modes)):
                for j in range(1,n+1):
                    for k in range(m):
                        model.addConstr(ct[n+1][k]+M*(1-cm[omm][j][k])+M*(1-z[k][j][n+1]),
                                        GRB.GREATER_EQUAL,ftime-
                                                modes[omm].slope*o[j][k]+modes[omm].intercept,"charge time set last")

        for k in range(m):
            for j in range(1,n+1):
                model.addConstr(z[k][j][n+1]-x[k][j][n+1],GRB.GREATER_EQUAL,0,"x iff z last")

        
        


    else:
        #sink no charge or depot, time zero
        for k in range(m):
            for j in range(1,n+1):
                model.addConstr(c[j]-c[n+1]+M*(1-x[k][j][n+1]),
                                    GRB.GREATER_EQUAL,0 ,"complete1")



    
    '''
    #starting charge time (no recharge  between source and sink)
    for om in range(len(modes)):
        for omm in range(len(modes)):
            for i in range(1,n+1):
                for k in range(m):
                    model.addConstr(ct[i][k]+M*(1-cm[om][i][k])+M*(1-cm[omm][0][k])+M*(1-z[k][0][i]),
                                GRB.GREATER_EQUAL,modes[omm].slope*ca[0][k]+modes[omm].intercept -
                                        modes[om].slope*ca[i][k]+modes[om].intercept,"charge time set")
    '''
    if fullcharge == 0:

        #only current level modes
        for om in range(len(modes)):
            for i in range(0,lindex):
                for k in range(m):
                    model.addConstr(modes[om].start[0]*cm[om][i][k],
                                    GRB.LESS_EQUAL, o[i][k],"charge time set")
        for om in range(len(modes)):
            for i in range(0,lindex):
                for k in range(m):
                    model.addConstr(modes[om].end[0]*cm[om][i][k],
                                    GRB.GREATER_EQUAL, o[i][k],"charge time set")


        for i in range(0,lindex):
            for k in range(m):
                temp = []
                for om in range(len(modes)):
                    temp.append(cm[om][i][k])
                model.addConstr(quicksum(temp),
                                    GRB.LESS_EQUAL, 1,"charge time set")


        for k in range(m):
            for i in range(1,n+1):
                for j in range(0,n+1):
                    if j == i:
                        continue
                        model.addConstr(o[i][k],GRB.GREATER_EQUAL,
                                        f-con[i]-M*(1-z[k][j][i]),"prod upper1")
        
        for k in range(m):
            for i in range(1,n+1):
                for j in range(0,n+1):
                    if j == i:
                        continue
                    model.addConstr(o[i][k],GRB.LESS_EQUAL,
                                        f-con[i]+M*(1-z[k][j][i]),"prod lower")
    else:
        #modes set with variable recharging point
        for om in range(len(modes)):
            for i in range(0,lindex):
                for k in range(m):
                    model.addConstr(modes[om].start[0]*cm[om][i][k][1],
                                    GRB.LESS_EQUAL, ca[i][k],"charge time set")
        for om in range(len(modes)):
            for i in range(0,lindex):
                for k in range(m):
                    model.addConstr(modes[om].end[0]*cm[om][i][k][1],
                                    GRB.GREATER_EQUAL, ca[i][k],"charge time set")
        #only current level modes
        for om in range(len(modes)):
            for i in range(0,lindex):
                for k in range(m):
                    model.addConstr(modes[om].start[0]*cm[om][i][k][0],
                                        GRB.LESS_EQUAL, o[i][k],"charge time set")
        for om in range(len(modes)):
            for i in range(0,lindex):
                for k in range(m):
                    model.addConstr(modes[om].end[0]*cm[om][i][k][0],
                                        GRB.GREATER_EQUAL, o[i][k],"charge time set")
        #atmost one mode
        for i in range(0,lindex):
            for k in range(m):
                temp = []
                for om in range(len(modes)):
                    temp.append(cm[om][i][k][1])
                model.addConstr(quicksum(temp),
                                    GRB.LESS_EQUAL, 1,"charge time set")

        for i in range(0,lindex):
            for k in range(m):
                temp = []
                for om in range(len(modes)):
                    temp.append(cm[om][i][k][0])
                model.addConstr(quicksum(temp),
                                    GRB.LESS_EQUAL, 1,"charge time set")

        #Battery production update
        for k in range(m):
            for i in range(1,n+1):
                for j in range(0,n+1):
                    if j == i:
                        continue
                        model.addConstr(o[i][k],GRB.GREATER_EQUAL,
                                        ca[i][k]-con[i]-M*(1-z[k][j][i]),"prod upper1")
        for k in range(m):
            for i in range(1,n+1):
                for j in range(0,n+1):
                    if j == i:
                        continue
                        model.addConstr(ca[i][k],GRB.GREATER,
                                        o[j][k]-M*(1-z[k][j][i]),"prod upper1")
        
        for k in range(m):
            for i in range(1,n+1):
                for j in range(0,n+1):
                    if j == i:
                        continue
                    model.addConstr(o[i][k],GRB.LESS_EQUAL,
                                        ca[i][k]-con[i]+M*(1-z[k][j][i]),"prod lower")



                
    '''
    Due dates
    '''
    for i in range(1,n+1):
        model.addConstr(c[i],GRB.LESS_EQUAL,d[i],"duedate")

    '''
        start
    '''
    for i in range(1,n+1):
        model.addConstr(c[i]-p[i],GRB.GREATER_EQUAL,st[i],"startdate")
        

    #constraint 7
    '''
    Completion time zero for dummy job
    '''
    model.addConstr(c[0],GRB.EQUAL,0)

    #constraint 8
    '''
    Restrict all completion times to cmax
    '''
    for i in range(1,n+2):
        model.addConstr(c[i],GRB.LESS_EQUAL,cmax,"mincmax")


    #battery constraints
        '''
    Set production
    '''

    for k in range(m):
        for i in range(1,lindex):
            for j in range(0,n+1):
                model.addConstr(x[k][j][i]-z[k][j][i],GRB.GREATER_EQUAL,0,"z iff x")
           



    for k in range(m):
        for i in range(1,n+1):
            for j in range(0,n+1):
                if j == i:
                    continue
                model.addConstr(o[i][k]+M*(1-x[k][j][i])+M*(z[k][j][i]),GRB.GREATER_EQUAL,o[j][k]-con[i],"prod no charge up")

    for k in range(m):
        for i in range(1,n+1):
            for j in range(0,n+1):
                if j == i:
                    continue
                model.addConstr(o[i][k],GRB.LESS_EQUAL,o[j][k]-con[i]+M*(1-x[k][j][i])+M*(z[k][j][i]),"prod no charge low")


    #Start battery level for each vehicle
    for k in range(m):
        for i in range(1,lindex):
            model.addConstr(o[i][k],GRB.GREATER_EQUAL,
                                    l[k]+ca[i][k]-M*(1-z[k][0][i]),"prod upper1")

    for k in range(m):
        for i in range(1,lindex):
                model.addConstr(o[i][k],GRB.LESS_EQUAL,
                                    l[k]+ca[i][k]+M*(1-z[k][j][i]),"prod lower")

    for k in range(m):
        for i in range(1,n+1):
            model.addConstr(o[0][k]+M*(1-x[k][0][i]),GRB.GREATER_EQUAL,l[k],"startprodl")

    for k in range(m):
        for i in range(1,n+1):
            model.addConstr(o[0][k]-M*(1-x[k][0][i]),GRB.LESS_EQUAL,l[k],"startprodu")
    
    '''
    Limit the resource level to the min and max battery power level
    '''
    '''
    for i in range(1,n+1):
        ttemp = []
        for k in range(m):
            ttemp.append(ca[i][k])
        model.addConstr(quicksum(ttemp)-u[i],GRB.LESS_EQUAL,f,"full")
    for i in range(1,n+1):
        ttemp = []
        for k in range(m):
            ttemp.append(ca[i][k])
        model.addConstr(quicksum(ttemp)-u[i],GRB.GREATER_EQUAL,q,"lim")    
    '''
    
    for i in range(0,lindex):
        for k in range(m):
            for j in range(0,n+1):
                model.addConstr(o[i][k]-M*(1-x[k][j][i]),GRB.LESS_EQUAL,f,"full")
        
        
        
    for i in range(0,lindex):
        for k in range(m):
            for j in range(0,n+1):
                model.addConstr(o[i][k]+M*(1-x[k][j][i]),GRB.GREATER_EQUAL,q,"lim")
        
    def outputStats(out):
        
        #add all stats to object
        #if model.status == GRB.OPTIMAL:
        if model.getAttr("SolCount")  >0:
            stats.obj = round(model.objVal,1)
            stats.lb = round(model.objBound,1)
            if model.getAttr("ObjVal") != 0:
                stats.gap = abs( -1.0 + model.getAttr("ObjBound") / model.getAttr("ObjVal") )*100
            
        
        stats.nodes = model.getAttr("NodeCount")

        
        stats.time = model.runtime
        s = stats.output()
        appendToFile(out,s)


        return stats
        
        
    def outputSolution():
        #if model.status == GRB.OPTIMAL:
        if model.getAttr("SolCount")  >0:   
            #create Results object, and enter schedule with times
            #task.started = prev.started + prev.ptime + setup
            #makespan too
            '''for i in range(n):
                print c[i+1].getAttr('x')'''
            allmachines = []
            for i in range(m):
                mtasks= {}
                for j in range(n+1):
                    for k in range(1,n+2):
                        if x[i][j][k].getAttr("x") == 1:
                            mtasks[str(j)] = str(k)
                            #print "i"+str(i)+"xj:"+str(j)+" k:"+str(k)
                            #print x[i][j][k]
                ztasks= {}
                for j in range(n+1):
                    for k in range(1,lindex):
                        if z[i][j][k].getAttr("x") == 1:
                            if j == 0 and (l[i] == f):
                                continue
                            ztasks[str(j)] = str(k)
                            #print "i"+str(i)+"zj:"+str(j)+" k:"+str(k)
                            #print z[i][j][k]
                key = "0"
                pkey = key
                sched = []
                setups = []
                ptimes = []
                starts = []
                cons = []
                prod = []
                #print mtasks
                #print ztasks
                #append setup time on index for setup after
                if fullcharge != 0:
                    for j in range(n+2):
                        print "ca{}{} {}".format(j,i,ca[j][i].getAttr("x"))
                        print "o{}{}  {}".format(j,i,o[j][i].getAttr("x"))
                while key != str(n+1):
                    
                    nkey = mtasks[key]
                    inkey = int(nkey)
                    ikey = int(key)
                    
                    if key in ztasks:
                        print "the {}".format(ct[inkey][i].getAttr("x"))
                        if key == str(0):
                            if fullcharge == 0:
                                amount = f
                            else:
                                amount = round(ca[inkey][i].getAttr("x"),2)
                            print amount
                            lmode = None
                            amode = None
                            for t in modes:
                                if t.inTMode(l[i]):
                                    lmode = t
                                if t.inTMode(amount):
                                    amode = t
                                    break
                            cht = ct[inkey][i].getAttr("x")
                            
                            chargetime = amode.getTime(amount)-lmode.getTime(l[i])
                            printMessage("detecting charges "+str(chargetime),0,False)
                            #printMessage("u detecting charges "+str(u[int(key)].getAttr("x")),0,False)
                            prod.append(amount)
                        else:
                            if fullcharge == 0:
                                amount = f
                            else:
                                amount = round(ca[inkey][i].getAttr("x"),2)
                            level = round(o[ikey][i].getAttr("x"),2)
                            lmode = None
                            amode = None
                            print Decimal(amount)
                            print level
                            
                            for t in modes:
                                print amount
                                print t.end[0]
                                print math.ceil(amount)
                                print amount <= float(t.end[0])
                                if t.inTMode(level):
                                    lmode = t
                                if t.inTMode(amount):
                                    amode = t
                                    break
                            cht = ct[inkey][i].getAttr("x")
                            chargetime = amode.getTime(amount)-lmode.getTime(level)
                            prod.append(amount)
                            printMessage("other detecting charges "+str(chargetime),0,False)
                        sched.append(n+1)
                        ptimes.append(chargetime)#charge time
                        setups.append(s[int(key)][n+1])
                        setups.append(s[n+1][int(nkey)])
                        if len(starts) > 0:
                            starts.append(starts[-1]+setups[-2]+ptimes[-2])
                        else:
                            starts.append(setups[-2])
                        cons.append(0)
                        
                    else:
                        setups.append(s[int(key)][int(nkey)])
                    if nkey == str(n+1):
                        key = nkey
                        continue
                    sched.append(int(nkey))
                    ptimes.append(p[int(nkey)])
                    cons.append(con[inkey])
                    prod.append(round(o[int(nkey)][i].getAttr("x"),5))
                    starts.append(round(c[int(nkey)].getAttr('x'),5)-ptimes[-1])
                    pkey = key
                    key = nkey
                allmachines.append({'index':i,'setups':setups,
                                    'tasks':sched,'ptimes':ptimes,'cons':cons,'prod':prod,"starts":starts})
                
            results = Results(allmachines,model.objVal, model.runtime, cmax.ub, model.getAttr("ObjBound"))
            return results
        else:
            print "No solution"
            #model.computeIIS()
            #model.write("model.ilp")
            return False
    def outputGANTTTikz(results):
        printMessage(printSolution(results,n),0, False)
        printMessage("makespan: "+str(results.makespan),0,False)
        s = ""
        height = len(results.machines)+1
        width = 0
        s = s + "\\foreach \\r in {0,...,"+str(len(results.machines)-1)+"}\n"
        s = s + "   \draw (0,\\r) node[inner sep=1pt,below=3pt,rectangle,fill=white, left=2pt] {$\\r$};\n"
        s = s + "\draw[thick,->,black] (0,-1)--(0,"+str(len(results.machines))+") node[left] {$vehicles$};\n"
        y = 0
        for i in results.machines:
            
            x = mst[i['index']]*0.1
            tindex = 0
            for j in i['tasks']:
                label = str(j-1)
                color = "rgb:red,2;green,5;yellow,0"
                if j == n+1:
                    label = "r"
                    color = "rgb:red,2;green,5;yellow,8"
                #Check for idle time
                arrival = (x*10+i['setups'][tindex])
                if st[j] > arrival:
                    idle = st[j]-arrival
                    x = x + idle*0.1
                #setup time
                oldx = x
                x = x + i['setups'][tindex]*0.1
                #s = s + "\draw("+str(oldx)+","+str(y)+") coordinate (A) -- ("+str(x)+","+str(y)+") coordinate (B);\n"
                s = s + "\draw[fill=gray] ("+str(oldx)+","+str(y-0.20)+") rectangle ("+str(x)+","+str(y+0.20)+") "
                s = s + "node[below=5pt,left="+str((x-oldx)*10)+"pt, right=0, text width=5em] {$ $};\n"
                oldx = x
                if label == "r":
                    printMessage("strange r "+str((x-oldx)*10),0,True )
                x = x + i['ptimes'][tindex]*0.1
                s = s + "\draw[fill={"+color+"}] ("+str(oldx)+","+str(y-0.20)+") rectangle ("+str(x)+","+str(y+0.20)+") "
                s = s + "node[below=5pt,left="+str((x-oldx)*10)+"pt, right=0, text width=5em] {$"+label+"$};\n"
                tindex = tindex + 1
            y = y+1
            if x > width:
                width = x
        s = s + "\draw[thick,->,black] (0,-1)--("+str(width)+",-1) node[left, below=10pt] {$time$};\n"
        s = s + "\\foreach \\r in {0, 5,...,"+str(width*10)+"}\n"
        s = s + "   \draw (\\r*0.1,-1) node[inner sep=1pt,below=5pt,rectangle,fill=white] {$\\r$};\n"
        printMessage("tikz width "+str(width*10),0,False)
        return s
    def outputGANTT(results):
        s = "$(function() {\n 'use strict';\n $('.gantt').gantt({\n source: ["
        index = 0
        msperh = 60*60*1000
        start = 1444741853908
        for i in results.machines:
            if index > 0:
                s = s + ","
            s = s + "{\n name: 'machine "+str(i['index'])+"',\n desc: '"+str(len(i['ptimes']))+" tasks',\n values: [\n"

            nextstart = start
            tindex = 0
            for j in i['tasks']:
                nextstart = nextstart + i['setups'][tindex]*msperh
                if tindex > 0:
                    s = s + ","
                label = str(j-1)    
                if j == n+1:
                    label = "recharge"
                s = s + "{\n from: '/Date("+str(nextstart)+")/',\n"
                s = s + "to: '/Date("+str(nextstart+(i['ptimes'][tindex]*msperh))+")/',\n"
                s = s + "label: 'task "+label+"',\n"
                s = s + "customClass: 'ganttBlue'\n"
                s = s + "}"
                nextstart = nextstart + i['ptimes'][tindex]*msperh
                tindex = tindex + 1
                
            s = s + "]\n }\n"
            index = index + 1
        s = s + "], \n navigate: 'scroll',\n scale: 'weeks',\n maxScale: 'months',\n"
        s = s + "minScale: 'hours',\n itemsPerPage: 10,\n useCookie: true,\n onItemClick: function(data) {\n"
        s = s + "alert('Item clicked - show some details');\n },\n onAddClick: function(dt, rowId) {\n"
        s = s + "alert('Empty space clicked - add an item!');\n },\n onRender: function() {\n"
        s = s + "if (window.console && typeof console.log === 'function') {\n console.log('chart rendered');\n"
        s = s + "}\n }\n });\n $('.gantt').popover({\n selector: '.bar',\n title: 'Im a popover',\n content: 'And Im the content of said popover.',\n"
        s = s + "trigger: 'hover'\n });\n prettyPrint();\n });"
        

        return s
    def mycallback(model, where):
        #https://groups.google.com/forum/#!topic/gurobi/KKbO2Bv0EL0
        #save incumbents

        if where == GRB.callback.MIPSOL:
            obj = model.cbGet(GRB.callback.MIPSOL_OBJ)
            utime = model.cbGet(GRB.callback.RUNTIME)
            stats.ub.append((obj,utime))
            #print stats.ub
        if where == GRB.callback.PRESOLVE:
            #presolve removal of rows/cols
            stats.rows = model.numConstrs - model.cbGet(GRB.callback.PRE_ROWDEL)
            stats.cols = model.numVars- model.cbGet(GRB.callback.PRE_COLDEL)
    #prior to presolve
    model.update()
    stats = MIPStats(instance.n,instance.m)
    stats.pre_rows = model.numConstrs
    stats.pre_cols = model.numVars
    # Solve
    model.optimize(mycallback)
    #print printSolution(outputSolution(),instance)
    if len(inp) > 2:
        outs = outputSolution()
        if outs:
            print outs.output()
            print outs.machines
            s = []#outputGANTT(outs)
            tikz = outputGANTTTikz(outs)
            return [outputStats(inp[2]),s,tikz ]
        else:
            return False
    else:
        outs = outputSolution()
        s = []#outputGANTT(outs)
        tikz = outputGANTTTikz(outs)
        return [outs,s,tikz]

def printSolution(results,n):
    s = ""
    for i in results.machines:
        index = i['index']
        s = s + "\n"+str(index)
        rplan = i['tasks']
        splan = i['setups']
        ptimes = i['ptimes']
        cons = i['cons']
        prod = i['prod']
        starttime = 0
        for k in range(len(rplan)):
            j = rplan[k]
            if j > n:
                #rechargeevent
                setup = splan[k]
                ptime = ptimes[k]
                c = cons[k]
                p = prod[k]
                starttime = starttime + setup  + ptime
                s = s + ", [i:"+str(-j)+" C:"+str(starttime)+" c:"+str(c)+" p:"+str(p)+"]"
            elif j == 0:
                continue
            else:
                ptime = ptimes[k]
                setup = splan[k]
                c = cons[k]
                p = prod[k]
                starttime = starttime + setup  + ptime
                s = s + ", [i:"+str(j-1)+" C:"+str(starttime)+" c:"+str(c)+" p:"+str(p)+"]"
    return s



#0 out 1... in
def runModelFile(*out):
    import finstance as finst
    
    addTableHeadToFile(out[0], 14,"Table showing the results from running"+
                       " the MIP model on instances of varying size.","mipperf")
    appendToFile(out[0],"\multirow{2}{*}{Vehicles} & \multirow{2}{*}{Tasks} & "+
                 "\multicolumn{2}{c}{\# Vars} & \multicolumn{2}{c}{\# Constr.} "+
                 "& \multirow{2}{*}{First UB} & \multirow{2}{*}{Time First UB} & \multirow{2}{*}{UB}"+
                 " & \multirow{2}{*}{LB} & \multirow{2}{*}{Gap} & \multirow{2}{*}{Nodes}"+
                 " & \multirow{2}{*}{Time}\\\ "+
                 " &  & presol & after & presol & after &  &  &  &  &  &  & \\\ \hline \n")
    files = out[1]
    timelimit = out[2]
    output = out[0]
    for fname in files:
            print fname
            contents = finst.parseInstance(fname)
            res,gantt,tikz = runModel(contents,timelimit,output,1,False )
            #overwriteFile(os.path.join("js","tgantt_"+fname),gantt)
            #tikz stuff
            addTikzHeader(os.path.join("tikz","ntikzgantt_{}".format(out[0][:-4])+fname),
                          "Plan for instance with "+str(res.m)+" vehicles and "+str(res.n)+" tasks",
                          "plan"+str(res.m)+"v"+str(res.n)+"t")
            appendToFile(os.path.join("tikz","ntikzgantt_{}".format(out[0][:-4])+fname),tikz)
            addTikzFooter(os.path.join("tikz","ntikzgantt_{}".format(out[0][:-4])+fname))
    
    addTableFootToFile(out[0])
    
    #print printSolution(res,contents)
    #print res.upper
    #print res.lower

#outputted up until and including 20_5
'''
filelist = ['randinst_5_10_fixed.txt','randinst_10_5_fixed.txt',
            'randinst_20_5_fixed.txt','randinst_25_5_fixed.txt','randinst_25_10_fixed.txt']

for i in range(5,26,5):
    for j in range(5,11,5):
        filelist.append("randinst_"+str(i)+"_"+str(j)+".txt")

#filelist = ['randinst_10_5.txt']
runModelFile("mipperfgantttikz.txt",filelist)
'''

filelist = ['randinst_10_5_fixedtest.txt','randinst_15_5test.txt',
            'randinst_20_5_fixedtest.txt','randinst_25_5_fixedtest.txt','randinst_25_10_fixedtest.txt']
filelist = filelist + ['randinst_15_10test.txt','randinst_20_10test.txt']
#filelist = ['inph.txt']
runModelFile("miptestcontfix60mt.txt",filelist,20)

filelist = ['randinst_10_5test.txt','randinst_15_5test.txt',
            'randinst_20_5test.txt','randinst_25_5test.txt','randinst_25_10test.txt']
filelist = filelist + ['randinst_15_10test.txt','randinst_20_10test.txt']
#filelist = ['inph.txt']
runModelFile("miptestcont60mt.txt",filelist,20)
