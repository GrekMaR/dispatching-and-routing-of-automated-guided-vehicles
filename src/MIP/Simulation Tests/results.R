#!/usr/bin/Rscript
## Get them args :)
#args <- commandArgs(trailingOnly = TRUE)

waitPlot <- function(pyout,gout){
  
  args = c(pyout,gout)
  #read in results for mip 
  resultsp <- read.csv(file=args[1],header=TRUE, sep=" ")
  #read in results for greedy
  resultsg <- read.csv(file=args[2],header=TRUE, sep=" ")
  
  library(ggplot2)
  library(cowplot)
  
  svg(filename="pygreedyplotwait.svg")
  p1 <-ggplot(data = resultsp, aes(x=task, y=wait) )+ 
    geom_bar(stat="identity", width=1)+
    xlab("Tasks") + ylab("Waiting Time")+
    scale_x_continuous(breaks = seq(0, 1500, 100))+
    scale_y_continuous(breaks = seq(0,50,2))+
    ggtitle("Waiting Time for All Tasks(MIP).")
  #print(p)
  #dev.off()
  
  p2 <-ggplot(data = resultsg, aes(x=task, y=wait) )+ 
    geom_bar(stat="identity", width=1)+
    xlab("Tasks") + ylab("Waiting Time")+
    scale_x_continuous(breaks = seq(0, 1500, 100))+
    scale_y_continuous(breaks = seq(0,50,2))+
    ggtitle("Waiting Time for All Tasks(GREEDY).")
  plot_grid(p1, p2, labels = "AUTO", ncol = 1, align = 'v')
  dev.off()
}

resultPlot <-function(pyout,gout){
  #Header Current
  # nrtasks makespan solvetime timestep
  
  #Header OLD
  #index entry start completion solvetime assignedTo makespan nrvehicles 
  #iterations 
  args = c(pyout,gout)
  
  #read in results for mip 
  resultsp <- read.csv(file=args[1],header=TRUE, sep=" ")
  #read in results for greedy
  resultsg <- read.csv(file=args[2],header=TRUE, sep=" ")
  
  # Plot the results!
  library(ggplot2)
  svg(filename="pygreedyplotg.svg")
  p1 <-ggplot(data = resultsp, aes(x=timestep, y=nrtasks) )+ 
    geom_bar(stat="identity", width=1)+
    xlab("Timestep") + ylab("#Tasks")+
    scale_x_continuous(breaks = seq(0, 1500, 100))+
    scale_y_continuous(breaks = seq(0,50,5))+
    ggtitle("Number of Tasks per Iteration(MIP) with Garbage Tasks.")
  
  #GREEDY
  p2 <-ggplot(data = resultsg, aes(x=timestep, y=nrtasks) )+ 
    geom_bar(stat="identity", width=1)+
    xlab("Timestep") + ylab("#Tasks")+
    scale_x_continuous(breaks = seq(0, 1500, 100))+
    scale_y_continuous(breaks = seq(0,50,5))+
    ggtitle("Number of Tasks per Iteration(GREEDY) with Garbage Tasks")
  pp <-plot_grid(p1, p2, labels = "AUTO", ncol = 1, align = 'v')
  print(pp)
  dev.off()
  #MIP
  #No starting
  svg(filename="pygreedyplotnog.svg")
  p1 <-ggplot(data = resultsp[-1,], aes(x=timestep, y=nrtasks) )+ 
    geom_bar(stat="identity", width=1)+
    xlab("Timestep") + ylab("#Tasks")+
    scale_x_continuous(breaks = seq(0, 1500, 50))+
    scale_y_continuous(breaks = seq(0,50,1))+
    ggtitle("Number of Tasks per Iteration(MIP)")
  #GREEDY
  #No starting
  p2 <-ggplot(data = resultsg[-1,], aes(x=timestep, y=nrtasks) )+ 
    geom_bar(stat="identity", width=1)+
    xlab("Timestep") + ylab("#Tasks")+
    scale_x_continuous(breaks = seq(0, 1500, 50))+
    scale_y_continuous(breaks = seq(0,50,1))+
    ggtitle("Number of Tasks per Iteration(GREEDY)")
  pp <- plot_grid(p1, p2, labels = "AUTO", ncol = 1, align = 'v')
  print(pp)
  dev.off()
  #mean tasks pr iteration ALL
  iterationsp <- resultsp[["nrtasks"]]
  resultsp.nrtaskmeang <- mean(iterationsp)
  print(resultsp.nrtaskmeang)
  #mean without first
  iterationsp <- iterationsp[2:length(iterationsp)]
  resultsp.nrtaskmean <- mean(iterationsp)
  print(resultsp.nrtaskmean)
  
  #mean tasks pr vehicle in iteration
  #taskvehicle <- resultsp.frame(table(resultsp$assignedTo,resultsp$timestep))
  
  #resultsp.nrtaskvehiclemean <- mean(count(taskvehicle$nrtasks))
  
  #Get entry for a task
  #Subset to each task, then get first row
  resultsp.entryrows <- resultsp[!duplicated(resultsp$index)]
  #get completion rows 
  resultsp.completiontasks <-resultsp[ !duplicated(resultsp$index,fromLast=TRUE),]
  
  #mean tasks pr iteration ALL
  iterationsg <- resultsg[["nrtasks"]]
  resultsg.nrtaskmeang <- mean(iterationsg)
  print(resultsg.nrtaskmeang)
  #mean without first
  iterationsg <- iterationsg[2:length(iterationsg)]
  resultsg.nrtaskmean <- mean(iterationsg)
  print(resultsg.nrtaskmean)
  
}