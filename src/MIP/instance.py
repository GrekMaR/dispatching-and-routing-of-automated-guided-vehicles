#rcpsp instans generator
import os.path, random, sys, math
import matplotlib.pyplot as plt
from helperclasses import *
import heapq
from MessageHandler import *

#Classes



   

'''origin
http://stackoverflow.com/questions/11551049/matplotlib-plot-zooming-with-scroll-wheel
'''
def zoom_factory(ax,base_scale = 2.):
    
    def zoom_fun(event):
        # get the current x and y limits
        cur_xlim = ax.get_xlim()
        cur_ylim = ax.get_ylim()
        cur_xrange = (cur_xlim[1] - cur_xlim[0])*.5
        cur_yrange = (cur_ylim[1] - cur_ylim[0])*.5
        xdata = event.xdata # get event x location
        ydata = event.ydata # get event y location
        if event.button == 'up':
            # deal with zoom in
            scale_factor = 1/base_scale
        elif event.button == 'down':
            # deal with zoom out
            scale_factor = base_scale
        else:
            # deal with something that should never happen
            scale_factor = 1
            print event.button
        # set new limits
        ax.set_xlim([xdata - cur_xrange*scale_factor,
                     xdata + cur_xrange*scale_factor])
        ax.set_ylim([ydata - cur_yrange*scale_factor,
                     ydata + cur_yrange*scale_factor])
        plt.draw() # force re-draw

    fig = ax.get_figure() # get the figure of interest
    # attach the call back
    fig.canvas.mpl_connect('scroll_event',zoom_fun)

    #return the function
    return zoom_fun


def createGraph(points):
    print "Saving path..."
    graph = []
    limit = 10
    print len(points)
    for i in range(len(points)):
        print "it"+str(i)
        #add immediate neighbour
        p = points[i]
        if i < len(points)-1:
            p.neighbours.add(points[i+1])
            points[i+1].neighbours.add(p)
        else:
            p.neighbours.add(points[0])
            points[0].neighbours.add(p)

    for i in range(len(points)):
        #Merge points
        p = points[i]
        if p.sol:
            continue
        j = i+1
        if i == len(points)-1:
            #If we have the last point it will most likely join with the first one
            j = 0
        while True and j < len(points)-1:
            if p.dist(points[j]) < limit:
                print "solution "+p.toString()+" matcho to"+points[j].toString()
                points[j].sol = True
                p.neighbours = p.neighbours | points[j].neighbours
                for k in p.neighbours:
                    if points[j] in k.neighbours:
                        k.neighbours.remove(points[j])
                    k.neighbours.add(p)
                    print "new neighbours"
                    for neighbour in k.neighbours:
                        print neighbour.toString()
            j = j +1
    j = 0
    for i in points:
        if not i.sol:
            i.index = j
            graph.append(i)
            j = j +1 
    '''
    for i in graph:
        print i.toString()
    print len(graph)
    '''
        
    return graph

def saveGraph(graph,fname):
    s = str(len(graph))
    for i in graph:
        s = s+"\n("+str(i.x)+","+str(i.y)+")"
        for j in i.neighbours:
            s = s + " "+str(j.index)
    
    return writeToFile(fname,s)

def parseGraph(content):
    print "Parsing graph..."
    graph = []
    tempneighbours = []
    for i in range(1,len(content)):
        line = content[i].split(" ")
        #print line
        l = line[0].split(",")
        lx = int(round(float(l[0].replace("(",""))))
        ly = int(round(float(l[1].replace(")",""))))
        p = Point(lx,ly)
        #print p.toString()
        #p.index = i-1
        tempneighbours.append([])
        for j in line[1:]:
            tempneighbours[-1].append(int(j))
        graph.append(p)
    for i in range(len(graph)):
        graph[i].index = i
        for j in tempneighbours[i]:
            graph[i].neighbours.add(graph[j])
    print "Done parsing..."
    return graph
            
    
def generateSimpleInstance(nrtasks,nrmachines):
    
    ptimes = []
    types = []
    starttimes = []
    duedates = []
    consumption = []
    pfortype = [random.randint(10,30),random.randint(10,30),random.randint(10,30)]
    ab = random.randint(10,30)
    ac = random.randint(10,30)
    bc = random.randint(10,30)
    bd = random.randint(10,30)
    cd = random.randint(10,30)
    #l,g,m,dummy
    '''
        Assume linen goes from depot to dest
        garbage from start to dest
        transport from start to dest
    '''
    sfortype = [[pfortype[0],ab,ac,pfortype[0]],[ab,pfortype[1],bc,bd],
                [ac,bc,pfortype[2],cd],[0,bd,cd,0]]
    maxtime = 24*60
    #a to a, a to b and b to b setups
    ab = random.randint(10,30)
    sfortype = [[pfortype[0],ab],[pfortype[1],ab]]
    #for 0
    ptimes.append(0)
    consumption.append(0)
    starttimes.append(0)
    duedates.append(maxtime)
    for i in range(nrtasks):
        #Choose type of task
        types.append(random.randint(0,1))
        #get processing time
        ptimes.append(pfortype[types[-1]])
        #get consumption
        consumption.append(getConsumption(ptimes[-1]))
        #get start time for task
        if random.randint(0,1) <0:
            starttimes.append(random.randint(0,10))
        else:
            starttimes.append(0)
        #get duedate
        if random.randint(0,1)>=0:
            duedates.append(maxtime)
        else:
            duedates.append(maxtime -(ptimes[-1]+sfortype[types[-1]][1]+random.randint(maxtime/4,maxtime/2)))
    #n+1
    ptimes.append(0)
    consumption.append(0)
    starttimes.append(0)
    duedates.append(maxtime)
    
    stimes = []
    for i in range(nrtasks+2):
        temp = []
        for j in range(nrtasks+2):
            if i == j:
                temp.append(0)
            elif i == 0 and j == nrtasks+1 or j == 0 and i == nrtasks+1:
                temp.append(0)
            elif (i == 0 or i == nrtasks+1) and (types[j-1]==0 and j != nrtasks+1):
                temp.append(0)
            elif i == 0 or i == nrtasks+1:
                temp.append(sfortype[types[3]][types[j-1]])
            elif j == 0 or j == nrtasks+1:
                temp.append(sfortype[types[i-1]][3])
            else:
                temp.append(sfortype[types[i-1]][types[j-1]])
        stimes.append(temp)
            
    blevels = []
    fullb = getFullb()
    minb = getMinb()
    charget = getCharget()
    chargea = getChargea()
    for j in range(nrmachines):
        blevels.append(fullb)
    inst = Instance(ptimes,stimes,starttimes,duedates,
                    blevels,consumption,fullb,minb,charget, chargea)
    return inst
    
    
def readGraph(fname):
    if not os.path.isfile(fname):
        print "File '"+fname+"' does not exist!"
        return False
    try:
        print "Reading graph..."
        with open(fname) as f:
            content = f.readlines()
        graph = parseGraph(content)
        f.close()
        return graph
    except IOError:
        print "Something went wrong opening the file..."
        sys.exit(0)

#give path to map and the output name for produced graph

def createMapping(mapfile, fname):
    if not os.path.isfile(mapfile):
        print "File '"+mapfile+"' does not exist!"
        return False
    if os.path.isfile(fname):
        print "File '"+fname+"' already exists!"
        return False
    im = plt.imread(mapfile)
    ax = plt.gca()
    fig = plt.gcf()
    scale = 1.5
    f=zoom_factory(ax,base_scale=1.5)
    implot = ax.imshow(im)
    points = []
    x = []
    y = []
    
    def onclick(event):
        if event.xdata != None and event.ydata != None:
            rx = int(round(event.xdata))
            ry = int(round(event.ydata))
            points.append(Point(rx, ry))
            print str(rx)+","+str(ry)
            plt.plot(rx, ry,marker='o',color='green')
            plt.draw()
            y.append(ry)
            x.append(rx)
            
    

    #TODO: need to fix old window, as it is not properly destroyed
    def onclose(event):
        print "---"
        '''im = plt.imread(mapfile)
        ax = plt.gca()
        fig = plt.gcf()
        implot = ax.imshow(im)
        plt.plot(x,y,ls='--',lw=1,marker='o',color='green')
        plt.show()'''
        #Save path
        plt.close()
        print "creating"
        #g = createGraph(points)
        print "muh"
        #saveGraph(g, fname)
        print "closing"
    clid = fig.canvas.mpl_connect('close_event', onclose)
    cid = fig.canvas.mpl_connect('button_press_event', onclick)
    

    plt.show()
    print "creating"
    g = createGraph(points)
    print "muh"
    saveGraph(g, fname)
    return


def shortestPath(graph,src):
    #Dijkstra
    d = {src:0} 
    prev = {src:None}
    Q = []
    for i in graph:
        if i != src:
            d[i] = 1000
            prev[i] = None
        Q.append(i)
    while len(Q)>0:
        u = min(Q,key=lambda x: d[x])
        Q.remove(u)
        for i in u.neighbours:
            alt = d[u]+u.dist(i)
            if alt < d[i]:
                d[i]=alt
                prev[i] = u
    return prev, d
        
def buildTemplate(tasks,machines,battery,graph):
    #Takes picked stations and links them to actual points
    #Also generates processing times, consumption and setup.
    limit = 20
    for i in tasks:
        for j in graph:
            if j.dist(i)<limit:
                i.pathpoint = j
                print "found "+str(j.index)
                break
        for j in graph:
            if j.dist(i.dest)<limit:
                i.dest = j
                break

    for i in machines:
        for j in graph:
            if j.dist(i)<limit:
                i.pathpoint = j
                break
    for j in graph:
        if j.dist(battery)<limit:
            battery.pathpoint = j
            break
    for i in tasks:
        prev, d = shortestPath(graph,i.pathpoint)
        i.ptime = int(round(d[i.dest]))

    s = "-t\n"
    index = 0
    for i in tasks:
        #index taskkind origin dest ptime
        s = s + str(index)+" "+str(i.taskkind)+" "+str(i.pathpoint.index)+" "+str(i.dest.index)+" "+str(i.ptime)+"\n"
        index = index+1
    s = s + "-m\n"
    index = 0
    for i in machines:
        #graph node ptime
        s = s + str(index)+" "+str(i.pathpoint.index)+"\n"
        index = index+1
    s = s+"-b\n"
    s = s + str(index)+" "+str(battery.pathpoint.index)+"\n"
    #Write to file then read in later for automatic generation of tasks
    return s



    
        
def updateOverview(taskoverview,results, tasks):
    print "len results "+str(len(results.machines))
    for i in results.machines:
        index = i['index']
        machine = taskoverview.machines[index]
        rplan = i['tasks']
        splan = i['setups']
        ptimes = i['ptimes']
        starts = i['starts']
        starttime = taskoverview.currenttime
        #remove all
        #print "Before CLEAN: \n{}\n ".format(machine.toString())
        machine.clean(starttime)
        for k in range(len(rplan)):
            starttime = taskoverview.currenttime
            j = rplan[k]
            if j > len(tasks):
                #rechargeevent
                setup = splan[k]
                ptime = ptimes[k]
                starttime += starts[k]
                rtask = createRechargeTask(starttime,ptime,setup,taskoverview)
                rtask.assignedTo = machine
                rtask.waiting = int(starttime-rtask.start )
                rtask.index = taskoverview.getCIndex()
                taskoverview.ctasks.append(rtask)
                machine.tasks.append(rtask)
                starttime +=rtask.ptime
            elif j == 0:
                continue
            else:
                
                t = tasks[j-1]
                t.setup = splan[k]
                starttime += starts[k]
                t.waiting = int(starttime - t.start) #wait till it is processed
                t.started = starttime  #the time the machine starts processing
                #print "index:{},setup:{},ptime:{},entry:{},started:{}".format(t.index,t.setup, t.ptime,t.entry,t.started)
                starttime +=t.ptime
                t.assignedTo = machine
                machine.tasks.append(t)
            #print starttime
            
        #print machine.toString()


def preprocessRandomInstance(tasks, taskoverview, sim):
    #print "Preprocessing..."
    
    #generates processing times, consumption and setup.
    #Also updates vehicle position for the current job
    #they are doing, or have finished
    machines = taskoverview.machines
    battery = taskoverview.stations[0]
    fullb = taskoverview.fullb
    minb = taskoverview.minb
    chargea = taskoverview.chargea
    charget = taskoverview.charget
    time = taskoverview.currenttime
    types ={'l':0,'g':1,'m':2 }
    mstarttimes = []
    #machine stuff
    blevels = []
    print "preprocessing"
    for i in machines:
        blevel = i.getBatteryLevel(taskoverview.currenttime)
        blevels.append(blevel)
        #print i.toString()
        i.batterylevel = blevel
        mtask, tindex = i.currentTask(taskoverview.currenttime)
        if mtask:
            doneat = int(mtask.started + mtask.ptime - taskoverview.currenttime)
            if doneat < 0:
                doneat = 0
            mstarttimes.append(doneat)
        else:
            mstarttimes.append(0)
        #printMessage(i.toString(), taskoverview.currenttime)
    modes = i.modes
    ptimes = [0]
    for i in tasks:
        ptimes.append(i.ptime)
    ptimes.append(0)

    stimes = []
    ztemp =[0]
    #Should be two when getting machines into the mix
    
    #assume central position
    #going from the point we are to the origin of the next task
    for j in tasks:
        temp = [taskoverview.stimes[j.dest][0]]
        for k in tasks:
            if j.index == k.index:
                temp.append(0)
                continue
            temp.append(taskoverview.stimes[j.dest][k.origin])
        temp.append(taskoverview.stimes[j.dest][0])
        #depot setups is ztemp
        ztemp.append(taskoverview.stimes[0][j.origin])
        stimes.append(temp)
    ztemp.append(0)
    stimes.insert(0,ztemp)
    stimes.append(ztemp)
        
    #Also shift start/end times to accommodate current time
    starttimes = [0]
    duedates = [24*60-taskoverview.currenttime]
    consumption = [0]
    for i in tasks:
        consumption.append(i.consumption)
        if sim:
            #print i.toString()
            #print "cur time "+str(taskoverview.currenttime)
            #print "start for i "+str(i.start)
            if i.start < taskoverview.currenttime:
                starttimes.append(0)
            else:
                starttimes.append(int(taskoverview.currenttime-i.start))
        else:
            starttimes.append(0)
        if i.end > taskoverview.currenttime:
            duedates.append(int(abs(i.end-taskoverview.currenttime)))
        else:
            duedates.append(int(abs(taskoverview.currenttime-i.end)))
    consumption.append(0)
    starttimes.append(0)
    duedates.append(24*60-taskoverview.currenttime)



    #need chargea, charget, full, min, levels and duedates
    instance = Instance(ptimes,stimes,starttimes,duedates,blevels,consumption,fullb,minb,charget, chargea,modes)
    instance.mstarttimes = mstarttimes
    if taskoverview.currenttime >= 2000:
        printMessage(instance.instanceToString(), taskoverview.currenttime,False)
    return instance 
    
    
def preprocessInstance(tasks, taskoverview, sim):
    #print "Preprocessing..."
    
    #generates processing times, consumption and setup.
    #Also updates vehicle position for the current job
    #they are doing, or have finished
    machines = taskoverview.machines
    graph = taskoverview.graph
    battery = taskoverview.stations[0]
    timeprdist = 0.005
    fullb = taskoverview.fullb
    minb = taskoverview.minb
    chargea = taskoverview.chargea
    charget = taskoverview.charget
    time = taskoverview.currenttime
    types ={'l':0,'g':1,'m':2 }
    #machine stuff
    blevels = []
    
    for i in machines:
        blevels.append(i.batterylevel)
        t, ind = i.currentTask(time)
        printMessage(i.toString())
        if t:
            if t.started + t.setup + t.ptime > time:
                #task is finished
                i.pos = t.dest
            elif t.started + t.setup < time:
                #task is being processed
                i.pos = t.origin
            elif ind > 0 and t.started + t.setup > time:
                #else, assume we are on our from last task and compare time diff
                prev = i.tasks[ind-1]
                prevfinish = prev.started + prev.setup + prev.ptime
                distp = time - prevfinish
                distt = time - (t.started + setup)
                if distp < distt:
                    i.pos = prev.dest
                else:
                    i.pos = t.origin
            
    modes = i.modes        
        #Also add positioning in later her based on current task

    
    ptimes = [0]
    for i in tasks:
        '''
        prev, d = shortestPath(graph,i.origin)
        ptimes.append(int(round(d[i.dest]*timeprdist)))
        '''
        ptimes.append(i.ptime)
    ptimes.append(0)
    
    #Determine if central position or field, then use
    #shortest path appropriately
    # if machine position is in depot, then continue from last dest
    #otherwise field
    # also use n+1 as starting pos instead of 0

    
    '''
    for i in machines:
        for j in tasks:
            for k in tasks:
                if i.pathnode == battery.pathnode:
                    prev, d = shortestPath(graph,j[1].pathpoint)
                    stimes.append(d[k[0].pathpoint])
    '''
    stimes = []
    ztemp =[0]
    #Should be two when getting machines into the mix
    
    #assume central position
    for j in tasks:
        temp = []
        #battery to origin of task
        prev, ddummy = shortestPath(graph,j.origin)
        temp.append(int(round(ddummy[battery.pos]*timeprdist)))
        #dest to battery
        prev, dz = shortestPath(graph,j.dest)
        ztemp.append(int(round(dz[battery.pos]*timeprdist)))
        for k in tasks:
            if j.index == k.index:
                temp.append(0)
                continue
            prev, d = shortestPath(graph,j.dest)
            temp.append(int(round(d[k.origin]*timeprdist)))
        temp.append(int(round(ddummy[battery.pos]*timeprdist)))
        stimes.append(temp)
    ztemp.append(0)
    stimes.insert(0,ztemp)
    stimes.append(ztemp)
        
    #Also shift start/end times to accommodate current time
    starttimes = [0]
    duedates = [24*60]
    for i in tasks:
        if sim:
            starttimes.append(int(taskoverview.currenttime-i.start))
        else:
            starttimes.append(0)
        if i.end > taskoverview.currenttime:
            duedates.append(int(abs(i.end-taskoverview.currenttime)))
        else:
            duedates.append(int(abs(taskoverview.currenttime-i.end)))
    starttimes.append(0)
    duedates.append(24*60)



    #Dummy tasks already taken care of at ptimes
    consumption = []
    cost = 1.55
    for i in ptimes:
        consumption.append(int(round(i*cost)))

    #need chargea, charget, full, min, levels and duedates
    instance = Instance(ptimes,stimes,starttimes,duedates,blevels,consumption,fullb,minb,charget, chargea,modes)
    instance.stationlimit = taskoverview.stationlimits
    #print instance.instanceToString()
    return instance 



class Placer():
    def __init__(self):
        self.origdest = []
        self.tasks = []
        self.machines = []
        self.battery = None
        self.mode = 0
        self.taskkind = "l"
        self.nodeColor = "midnightblue"
    def placeTasksMachines(self,graph,mapfile):
        im = plt.imread(mapfile)
        ax = plt.gca()
        fig = plt.gcf()
        scale = 1.5
        f=zoom_factory(ax,base_scale=1.5)
        implot = ax.imshow(im)
        print "linen task plotting"
        x = []
        y = []
        for i in graph:
            plt.plot(i.x, i.y,marker='o',color='green')

        def onclick(event):
            if event.xdata != None and event.ydata != None:
                #depending on mode, append to appropriate array
                rx = int(round(event.xdata))
                ry = int(round(event.ydata))
                if self.mode == 0:
                    
                    n = Node(rx, ry,self.mode)
                    self.origdest.append(n)
                    n.taskkind = self.taskkind
                    if len(self.origdest)==2:
                        plt.plot([self.origdest[0].x,self.origdest[1].x],
                                 [self.origdest[0].y,self.origdest[1].y],ls='--',
                                 marker='o',color=self.nodeColor)
                        n = Node(self.origdest[0].x, self.origdest[0].y,self.mode)
                        n.dest = self.origdest[1]
                        n.taskkind = self.taskkind
                        self.tasks.append(n)
                        self.origdest = []
                        
                    plt.plot(rx, ry,marker='o',color=self.nodeColor)
                elif self.mode == 1:
                    self.machines.append(Node(rx, ry,self.mode))
                    plt.plot(rx, ry,marker='o',color=self.nodeColor)
                elif self.mode == 2:
                    self.battery = Node(rx, ry,self.mode)
                    plt.plot(rx, ry,marker='o',color=self.nodeColor)
                print str(rx)+","+str(ry)+"  mode: "+str(self.mode)+"taskkind: "+str(self.taskkind)
                
                plt.draw()
        cid = fig.canvas.mpl_connect('button_press_event', onclick)
        def onpress(event):#b v l g m
            if event.key =='b':
                print "battery plotting"
                self.nodeColor = "sienna"
                self.mode = 2
            elif event.key == 'v':
                print "vehicle plotting"
                self.nodeColor = "darkred"
                self.mode = 1
            elif event.key == 'l':
                print "linen task plotting"
                self.nodeColor = "midnightblue"
                self.mode = 0
                self.taskkind = "l"
            elif event.key == 'g':
                print "garbage task plotting"
                self.nodeColor = "darkslategray"
                self.mode = 0
                self.taskkind = "g"
            elif event.key == 'm':
                print "transport task plotting"
                self.nodeColor = "darkgoldenrod"
                self.mode = 0
                self.taskkind = "m"
        cid = fig.canvas.mpl_connect('key_press_event', onpress)
        
        def onclose(event):
            print "---"
            plt.close()
            
        clid = fig.canvas.mpl_connect('close_event', onclose) 
        
        plt.show()
        b = buildTemplate(self.tasks,self.machines,self.battery,graph)
        return b 

def parseTemplate(graph,content):
    print "Parsing template..."
    line = content.line
    tokens = line.split(" ")
    machines = []
    garbage = []
    linen = []
    tasks = []
    misc = []
    battery = []
    #task type origin dest ptime
    if '-t' in tokens[0] :
        line = content.nextLine()
        tokens = line.split(" ")
        while '-m' not in tokens[0]:
            indexorigin = int(tokens[2])
            o = graph[indexorigin]
            indexdest = int(tokens[3])
            d = graph[indexdest]
            n = Node(o.x, o.y,0)
            n.pathnode = o
            n.dest = d
            n.ptime = int(tokens[4])
            if "g" in tokens[1]:
                garbage.append(n)
                n.taskkind = "g"
                tasks.append(n)
            elif "l" in tokens[1]:
                linen.append(n)
                n.taskkind = "l"
                tasks.append(n)
            else:
                misc.append(n)
                n.taskkind = "m"
                tasks.append(n)
            line = content.nextLine()
            tokens = line.split(" ")
    #machine pos
    if "-m" in tokens[0]:
        line = content.nextLine()
        tokens = line.split(" ")
        while "-b" not in tokens[0]:
            index = int(tokens[1])
            node = graph[index]
            n = Node(node.x, node.y,1)
            n.pathnode = node
            machines.append(n)
            line = content.nextLine()
            tokens = line.split(" ")
    if "-b" in tokens[0]:
        line = content.nextLine()
        tokens = line.split(" ")
        while True:
            index = int(tokens[1])
            node = graph[index]
            n = Node(node.x, node.y,2)
            n.pathnode = node
            battery.append(n)
            line = content.nextLine()
            if line:
                tokens = line.split(" ")
            else:
                break
    template = Template(linen, garbage,misc,machines,battery,graph,tasks)     
        
        

    return template
    
def readInstanceTemplate(graph, instancefile):
    if not os.path.isfile(instancefile):
        print "File '"+instancefile+"' does not exist!"
        return
    try:
        with open(instancefile) as f:
            content = f.readlines()
        template = parseTemplate(graph,Content(content))
        f.close()
        return template
    except IOError:
        print "Something went wrong opening the file..."
        sys.exit(0)


    return False

def writeTemplateInstance(instance):
    #Write instance generated from template to a string
    s = "-t index kind start end origin dest ptime entry\n"
    for i in instance.tasks:
        s = s + str(i.index)+" "+i.kind+" "+str(i.start)+" "+str(i.end)
        s = s + " "+str(i.origin.index)+" "+str(i.dest.index)+" "+str(i.ptime)
        s = s+" "+str(i.entry)+"\n"
    s = s + "-m index pos blevel\n"
    for i in instance.machines:
        s = s + str(i.index)+" "+str(i.pos.index)+" "+str(i.batterylevel)+"\n"
    s = s + "-s index pos\n"
    i = instance.battery
    s = s + str(i.index)+" "+str(i.pos.index)
    return s

def writeRandomInstance(instance):
    #Write instance generated randomly to a string
    s = ""
    tmp = ""
    for i in instance.stimes:
        for j in i:
            s = s + tmp+str(j)
            tmp = " "
        s = s + "\n"
        tmp = ""
    s = s + "-t index kind start end origin dest ptime entry\n"
    for i in instance.tasks:
        s = s + str(i.index)+" "+i.kind+" "+str(i.start)+" "+str(i.end)
        s = s + " "+str(i.origin)+" "+str(i.dest)+" "+str(i.ptime)
        s = s+" "+str(i.entry)+"\n"
    s = s + "-m index pos blevel\n"
    for i in instance.machines:
        s = s + str(i.index)+" "+str(i.pos)+" "+str(i.batterylevel)+"\n"
    s = s + "-s index pos\n"
    i = instance.battery
    s = s + str(i.index)+" "+str(i.pos)
    return s


def createPlanFromRandom(nrTasks,nrMachines):
    machines = []
    batterylevel = 500
    tasks = []
    garbage = []
    types = ['l','g','m']
    nrgarbage = int(math.ceil(nrTasks*(1/2.3)))
    nrlinen = nrgarbage
    nrmisc = nrTasks- nrgarbage - nrlinen
    order = ([0]*nrlinen)+([2]*nrmisc)
    #Shuffle order with no regard for time limits
    random.shuffle(order)
    order = ([1]*nrgarbage) + order
    
    garbageptimes = [random.randint(10,30) for i in range(nrgarbage) ]
    linenptimes = [random.randint(10,30) for i in range(nrlinen) ]
    miscptimes = [random.randint(10,30) for i in range(nrmisc) ]
    pfortype = [garbageptimes,linenptimes,miscptimes]
    '''
    ab = random.randint(10,30)
    ac = random.randint(10,30)
    bc = random.randint(10,30)
    bd = random.randint(10,30)
    cd = random.randint(10,30)
    '''
    nrpos = 10
    possetups = []
    for i in range(nrpos):
        possetups.append([])
        for j in range(nrpos):
            if j < i:
                possetups[-1].append(possetups[j][i])
            elif j == i:
                possetups[-1].append(0)
            else:
                possetups[-1].append(random.randint(10,30))
    #l,g,m,dummy
    '''
        Assume linen goes from depot to dest
        garbage from start to dest
        transport from start to dest
    
    sfortype = [[pfortype[0],ab,ac,pfortype[0]],[ab,pfortype[1],bc,bd],
                [ac,bc,pfortype[2],cd],[0,bd,cd,0]]
    '''
    time = 16*60
    lambd = math.floor((time)/(nrTasks-nrgarbage))
    #Now fill out tasks
    prevtime = 8*60
    index = -1
    endtime = 24*60
    for i in order:
        index = index + 1
        if types[i]=='g':
            ptimeind = random.randint(0,len(pfortype[i])-1)
            orig = random.randint(0,nrpos-1)
            dest = random.randint(0,nrpos-1)
            while dest == orig:
                dest = random.randint(0,nrpos-1)
            tasks.append(Task(index, types[i], 0,time, orig,
                            dest,pfortype[i][ptimeind],0 ))
            
            continue
        ptimeind = random.randint(0,len(pfortype[i])-1)
        orig = random.randint(0,nrpos-1)
        if types[i]=='l':
            orig = 0
        dest = random.randint(0,nrpos-1)
        while dest == orig:
            dest = random.randint(0,nrpos-1)
        entry = int(round(random.expovariate(1/lambd)))
        prevtime = prevtime + entry
        tasks.append(Task(index, types[i], prevtime,endtime, orig,
                            dest,pfortype[i][ptimeind],prevtime ))
    for i in range(nrMachines):
        machines.append(Vehicle(i,0,batterylevel, batterylevel))

    station =Station(0,0)

    
    temp = Template([], [],[],machines,station,None,tasks)
    temp.stimes = possetups
    return temp  
        
    
def createFromTemplate(g,template,nrTasks,nrMachines):
#Randomly add machines and tasks based on template and preprocess using
    #Currently entry = start
    timeprdist = 0.005
    step = 1
    machines = []
    batterylevel = getFullb()
    tasks = []
    garbage = []
    
    nrgarbage = int(math.ceil(nrTasks*(1/2.3)))
    garbagetime = 8*60
    #get the rate of growth
    #gr = rgen(garbagetime,nrgarbage)
    glam = math.floor(garbagetime/nrgarbage)
    
    linen = []
    nrlinen = nrgarbage
    linentime = 22*60
    #lr = rgen(linentime,nrlinen)
    llam = math.floor((linentime-20)/nrlinen)
    
    misc = []
    misctime = linentime
    nrmisc = nrTasks- nrgarbage - nrlinen

    #mr = rgen(misctime,nrmisc)
    mlam = math.floor((misctime-20)/nrmisc)
    #do exponential distribution here
    prevtime = 0
    for i in range(nrgarbage):
        #randomly assign template
        gtemplate = template.garbage[random.randint(0,len(template.garbage)-1)]
        #create garbage task from template and get entry time for task
        entry = prevtime + int(round(random.expovariate(1/glam)))
        prevtime = entry
        ptime = int(round(gtemplate.ptime*timeprdist))
        garbage.append(Task(i, "g", entry,garbagetime, gtemplate.pathnode,
                            gtemplate.dest,ptime,entry ))
        tasks.append(garbage[-1])
    prevtime = 0
    for i in range(nrlinen):
        #randomly assign template
        ltemplate = template.linen[random.randint(0,len(template.linen)-1)]
        #get time
        #create garbage task from template and get entry time for task
        entry = prevtime + int(round(random.expovariate(1/llam)))
        prevtime = entry
        ptime = int(round(ltemplate.ptime*timeprdist))
        linen.append(Task(i, "l", entry, linentime, ltemplate.pathnode,
                            ltemplate.dest,ptime,entry ))
        tasks.append(linen[-1])
    prevtime = 0
    for i in range(nrmisc):
        #randomly assign template
        mtemplate = template.misc[random.randint(0,len(template.misc)-1)]
        #get time
        #create garbage task from template and get entry time for task
        ptime = int(round(mtemplate.ptime*timeprdist))
        entry = prevtime + int(round(random.expovariate(1/mlam)))
        prevtime = entry
        misc.append(Task(i, "m", entry, misctime, mtemplate.pathnode,
                            mtemplate.dest,ptime,entry ))
        tasks.append(misc[-1])
    tasks = sorted(tasks,key = lambda x: x.entry)
        
    for i in range(len(tasks)):
        t = tasks[i]
        t.index = i
        if t.entry  > t.end:
            t.end = t.entry + t.ptime
        elif t.entry + t.ptime > t.end:
            t.end = t.end + t.ptime
    #machines
    for i in range(nrMachines):
        #randomly assign template
        print len(template.machines)
        mtemplate = template.machines[random.randint(0,len(template.machines)-1)]
        #get time
        #create garbage task from template and get entry time for task
        machines.append(Vehicle(i,mtemplate.pathnode,batterylevel ))
    #station
     #randomly assign template
    stemplate = template.battery[0]
    #get time
    #create garbage task from template and get entry time for task
    station =Station(0,stemplate.pathnode)
    #preprocessInstance(tasks,machines,battery,g)

    
    temp = Template(linen, garbage,misc,machines,station,g,tasks)
    return temp

    

def generateInstanceFromTemplate(nrTasks,nrMachines,battery,setup,dummy,instancefile,graphfile,fname):

    '''
    So in order to place tasks around, a graph should show and display
    nr of tasks left to place. When clicking on the graph, the closest line
    is detected.
    Once created, use map again to click tasks/machines in
    Will also need simple routing to calculate setup and
    processing

    start by showing the map(with plotted points from the graph)
    Let user click points on map and save them depending on if
    in task or machine mode.
    '''
    if not os.path.isfile(graphfile):
        print "File '"+graphfile+"' does not exist!"
        return False
    if not os.path.isfile(instancefile):
        print "File '"+instancefile+"' does not exist!"
        return False
    if os.path.isfile(fname):
        print "File '"+fname+"' already exists!"
        return False
    g = readGraph(graphfile)
    template = readInstanceTemplate(g,instancefile)
    if type(g) is not bool and type(template is not bool):
        instance = createFromTemplate(g,template,nrTasks,nrMachines)
        
        #s = generateInstanceFromGraph(instance,g)
        return writeToFile(fname,writeTemplateInstance(instance))
    else:
        return g
    
    s = ""

    return s

#call when making annotation for graph(tasks machines...)
def createInstanceFromGraph(gfile,fname,mapfile):
    if not os.path.isfile(gfile):
        print "File '"+gfile+"' does not exist!"
        return False
    if os.path.isfile(fname):
        print "File '"+fname+"' already exists!"
        return False
    g = readGraph(gfile)
    if type(g) is not bool:
        p = Placer()
        instance = p.placeTasksMachines(g,mapfile)
        #s = generateInstanceFromGraph(instance,g)
        return writeToFile(fname,instance)
    else:
        return g




def parseInstance(*args):
    content = args[0]
    graph = args[1]
    line = content.line
    tokens = line.split(" ")
    machines = []
    garbage = []
    linen = []
    misc = []
    tasks = []
    battery = []
    #task type start end origin dest ptime entry
    if "-t" in tokens[0]:
        line = content.nextLine()
        tokens = line.split(" ")
        while "-m" not in tokens[0]:
            indexorigin = int(round(float(tokens[4])))
            o = graph[indexorigin]
            indexdest = int(round(float(tokens[5])))
            d = graph[indexdest]
            index = int(round(float(tokens[0])))
            kind = tokens[1]
            start = int(round(float(tokens[2])))
            end = int(round(float(tokens[3])))
            origin = o
            dest = d
            ptime = int(round(float(tokens[6])))
            entry = int(round(float(tokens[7])))
            task = Task(index,kind,start,end,origin,dest,ptime,entry)
            #type
            if "g" in tokens[1]:
                garbage.append(task)
            elif "l" in tokens[1]:
                linen.append(task)
            else:
                misc.append(task)
            tasks.append(task)
            line = content.nextLine()
            tokens = line.split(" ")
    #machine pos batterylevel
    if "-m" in tokens[0]:
        line = content.nextLine()
        tokens = line.split(" ")
        while "-s" not in tokens[0]:
            #print tokens
            index = int(round(float(tokens[1])))
            pos = graph[index]
            index = int(round(float(tokens[0])))
            batterylvl = int(round(float(tokens[2])))
            machine = Vehicle(index,pos,batterylvl)
            machines.append(machine)
            line = content.nextLine()
            tokens = line.split(" ")
    #battery pos
    if "-s" in tokens[0]:
        line = content.nextLine()
        tokens = line.split(" ")
        while True:
            index = int(round(float(tokens[1])))
            pos = graph[index]
            index = int(round(float(tokens[0])))
            station = Station(index,pos)
            battery.append(station)
            line = content.nextLine()
            if line:
                tokens = line.split(" ")
            else:
                break
    taskoverview = TaskOverview(tasks, garbage, linen, misc, machines, battery, graph)
    taskoverview.stimes = genstimes
    return taskoverview


def parseRandomInstance(*args):
    content = args[0]
    #starti = 4
    genstimes = []
    line = content.line
    tokens = line.split(" ")
    for i in range(len(tokens)):
        tmp = []
        for j in tokens:
            tmp.append(int(j))
        genstimes.append(tmp)
        line = content.nextLine()
        tokens = line.split(" ")
    machines = []
    garbage = []
    linen = []
    misc = []
    tasks = []
    battery = []
    #task type start end origin dest ptime entry
    if "-t" in tokens[0]:
        line = content.nextLine()
        tokens = line.split(" ")
        while "-b" not in tokens[0]:
            indexorigin = int(round(float(tokens[4])))
            o = indexorigin
            indexdest = int(round(float(tokens[5])))
            d = indexdest
            index = int(round(float(tokens[0])))
            kind = tokens[1]
            start = int(round(float(tokens[2])))
            end = int(round(float(tokens[3])))
            ptime = int(round(float(tokens[6])))
            entry = int(round(float(tokens[7])))
            task = Task(index,kind,start,end,o,d,ptime,entry)
            #type
            if "g" in tokens[1]:
                garbage.append(task)
            elif "l" in tokens[1]:
                linen.append(task)
            else:
                misc.append(task)
            tasks.append(task)
            line = content.nextLine()
            tokens = line.split(" ")
    #battery full min amount time
    full = 100
    minb = 50
    chargea = 50
    charget = 25
    if "-b" in tokens[0]:
        line = content.nextLine()
        tokens = line.split(" ")
        while True:
            try:#f m a t
                for i,v in enumerate(tokens):
                    tokens[i] = int(v)
                full = tokens[0]
                minb = tokens[1]
                chargea = tokens[2]
                charget = tokens[3]
                line = content.nextLine()
                tokens = line.split(" ")
            except ValueError:
                break
    #machine pos batterylevel
    if "-m" in tokens[0]:
        line = content.nextLine()
        tokens = line.split(" ")
        while "-s" not in tokens[0]:
            #print tokens
            index = int(round(float(tokens[1])))
            pos = 0
            index = int(round(float(tokens[0])))
            batterylvl = int(round(float(tokens[2])))
            machine = Vehicle(index,pos,batterylvl,full)
            machines.append(machine)
            line = content.nextLine()
            tokens = line.split(" ")
    #battery pos
    if "-s" in tokens[0]:
        line = content.nextLine()
        tokens = line.split(" ")
        while True:
            try:
                index = int(round(float(tokens[1])))
                pos = 0
                index = int(round(float(tokens[0])))
                station = Station(index,pos)
                battery.append(station)
                line = content.nextLine()
                if line:
                    tokens = line.split(" ")
                else:
                    break
            except ValueError:
                break
    modes = []
    if "-mo" in tokens[0]:
        line = content.nextLine()
        tokens = line.split(" ")
        while True:
            try:#x,y x,y H eta
                for i,v in enumerate(tokens):
                    tokens[i] = int(v)
                modes.append(tokens[i])
                line = content.nextLine()
                tokens = line.split(" ")
            except ValueError:
                break
    stationlimit = 5
    if "-re" in tokens[0]:
        tokens = content.nextLine()
        try:
            nrstations = int(tokens)
            tokens = content.nextLine()
            stationlimit = int(tokens)
            tokens = content.nextLine()
        except ValueError:
            line = content.nextLine()
            tokens = line.split(" ")
    for i in machines:
        i.modes = generateModes(i.modes,i.full,chargea,charget)
    taskoverview = TaskOverview(tasks, garbage, linen, misc, machines, battery, None,stationlimit,full,minb,chargea,charget)
    taskoverview.stimes = genstimes
    return taskoverview

def readInstance(*args):
    fname = args[0]
    if not os.path.isfile(fname):
        print "File '"+fname+"' does not exist!"
        return False
    if len(args) > 1:
        gfile = args[1]
        if not os.path.isfile(gfile):
            print "File '"+gfile+"' does not exist!"
            return False
        try:
            with open(fname) as f:
                content = f.readlines()
            g = readGraph(gfile)
            #print "graph size "+str(len(g))
            instance = parseInstance(Content(content),g)
            f.close()
            return instance
        except IOError:
            print "Something went wrong opening the file..."
            sys.exit(0)
    else:       
        try:
            with open(fname) as f:
                content = f.readlines()
            #print "graph size "+str(len(g))
            instance = parseRandomInstance(Content(content))
            f.close()
            return instance
        except IOError:
            print "Something went wrong opening the file..."
            sys.exit(0)                
            

#createInstance(12,5,False,False,True,"test2.txt")
#createMapping("aabenraa_stue.png","graph2.txt")
#createInstanceFromGraph("graph2.txt","tanno1.txt","aabenraa_stue.png")
#generateInstanceFromTemplate(400,5,True,True,True,"tanno1.txt","graph2.txt","tinstance6.txt")

def makeRandomPlans(name):
    tmp = createPlanFromRandom(100,8)
    s = writeRandomInstance(tmp)
    return overwriteFile("taskplan-{}200.txt".format(name),s)
    

#makeRandomPlans("new")
'''
for i in range(5,26,5):
    for j in range(5,11,5):
        writeToFile("randinst_"+str(j)+"_"+str(i)+".txt",generateSimpleInstance(i,j).instanceToString())
        '''
