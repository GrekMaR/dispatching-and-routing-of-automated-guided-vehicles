\begin{figure}[H]\label{fig:plan10v20t}
\caption{Plan for instance with 10 vehicles and 20 tasks}
\begin{tikzpicture}
\foreach \r in {0,...,9}
   \draw (0,\r) node[inner sep=1pt,below=3pt,rectangle,fill=white, left=2pt] {$\r$};
\draw[thick,->,black] (0,-1)--(0,10) node[left] {$vehicles$};
\draw[fill=gray] (0.0,-0.2) rectangle (1.2,0.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,0}] (1.2,-0.2) rectangle (2.4,0.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$8$};
\draw[fill=gray] (2.4,-0.2) rectangle (3.6,0.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,8}] (3.6,-0.2) rectangle (3.9,0.2) node[below=5pt,left=3.0pt, right=0, text width=5em] {$r$};
\draw[fill=gray] (3.9,-0.2) rectangle (3.9,0.2) node[below=5pt,left=0.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,0}] (3.9,-0.2) rectangle (5.9,0.2) node[below=5pt,left=20.0pt, right=0, text width=5em] {$3$};
\draw[fill=gray] (0.0,0.8) rectangle (1.2,1.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,0}] (1.2,0.8) rectangle (2.4,1.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$1$};
\draw[fill=gray] (2.4,0.8) rectangle (3.6,1.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,8}] (3.6,0.8) rectangle (3.9,1.2) node[below=5pt,left=3.0pt, right=0, text width=5em] {$r$};
\draw[fill=gray] (3.9,0.8) rectangle (3.9,1.2) node[below=5pt,left=0.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,0}] (3.9,0.8) rectangle (5.9,1.2) node[below=5pt,left=20.0pt, right=0, text width=5em] {$19$};
\draw[fill=gray] (0.0,1.8) rectangle (1.2,2.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,0}] (1.2,1.8) rectangle (2.4,2.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$11$};
\draw[fill=gray] (2.4,1.8) rectangle (3.6,2.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,8}] (3.6,1.8) rectangle (3.9,2.2) node[below=5pt,left=3.0pt, right=0, text width=5em] {$r$};
\draw[fill=gray] (3.9,1.8) rectangle (3.9,2.2) node[below=5pt,left=0.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,0}] (3.9,1.8) rectangle (5.9,2.2) node[below=5pt,left=20.0pt, right=0, text width=5em] {$0$};
\draw[fill=gray] (0.0,2.8) rectangle (1.2,3.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,0}] (1.2,2.8) rectangle (2.4,3.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$17$};
\draw[fill=gray] (2.4,2.8) rectangle (3.6,3.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,8}] (3.6,2.8) rectangle (3.9,3.2) node[below=5pt,left=3.0pt, right=0, text width=5em] {$r$};
\draw[fill=gray] (3.9,2.8) rectangle (3.9,3.2) node[below=5pt,left=0.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,0}] (3.9,2.8) rectangle (5.9,3.2) node[below=5pt,left=20.0pt, right=0, text width=5em] {$7$};
\draw[fill=gray] (0.0,3.8) rectangle (1.2,4.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,0}] (1.2,3.8) rectangle (2.4,4.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$14$};
\draw[fill=gray] (2.4,3.8) rectangle (3.6,4.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,8}] (3.6,3.8) rectangle (3.9,4.2) node[below=5pt,left=3.0pt, right=0, text width=5em] {$r$};
\draw[fill=gray] (3.9,3.8) rectangle (3.9,4.2) node[below=5pt,left=0.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,0}] (3.9,3.8) rectangle (5.9,4.2) node[below=5pt,left=20.0pt, right=0, text width=5em] {$4$};
\draw[fill=gray] (0.0,4.8) rectangle (1.2,5.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,0}] (1.2,4.8) rectangle (2.4,5.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$10$};
\draw[fill=gray] (2.4,4.8) rectangle (3.6,5.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,8}] (3.6,4.8) rectangle (3.9,5.2) node[below=5pt,left=3.0pt, right=0, text width=5em] {$r$};
\draw[fill=gray] (3.9,4.8) rectangle (3.9,5.2) node[below=5pt,left=0.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,0}] (3.9,4.8) rectangle (5.9,5.2) node[below=5pt,left=20.0pt, right=0, text width=5em] {$9$};
\draw[fill=gray] (0.0,5.8) rectangle (1.2,6.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,0}] (1.2,5.8) rectangle (2.4,6.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$16$};
\draw[fill=gray] (2.4,5.8) rectangle (3.6,6.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,8}] (3.6,5.8) rectangle (3.9,6.2) node[below=5pt,left=3.0pt, right=0, text width=5em] {$r$};
\draw[fill=gray] (3.9,5.8) rectangle (3.9,6.2) node[below=5pt,left=0.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,0}] (3.9,5.8) rectangle (5.9,6.2) node[below=5pt,left=20.0pt, right=0, text width=5em] {$18$};
\draw[fill=gray] (0.0,6.8) rectangle (1.2,7.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,0}] (1.2,6.8) rectangle (2.4,7.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$15$};
\draw[fill=gray] (2.4,6.8) rectangle (3.6,7.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,8}] (3.6,6.8) rectangle (3.9,7.2) node[below=5pt,left=3.0pt, right=0, text width=5em] {$r$};
\draw[fill=gray] (3.9,6.8) rectangle (3.9,7.2) node[below=5pt,left=0.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,0}] (3.9,6.8) rectangle (5.9,7.2) node[below=5pt,left=20.0pt, right=0, text width=5em] {$12$};
\draw[fill=gray] (0.0,7.8) rectangle (1.2,8.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,0}] (1.2,7.8) rectangle (2.4,8.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$2$};
\draw[fill=gray] (2.4,7.8) rectangle (3.6,8.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,8}] (3.6,7.8) rectangle (3.9,8.2) node[below=5pt,left=3.0pt, right=0, text width=5em] {$r$};
\draw[fill=gray] (3.9,7.8) rectangle (3.9,8.2) node[below=5pt,left=0.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,0}] (3.9,7.8) rectangle (5.9,8.2) node[below=5pt,left=20.0pt, right=0, text width=5em] {$5$};
\draw[fill=gray] (0.0,8.8) rectangle (1.2,9.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,0}] (1.2,8.8) rectangle (2.4,9.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$6$};
\draw[fill=gray] (2.4,8.8) rectangle (3.6,9.2) node[below=5pt,left=12.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,8}] (3.6,8.8) rectangle (3.9,9.2) node[below=5pt,left=3.0pt, right=0, text width=5em] {$r$};
\draw[fill=gray] (3.9,8.8) rectangle (3.9,9.2) node[below=5pt,left=0.0pt, right=0, text width=5em] {$ $};
\draw[fill={rgb:red,2;green,5;yellow,0}] (3.9,8.8) rectangle (5.9,9.2) node[below=5pt,left=20.0pt, right=0, text width=5em] {$13$};
\draw[thick,->,black] (0,-1)--(5.9,-1) node[left, below=10pt] {$time$};
\foreach \r in {0, 5,...,59.0}
   \draw (\r*0.1,-1) node[inner sep=1pt,below=5pt,rectangle,fill=white] {$\r$};
\end{tikzpicture}
\end{figure}