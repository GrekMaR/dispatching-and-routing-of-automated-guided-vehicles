#!/usr/bin/Rscript
## Get them args :)
#args <- commandArgs(trailingOnly = TRUE)

times <- function(input,fname,fname2){
  args = c(input,fname,fname2)
  #read in results
  results <- read.csv(file=args[1],header=TRUE, sep=" ")
  names(results)
  
  results <- aggregate(cbind(time_preprocessing,time_solving,obj) ~ instance+samplesize, results, mean)
  X <- split(results,results$instance)
  #APPLY MEAN
  #print(results)
  #Now for each instance, plot x = sample, y = time
  linecol = rainbow(28)#c("green","blue","red","orange", "darkred", "black","blue4","darkgreen")
  library("reshape2")
  library("ggplot2")
  #reshaped <- melt(results,id="instance",variable="samplesize",value="time_preprocessing")
  #print(results)
  svg(filename=fname)
  p1 <-ggplot(data=results) +
    xlab("Sample Size") + ylab("Preprocessing Time")+
    geom_line(aes(x =results$samplesize, y =results$time_preprocessing,
                  group=instance,color=instance))+
    ggtitle("Time spent preprocessing given different samplesizes. ")+
    theme(plot.title = element_text(family = "Trebuchet MS", hjust=0))
  p2 <-ggplot(data=results) +
    xlab("Sample Size") + ylab("Solve Time")+
    geom_line(aes(x =results$samplesize, y =results$time_solving,
                  group=instance,color=instance))+
    ggtitle("Time spent solving given different samplesizes.")+
    theme(plot.title = element_text(family = "Trebuchet MS", hjust=0))
  pp <-plot_grid(p1, p2, labels = "AUTO", ncol = 1, align = 'v')
  print(pp)
  dev.off()
  
  svg(filename=fname2)
  #obj
  p1 <-ggplot(data=results) +
    xlab("Sample Size") + ylab("Objective Value")+
    geom_line(aes(x =results$samplesize, y =results$obj,
                  group=instance,color=instance))+
    ggtitle("Objective value given different samplesizes. ")+
    coord_fixed(ratio = 1)+
    scale_x_continuous(breaks = seq(0, 1000, 100))+
    scale_y_continuous(breaks = seq(0,500,100),limits=c(0,500))+
    theme(plot.title = element_text(family = "Trebuchet MS", hjust=0))
  #pp <-plot_grid(pp, p1, labels = "AUTO", ncol = 2, align = 'h')
  print(p1)
  dev.off()
  
  
  
}

timespair  <- function(input1,input2, fname1, fname2){
  args = c(input1,input2,fname1,fname2)
  #read in results
  results1 <- read.csv(file=args[1],header=TRUE, sep=" ")
  results2 <- read.csv(file=args[2],header=TRUE, sep=" ")
  names(results)
  X <- split(results,results$instance)
  #APPLY MEAN
  results1 <- aggregate(cbind(time_preprocessing,time_solving,obj) ~ instance+samplesize, results1, mean)
  results2 <- aggregate(cbind(time_preprocessing,time_solving,obj) ~ instance+samplesize, results2, mean)
  #Now for each instance, plot x = sample, y = time
  linecol = rainbow(28)#c("green","blue","red","orange", "darkred", "black","blue4","darkgreen")
  library("reshape2")
  library("ggplot2")
  #reshaped <- melt(results,id="instance",variable="samplesize",value="time_preprocessing")
  #print(results)
  svg(filename=fname1)
  p1 <-ggplot(data=results1) +
    xlab("Sample Size") + ylab("Preprocessing Time")+
    geom_line(aes(x =results$samplesize, y =results$time_preprocessing,
                  group=instance,color=instance))+
    ggtitle("Time spent preprocessing given different samplesizes. ")+
    theme(plot.title = element_text(family = "Trebuchet MS", hjust=0))
  p2 <-ggplot(data=results2) +
    xlab("Sample Size") + ylab("Preprocessing Time")+
    geom_line(aes(x =results$samplesize, y =results$time_preprocessing,
                  group=instance,color=instance))+
    ggtitle("Time spent preprocessing given different samplesizes. ")+
    theme(plot.title = element_text(family = "Trebuchet MS", hjust=0))
  pp <-plot_grid(p1, p2, labels = "AUTO", ncol = 1, align = 'v')
  print(pp)
  dev.off()
  
  svg(filename=fname2)
  p1 <-ggplot(data=results1) +
    xlab("Sample Size") + ylab("Solve Time")+
    geom_line(aes(x =results$samplesize, y =results$time_solving,
                  group=instance,color=instance))+
    ggtitle("Time spent solving given different samplesizes.")+
    theme(plot.title = element_text(family = "Trebuchet MS", hjust=0))
  p2 <-ggplot(data=results2) +
    xlab("Sample Size") + ylab("Solve Time")+
    geom_line(aes(x =results$samplesize, y =results$time_solving,
                  group=instance,color=instance))+
    ggtitle("Time spent solving given different samplesizes.")+
    theme(plot.title = element_text(family = "Trebuchet MS", hjust=0))
  pp <-plot_grid(p1, p2, labels = "AUTO", ncol = 1, align = 'v')
  print(pp)
  dev.off()
  
  
  
  
  
  
  
}