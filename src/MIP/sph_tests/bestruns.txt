Best runs

HEAVY

-200 off 3 no starts
\begin{table}[H]
\centering
\caption{Table showing the results from running the heuristic set partitioning model on instances with varying sets of columns. These runs used a makespan objective function. The schedule generation phase was done using sampling. Additionally, offsetting was applied to some of the schedules.}
\label{sphperf}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
Vehicles & Tasks & \#Cols & Tasklimits & \#St & \#Ov & CG Time & Solve Time & Obj.\\ \hline 
5 & 10 & 1255 & (1.0, 3.0) & 1 & 3 & 0.5815 & 0.0744 & 105.0\\ \hline 
5 & 15 & 1668 & (2.0, 4.0) & 1 & 3 & 2.0058 & 0.3895 & 207.0\\ \hline
5 & 20 & 1900 & (3.0, 5.0) & 1 & 3 & 2.8085 & 0.9163 & 282.0\\ \hline
5 & 25 & 2333 & (4.0, 6.0) & 1 & 3 & 4.9521 & 9.9024 & 452.0\\ \hline 
10 & 15 & 2636 & (1.0, 3.0) & 1 & 5 & 3.9737 & 0.2067 & 124.0\\ \hline 
10 & 20 & 2533 & (1.0, 3.0) & 1 & 5 & 0.7999 & 0.612 & 78.0\\ \hline 
10 & 25 & 3273 & (2.0, 4.0) & 1 & 5 & 6.9286 & 2.469 & 147.0\\ \hline \\ \hline 
\end{tabular}
\end{table}



300 off 3 no starts

\begin{table}[H]
\centering
\caption{Table showing the results from running the heuristic set partitioning model on instances with varying sets of columns. These runs used a makespan objective function. The schedule generation phase was done using sampling. Additionally, offsetting was applied to some of the schedules.}
\label{sphperf}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
Vehicles & Tasks & \#Cols & Tasklimits & \#St & \#Ov & CG Time & Solve Time & Obj.\\ \hline 
5 & 10 & 1865 & (1.0, 3.0) & 1 & 3 & 1.3808 & 0.0926 & 104.0\\ \hline 
5 & 15 & 2501 & (2.0, 4.0) & 1 & 3 & 4.7207 & 1.7125 & 207.0\\ \hline 
5 & 20 & 2806 & (3.0, 5.0) & 1 & 3 & 6.2683 & 2.6801 & 282.0\\ \hline 
5 & 25 & 3463 & (4.0, 6.0) & 1 & 3 & 9.7106 & 18.96 & 426.0\\ \hline 
10 & 15 & 3960 & (1.0, 3.0) & 1 & 5 & 9.3776 & 0.1924 & 124.0\\ \hline 
10 & 20 & 3706 & (1.0, 3.0) & 1 & 5 & 1.5829 & 0.3953 & 77.0\\ \hline 
10 & 25 & 4963 & (2.0, 4.0) & 1 & 5 & 16.7073 & 1.6091 & 146.0\\ \hline  
\end{tabular}
\end{table}


500 off 3 no starts
\centering
\caption{Table showing the results from running the heuristic set partitioning model on instances with varying sets of columns. These runs used a makespan objective function. The schedule generation phase was done using sampling. Additionally, offsetting was applied to some of the schedules.}
\label{sphperf}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
Vehicles & Tasks & \#Cols & Tasklimits & \#St & \#Ov & CG Time & Solve Time & Obj.\\ \hline 
5 & 10 & 3170 & (1.0, 3.0) & 1 & 3 & 5.4101 & 0.1383 & 104.0\\ \hline 
5 & 15 & 4178 & (2.0, 4.0) & 1 & 3 & 13.5508 & 0.6371 & 207.0\\ \hline 
5 & 20 & 4616 & (3.0, 5.0) & 1 & 3 & 18.0126 & 1.9064 & 282.0\\ \hline 
5 & 25 & 5831 & (4.0, 6.0) & 1 & 3 & 26.9986 & 15.1397 & 386.0\\ \hline 
10 & 15 & 6566 & (1.0, 3.0) & 1 & 5 & 24.7457 & 0.3559 & 124.0\\ \hline
10 & 20 & 6260 & (1.0, 3.0) & 1 & 5 & 4.9306 & 0.588 & 74.0\\ \hline 
10 & 25 & 8316 & (2.0, 4.0) & 1 & 5 & 45.8697 & 2.8392 & 146.0\\ \hline 
\end{tabular}
\end{table}



1000 off 3 no starts
\begin{table}[H]
\centering
\caption{Table showing the results from running the heuristic set partitioning model on instances with varying sets of columns. These runs used a makespan objective function. The schedule generation phase was done using sampling. Additionally, offsetting was applied to some of the schedules.}
\label{sphperf}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
Vehicles & Tasks & \#Cols & Tasklimits & \#St & \#Ov & CG Time & Solve Time & Obj.\\ \hline 
5 & 10 & 6299 & (1.0, 3.0) & 1 & 3 & 18.522 & 0.2667 & 104.0\\ \hline
5 & 15 & 8355 & (2.0, 4.0) & 1 & 3 & 55.1209 & 0.9974 & 207.0\\ \hline
5 & 20 & 9263 & (3.0, 5.0) & 1 & 3 & 70.6751 & 3.4718 & 282.0\\ \hline 
5 & 25 & 11651 & (4.0, 6.0) & 1 & 3 & 113.5154 & 65.5382 & 383.0\\ \hline 
10 & 15 & 13430 & (1.0, 3.0) & 1 & 5 & 106.348 & 1.3801 & 124.0\\ \hline 
10 & 20 & 12656 & (1.0, 3.0) & 1 & 5 & 23.2818 & 1.1189 & 73.0\\ \hline 
10 & 25 & 16550 & (2.0, 4.0) & 1 & 5 & 179.821 & 3.878 & 146.0\\ \hline
\end{tabular}
\end{table}


LIGHT

300 off 0 no start sol

\begin{table}[H]
\centering
\caption{Table showing the results from running the heuristic set partitioning model on instances with varying sets of columns. These runs used a makespan objective function. The schedule generation phase was done using sampling. Additionally, offsetting was applied to some of the schedules.}
\label{sphperf}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
Vehicles & Tasks & \#Cols & Tasklimits & \#St & \#Ov & CG Time & Solve Time & Obj.\\ \hline 
5 & 10 & 2005 & (1.0, 3.0) & 1 & 3 & 1.8953 & 0.1456 & 104.0\\ \hline 
5 & 15 & 2483 & (2.0, 4.0) & 1 & 3 & 4.4669 & 0.9254 & 207.0\\ \hline 
5 & 20 & 1671 & (3.0, 5.0) & 1 & 3 & 0.036 & 0.771 & 192.0\\ \hline 
5 & 25 & 1760 & (4.0, 6.0) & 1 & 3 & 0.0509 & 8.9716 & 264.0\\ \hline  
10 & 15 & 3900 & (1.0, 3.0) & 1 & 5 & 7.9403 & 0.1814 & 124.0\\ \hline 
10 & 20 & 3706 & (1.0, 3.0) & 1 & 5 & 1.5802 & 0.3969 & 77.0\\ \hline 
10 & 25 & 3310 & (2.0, 4.0) & 1 & 3 & 0.2988 & 0.2579 & 93.0\\ \hline 
\end{tabular}
\end{table}

500 off 0 no start sol
\begin{table}[H]
\centering
\caption{Table showing the results from running the heuristic set partitioning model on instances with varying sets of columns. These runs used a makespan objective function. The schedule generation phase was done using sampling. Additionally, offsetting was applied to some of the schedules.}
\label{sphperf}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
Vehicles & Tasks & \#Cols & Tasklimits & \#St & \#Ov & CG Time & Solve Time & Obj.\\ \hline 
5 & 10 & 3335 & (1.0, 3.0) & 1 & 3 & 5.659 & 0.1627 & 104.0\\ \hline 
5 & 15 & 4161 & (2.0, 4.0) & 1 & 3 & 14.3076 & 0.4704 & 207.0\\ \hline 
5 & 20 & 2791 & (3.0, 5.0) & 1 & 3 & 0.0881 & 1.7627 & 185.0\\ \hline 
5 & 25 & 2975 & (4.0, 6.0) & 1 & 3 & 0.1146 & 21.0715 & 251.0\\ \hline 
10 & 15 & 6636 & (1.0, 3.0) & 1 & 5 & 24.6081 & 0.3739 & 124.0\\ \hline 
10 & 20 & 6260 & (1.0, 3.0) & 1 & 5 & 4.9262 & 0.4 & 74.0\\ \hline 
10 & 25 & 5573 & (2.0, 4.0) & 1 & 3 & 0.4305 & 0.3476 & 93.0\\ \hline 
\end{tabular}
\end{table}

1000 off 0 no start sol
\begin{table}[H]
\centering
\caption{Table showing the results from running the heuristic set partitioning model on instances with varying sets of columns. These runs used a makespan objective function. The schedule generation phase was done using sampling. Additionally, offsetting was applied to some of the schedules.}
\label{sphperf}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
Vehicles & Tasks & \#Cols & Tasklimits & \#St & \#Ov & CG Time & Solve Time & Obj.\\ \hline 
5 & 10 & 6583 & (1.0, 3.0) & 1 & 3 & 22.4051 & 0.2668 & 104.0\\ \hline 
5 & 15 & 8268 & (2.0, 4.0) & 1 & 3 & 55.7173 & 0.8753 & 207.0\\ \hline 
5 & 20 & 5585 & (3.0, 5.0) & 1 & 3 & 0.217 & 2.0664 & 180.0\\ \hline 
5 & 25 & 5911 & (4.0, 6.0) & 1 & 3 & 0.3207 & 9.8177 & 235.0\\ \hline 
10 & 15 & 13166 & (1.0, 3.0) & 1 & 5 & 96.7778 & 0.8857 & 124.0\\ \hline
10 & 20 & 12760 & (1.0, 3.0) & 1 & 5 & 25.0978 & 0.731 & 73.0\\ \hline
10 & 25 & 11116 & (2.0, 4.0) & 1 & 3 & 1.4263 & 0.6325 & 93.0\\ \hline 
\end{tabular}
\end{table}

HEAVY

off0


500  off0 no start
\begin{table}[H]
\centering
\caption{Table showing the results from running the heuristic set partitioning model on instances with varying sets of columns. These runs used a makespan objective function. The schedule generation phase was done using sampling. }
\label{sphperf}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
Vehicles & Tasks & \#Cols & Tasklimits & \#St & \#Ov & CG Time & Solve Time & Obj.\\ \hline 
5 & 10 & 2405 & (1.0, 3.0) & 1 & 3 & 1.2491 & 0.0377 & 162.0\\ \hline 
5 & 15 & 2500 & (2.0, 4.0) & 1 & 3 & 3.093 & 0.2396 & 207.0\\ \hline 
5 & 20 & 2500 & (3.0, 5.0) & 1 & 3 & 5.5143 & 1.6863 & 282.0\\ \hline 
5 & 25 & 2500 & (4.0, 6.0) & 1 & 3 & 8.0966 & 6.9257 & 411.0\\ \hline 
10 & 15 & 5000 & (1.0, 3.0) & 1 & 5 & 4.0749 & 0.0762 & 124.0\\ \hline 
10 & 20 & 5000 & (1.0, 3.0) & 1 & 5 & 3.9605 & 0.2086 & 79.0\\ \hline 
10 & 25 & 5000 & (2.0, 4.0) & 1 & 5 & 21.5907 & - & -\\ \hline 
\end{tabular}
\end{table}

1000 off0 no start
\begin{table}[H]
\centering
\caption{Table showing the results from running the heuristic set partitioning model on instances with varying sets of columns. These runs used a makespan objective function. The schedule generation phase was done using sampling. }
\label{sphperf}
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
Vehicles & Tasks & \#Cols & Tasklimits & \#St & \#Ov & CG Time & Solve Time & Obj.\\ \hline 
5 & 10 & 4840 & (1.0, 3.0) & 1 & 3 & 5.0435 & 0.054 & 162.0\\ \hline 
5 & 15 & 5000 & (2.0, 4.0) & 1 & 3 & 10.9839 & 0.1561 & 207.0\\ \hline
5 & 20 & 5000 & (3.0, 5.0) & 1 & 3 & 21.3935 & 0.7952 & 282.0\\ \hline 
5 & 25 & 5000 & (4.0, 6.0) & 1 & 3 & 30.47 & 15.2606 & 384.0\\ \hline 
10 & 15 & 10000 & (1.0, 3.0) & 1 & 5 & 16.5524 & 0.1548 & 124.0\\ \hline 
10 & 20 & 10000 & (1.0, 3.0) & 1 & 5 & 9.7247 & 0.3399 & 79.0\\ \hline 
10 & 25 & 10000 & (2.0, 4.0) & 1 & 5 & 71.322 & - & -\\ \hline 
\end{tabular}
\end{table}


HEAVY off0 startsol RUNNING


200 off0 no start



