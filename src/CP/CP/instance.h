/* 
 * File:   instance.h
 * Author: thor
 *
 * Created on October 8, 2015, 1:43 PM
 */

#ifndef INSTANCE_H
#define	INSTANCE_H

#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <vector>       // std::vector
#include <stdlib.h>     // std::atoi

using namespace std;
typedef vector<int> Row;
typedef vector<Row> Matrix;

class Instance { // num_tasks = n, num_vehicles = m, num_recharges = r
    int num_tasks, num_vehicles, num_recharges, total_tasks, max, 
    lowerBound, upperBound, maxRechargeTime, maxBatterySetup;
    int* processTime;         // duration for all tasks [0,...,n-1]
    Matrix setuptimes;
    int* starttimes;        // starttimes for all tasks [0,...,n-1]
    int* batterycosts;      // battery cost for each task [0,...,n-1]
    int* initialbattery;    // initial battery for each vehicle [0,...,m-1]
    int* minMaxBattery;     // [lowerbound, upperbound, battery recharge rate]
    int* deadlines;         // deadline for each task [0,...,n-1]
    int* vehicleStartTimes; // start times for a machine, since it could be busy
    int var_index, val_index;
    int timelimit;
    int battery_resources;
    const char* firstOut;
    const char* secondOut;
    const char* jsFirstOut;
    const char* jsBestOut;
    vector<int> setupsToDepot;
    
public:
    
    Instance();             // use dummy instance, to test the model
    Instance(const char* input, const char* outB, 
    int timelimit, int battery_amount, int setupAtEnd, int batRechargeType, int startNumRecharges); 
    // use input given
    void printProblem();    // print out the problem instance
    void addBatteryRecharge();
    // getters for the problem instance
    const int getNumTasks();
    const int getNumVehicles();
    const int getNumRecharges();
    const int getTotalTasks();
    const int getMax();
    const int getBatteryResourceAmount();
    const int getLowerBound();
    const int getUpperBound();
    const int getMaxRecharge();
    const int getMinBattery();
    const int getMaxBattery();
    int* getProcessTimes();
    Matrix getSetuptimes();
    int* getStarttimes();
    int* getBatterycost();
    int* getInitialBattery();
    int* getMinMaxBattery();
    int* getDeadlines();    
    int getTimelimit();
    int getVarIndex();
    int getValIndex();
    int goToSetupAtEnd;
    int batteryRechargePerTimeUnit;
    int maxBatteryCapacity;
    int minBatteryCapacity;
    int spanBetweenMinAndMaxBatteryCapacity;
    int batRechargeType;
    int maxRechargeDuration;
    void setVarIndex(int);
    void setValIndex(int);
    const char* getFirstOut();
    const char* getSecondOut();
    const char* getJsFirstOut();
    const char* getJsBestOut();
    void setFirstOut(char*);
    void setSecondOut(char*);
    void setJsFirstOut(char*);
    void setJsBestOut(char*);
    int* getVehicleStartTimes();
    vector<int> getSetupsToDepot() { return setupsToDepot; }
private:

};

#endif	/* INSTANCE_2_H */

