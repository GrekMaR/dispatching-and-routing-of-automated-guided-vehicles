/* 
 * File:   instance.cpp
 * Author: thor
 * 
 * Created on October 8, 2015, 1:43 PM
 */
using namespace std;

#include "instance.h"

void Instance::printProblem() {
    int n = num_tasks;
    int m = num_vehicles;
    int r = num_recharges;
    cout << "---------------------------------------\n";
    cout << "----     Problem Instance:        ---\n";
    cout << "---------------------------------------\n";
    cout << "num_tasks: " << num_tasks << endl;
    cout << "num_vehicles: " << num_vehicles << endl;
    cout << "num_recharges: " << num_recharges << endl;
    cout << "Durations: [";
    for (int i = 0; i < n; i++) {
        cout << processTime[i] << ", ";
    }
    cout << "]" << endl;
    cout << "Starttimes: [";
    for (int i = 0; i < n; i++) {
        cout << starttimes[i] << ", ";
    }
    cout << "]" << endl;
    cout << "Deadlines: [";
    for (int i = 0; i < n; i++) {
        cout << deadlines[i] << ", ";
    }
    cout << "]" << endl;
    /*cout << "initial setup: [" << endl;
    for (int i = 0; i < n; i++) {
        cout << initialSetup[i] << ", ";
    }
    cout << "]" << endl;*/
    cout << "Setuptimes:" << endl;
    for (int i = 0; i < n+r+m; i++) {
        cout << "            [";
        for (int j = 0; j < n+r+m; j++) {
            cout << setuptimes[i][j] << ", ";
        }
        cout << "]" << endl;
    }
    cout << "]" << endl;
    cout << "Battery lower, upper and charge amount: ";
    cout << minMaxBattery[0] << ", ";
    cout << minMaxBattery[1] << ", ";
    cout << minMaxBattery[2] << endl;
    cout << "Initial battery: [";
    for (int i = 0; i < m; i++) {
        cout << initialbattery[i] << ", ";
    }
    cout << "]" << endl;
    cout << "Battery cost for each task: [";
    for (int i = 0; i < n; i++) {
        cout << batterycosts[i] << ", ";
    }
    cout << "]" << endl;
    cout << "timelimit: " << timelimit << endl;
    cout << "max recharge duration: " << maxRechargeDuration << endl;
    cout << "battery per time unit: " << batteryRechargePerTimeUnit << endl;
    cout << "battery span between min and max: " << spanBetweenMinAndMaxBatteryCapacity << endl;
    if (batRechargeType == 0) {
        cout << "battery type is : " << batRechargeType << " = VARIABLE RECHARGES" << endl;
    } else {
        cout << "battery type is : " << batRechargeType << " = FIXED RECHARGES" << endl;
    }
    cout << "---------------------------------------\n";
    cout << "---------------------------------------\n";
    
}

Instance::Instance () {
    cout << "new Instance" << endl;
}

Instance::Instance(const char* fileToRead, const char* outB, int timelimit, 
        int battery_resources, int setupAtEnd, int batRechargeType, int startNumRecharges) {
    this->goToSetupAtEnd = setupAtEnd;
    this->batRechargeType = batRechargeType;
    // read in the line
    cout << "\nReading Instance: " << fileToRead<< "\n";
    string line;
    ifstream a_file (fileToRead);
    vector<int> input;
    if (a_file.is_open())
    {
        string word;        
        while (a_file >> word) {
            if (word.find("-")==string::npos) {
                input.push_back(atoi(word.c_str()));
            }
        }
        a_file.close();
    }
    cout << "lolol" << endl;
    this->timelimit = timelimit;
    this->firstOut = firstOut;
    this->secondOut = outB;
    this->jsFirstOut = jsFirstOut;
    this->jsBestOut = jsBestOut;
    this->battery_resources = battery_resources;
    num_tasks = input[0];
    num_vehicles = input[1];
    int n = num_tasks;
    int m = num_vehicles;       
    Matrix st;
    st.resize(n+1);
    for (int i = 0; i < n+1; i++) {
        st[i].resize(n+1);
    }
    cout << "n: " << num_tasks << ", m: " << num_vehicles << endl;
    var_index = 0;
    val_index = 0;
    // fill in task processing time
    processTime = new int[n];             // duration for all tasks [0,...,n-1]
    starttimes = new int[n];
    batterycosts = new int[n];          // battery cost for each task [0,...,n-1]
    initialbattery = new int[m];        // initial battery for each vehicle [0,...,m-1]
    minMaxBattery = new int[3];         // [lowerbound, upperbound, battery recharge rate]
    deadlines = new int[n];             // deadline for each task [0,...,n-1]
    //initialSetup = new int[n];          // initial setuptimes, for first task
                                        // scheduled on each vehicle,
    //batterySetup = new int[n];          // battery setuptimes
    vehicleStartTimes = new int[m];     // min start times for each machine, 
    max = 0;
    // skip input[0,1,2]: Since 
    // 0 = num_tasks,
    // 1 = num_vehicles and
    // 2 = start dummy task duration.
    int index = 3;

    for (int i = 0; i < n; i++) {       // fill in durations
        processTime[i] = input[index];
        index++;
    }
    index++;                            // skip past end dummy task duration
    
    if (input[index] > 2147483644) {    // fill in max time
        max = 2147483644;
    } else {
        max = input[index];
    }
    index+=2;                            // skip past start dummy task deadline
    for (int i = 0; i < n; i++) {       // fill in starttimes
        starttimes[i] = input[index];
        index++;
    }
    index+=2;                            // skip past end and start dummy task deadline
    for (int i = 0; i < n; i++) {       // fill in deadlines
        deadlines[i] = input[index];
        index++;
    }
    cout << "input[index]: " << input[index] << endl;
    index++;                            // skip past end dummy task deadline
    // fill in for all normal tasks
    for (int i = 0; i < n+2; i++) {
        for (int j = 0; j < n+2; j++) {
            if (i > 0 && j > 0) {
                st[i-1][j-1] = input[index];
            }
            if (j == n+1 && i > 0 && i < n+2) {
                setupsToDepot.push_back(input[index]);
            }
            index++;
        }
    }
    
    minMaxBattery[1] = input[index];
    index++;
    minMaxBattery[0] = input[index];
    index++;
    // fill in charge amount per charge unit
    minMaxBattery[2] = input[index+1]/input[index];
    index++;
    batteryRechargePerTimeUnit = input[index+1]/input[index];
    maxRechargeDuration = input[index];
    spanBetweenMinAndMaxBatteryCapacity = minMaxBattery[1]-minMaxBattery[0];
    index++; // skip past charge amount
    
    for (int i = 0; i < m; i++) {       // fill in initial battery
        initialbattery[i] = input[index];
        index++;
    }
    
    index++;                            // skip past start dummy task batterycost
    for (int i = 0; i < n; i++) {       // fill in batterycost for each task
        if (i < n) {
            batterycosts[i] = input[index];
            index++;
        }
    }
    index++;                            // skip past end dummy task batterycost

    int sumBatteryCost = 0;
    for (int i = 0; i < n; i++) {
        sumBatteryCost += batterycosts[i];
    }
    int minBatteryPerVehicle = minMaxBattery[0]*m;
    int sumInitialBattery = 0;
    for (int i = 0; i < m; i++) {
        sumInitialBattery += initialbattery[i];
    }
    int maxRechargePossible = minMaxBattery[1]-minMaxBattery[0];
    int estimate = m/2;
    int avg_est = 1;
    this->num_recharges = (sumBatteryCost + minBatteryPerVehicle 
            - sumInitialBattery) / (maxRechargePossible * avg_est);
    // if we have more battery than needed initially,
    // we can schedule with out a recharge
    if (sumInitialBattery > sumBatteryCost + minBatteryPerVehicle) {
        num_recharges = 0;
    }
    int r = num_recharges;
    if (startNumRecharges > num_recharges) {
        r = startNumRecharges;
        num_recharges = startNumRecharges;
    }
    setuptimes.resize(n+r+m);
    for (int i = 0; i < n+r+m; i++) {
        setuptimes[i].resize(n+r+m);
    }
    for (int i = 0; i < n+r+m; i++) {
        for (int j = 0; j < n+r+m; j++) {
            // depot = start + end + bat tasks
            if (i > n && j > n) { // depot -> depot
                setuptimes[i][j] = 0;
            } else if (i > n) { // depot -> tasks
                setuptimes[i][j] = st[n][j];
            } else if (j > n) { // tasks -> depot
                if (j >= n+r && goToSetupAtEnd == 0) {
                    setuptimes[i][j] = 0;
                } else {
                    setuptimes[i][j] = st[i][n];    
                }
            } else { // task -> task
                setuptimes[i][j] = st[i][j];
            }
        }
    }
    for (int i = 0; i < m; i++) {
        if (input.size() > index) {
            vehicleStartTimes[i] = input[index];
            index++;
        } else {
            vehicleStartTimes[i] = 0;
        }   
    }
    
    // calculate lower and upper bounds
    maxBatterySetup = 0;
    int max_setup = 0;
    for (int i = 0; i < n+r+2*m; i++) {
        int row_max = 0;
        for (int j = 0; j < n+r+m; j++) {
            int current_setup = 0;
            if (i >= n+r+m) {
                current_setup = setuptimes[i-m][j];
            } else {
                current_setup = setuptimes[i][j];
            }
            if (row_max < current_setup)
                row_max = current_setup;
            if (i >= n && maxBatterySetup < current_setup) {
                // save max battery setup if we increase number of recharges
                maxBatterySetup = current_setup;
            }
        }
        max_setup += row_max;
    }
    int sum_dur = 0;
    for (int i = 0; i < n; i++) {
        sum_dur += processTime[i];
    }
    lowerBound = sum_dur / num_vehicles;
    // calculate number of recharges nedded
    // (max-min) / battery per unit of recharge
    maxRechargeTime = 
    (minMaxBattery[1]-minMaxBattery[0])/minMaxBattery[2];
    
    int maxVehicleStart = 0;
    for (int i = 0; i < m; i++) {
        if (maxVehicleStart < vehicleStartTimes[i])
            maxVehicleStart = vehicleStartTimes[i];
    }
    
    int maxTaskStart = 0;
    for (int i = 0; i < n; i++) {
        if (maxTaskStart < starttimes[i])
            maxTaskStart = starttimes[i];
    }
    upperBound = max_setup + sum_dur +  maxRechargeTime*r + maxVehicleStart + maxTaskStart;
}

void Instance::addBatteryRecharge() {
    int n = num_tasks;
    int m = num_vehicles;
    int r = num_recharges;
    
    // save old setup time matrix
    Matrix st;
    st.resize(n+r+m);
    for (int i = 0; i < n+r+m; i++) {
        st[i].resize(n+r+m);
        for (int j = 0; j < n+r+m; j++) {
            st[i][j] = setuptimes[i][j];
        }
    }
    
    // add battery recharge
    num_recharges++;
    r++;
    
    // update the setup times matrix to new size
    setuptimes.resize(n+r+m);
    for (int i = 0; i < n+r+m; i++) {
        setuptimes[i].resize(n+r+m);
    }
    
    for (int i = 0; i < n+r+m; i++) {
        for (int j = 0; j < n+r+m; j++) {
            int i_index = i, j_index = j;
            if (i >= n+1)
                i_index = n;
            if (j >= n+1)
                j_index = n;
            if (i >= n+r || j >= n+r) { // aux task -> everything +
                setuptimes[i][j] = 0;
            } else { // just copy correctly (n+1 row + column is battery)
                setuptimes[i][j] = st[i_index][j_index];    
            }
        }
    }
    // add max recharge time and setup to a battery task
    upperBound += maxRechargeTime + maxBatterySetup;
}

// getters
const int Instance::getLowerBound() {
    return lowerBound;
}

const int Instance::getUpperBound() {
    return upperBound;
}

const int Instance::getNumTasks() {
    return num_tasks;
}

const int Instance::getNumVehicles() {
    return num_vehicles;
}

const int Instance::getNumRecharges() {
    return num_recharges;
}

const int Instance::getTotalTasks() {
    return total_tasks;
}

const int Instance::getMax() {
    return max;
}

int* Instance::getProcessTimes() {
    return processTime;
}

Matrix Instance::getSetuptimes() {
    return setuptimes;
}

int* Instance::getStarttimes() {
    return starttimes;
}

int* Instance::getBatterycost() {
    return batterycosts;
}

int* Instance::getInitialBattery() {
    return initialbattery;
}

/** [lowerbound, upperbound, battery recharge rate]
 */
int* Instance::getMinMaxBattery() {
    return minMaxBattery;
}

int Instance::getVarIndex() {
    return var_index;
}

void Instance::setVarIndex(int index) {
    var_index = index;
}

int Instance::getValIndex() {
    return val_index;
}
/**
 * @return the timelimit of the Instance in seconds
 */
int Instance::getTimelimit() {
    return timelimit;
}

void Instance::setValIndex(int index) {
    val_index = index;
}

void Instance::setFirstOut(char* output) {
    firstOut = output;
}

void Instance::setSecondOut(char* output) {
    secondOut = output;
}

void Instance::setJsFirstOut(char* output) {
    jsFirstOut = output;
}

void Instance::setJsBestOut(char* output) {
    jsBestOut = output;
}

const char* Instance::getFirstOut() {
    return firstOut;
}

const char* Instance::getSecondOut() {
    return secondOut;
}

const char* Instance::getJsFirstOut() {
    return jsFirstOut;
}

const char* Instance::getJsBestOut() {
    return jsBestOut;
}

int* Instance::getVehicleStartTimes() {
    return vehicleStartTimes;
}

int* Instance::getDeadlines() {
    return deadlines;
}

const int Instance::getBatteryResourceAmount() {
    return battery_resources;
}

const int Instance::getMaxRecharge() {
    return maxRechargeTime;
}

const int Instance::getMinBattery() {
    return minMaxBattery[0];
}

const int Instance::getMaxBattery() {
    return minMaxBattery[1];
}




