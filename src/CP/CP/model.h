/* 
 * File:   model.h
 * Author: thor
 *
 * Created on October 8, 2015, 11:42 AM
 */

// This is start of the header guard.
#ifndef MODEL_H
#define	MODEL_H



// This is the content of the .h file, which is where the declarations go

//class Instance;
#include "instance.h"
#include "solution.h"
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <vector>       // std::vector
#include <stdlib.h>     // std::atoi
#include <string>

#include <gecode/driver.hh>
#include <gecode/minimodel.hh>
#include <gecode/int.hh>
#include <gecode/gist.hh>
#include <gecode/search.hh>


using namespace Gecode;
using namespace std;

class Model : public MinimizeScript {
    IntVarArray start;
    IntVarArray vehicles;
    IntVarArray successor;
    IntVarArray battery;
    IntVarArray process;
    IntVar makespan;
    
public :
    Model(const SizeOptions& opt);
    // Print solution
    void print(ostream& os) const;
    // Return cost
    IntVar cost(void) const;
    // Constructor for cloning s
    Model(bool share, Model& s);
    // Copy during cloning
    Space* copy(bool share);
    // read in a insane and output statistics
    static Solution solveInstance(Instance insToRead, 
        const char* bestOut);
    
    // getters for problem instance used in model
    int num_tasks() const;
    int num_machines() const;
    int num_battery() const;
    int total_tasks();
    int max_time() const;
    int total() const;
};

// This is the end of the header guard
#endif	/* MODEL_H */

