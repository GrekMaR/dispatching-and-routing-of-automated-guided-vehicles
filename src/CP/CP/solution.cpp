/* 
 * File:   Solution.cpp
 * Author: thor
 * 
 * Created on October 14, 2015, 1:47 PM
 */

#include "solution.h"
#include <stdlib.h>
#include <string>
#include <sstream>

namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}

using namespace std;

Solution::Solution() {
    didFirstOut = false;
    sjsfout = "";
    sfout = "";
}
Solution::Solution(int* tasks, int* setuptimes, int* processingtimes) {
    didFirstOut = false;
    sjsfout = "";
    sfout = "";
}

void Solution::printSolution() {
    cout << "solution: " << endl;
        for (int i = 0; i < num_machines; i++) {
        for (int j = 0; j < num_tasks; j++) {
            if (vehicles[i].tasks[j].processingTime > -1) {
                cout << "vehicle: " << i << ", task: " << j << ": " 
                        << vehicles[i].tasks[j].label << ", " 
                        << vehicles[i].tasks[j].startTime << ", " 
                        << vehicles[i].tasks[j].processingTime << ", " 
                        << vehicles[i].tasks[j].setupTime << endl;
            }
        }
    }
}

void Solution::outputTikz(ostream& os) {
    if (makespan == 0) {
        return;
    }
    int height = num_machines+1;
    int index = 0;
    double width = 0;
    os << "\\foreach \\r in {0,...," << num_machines-1 << "}\n";
    os << "   \\draw (0,\\r) node[inner sep=1pt,below=3pt,rectangle,fill=white, left=2pt] {$\\r$};\n";
    os << "\\draw[thick,->,black] (0,-1)--(0," << num_machines << ") node[left] {$vehicles$};\n";
    double y = 0;
    for (int i = 0; i < num_machines; i++) {
        double x = 0;
        for (int j = 0; j < tasksPerVehicle[i]; j++) {
            const char* color = "rgb:red,2;green,5;yellow,0";
            const char* label = " ";
            if (vehicles[i].tasks[j+index].processingTime > 0) {
                // Label
                string s = patch::to_string(vehicles[i].tasks[j+index].label);
                label = s.c_str();
                if (vehicles[i].tasks[j+index].label == -1) {
                    // Task is a recharge
                    label = "R"; 
                    color = "rgb:red,2;green,5;yellow,8";
                }
                // use this on CP model
                // and just start times on the greedy one!
                double start = (double) vehicles[i].tasks[j+index].startTime-vehicles[i].tasks[j+index].setupTime;
                // Idle times
                double arrival = start*0.1;
                if (x-arrival < 0) {
                    x = x - (x-arrival);
                } 
                
                // Setup Time
                double oldx = x;
                if (vehicles[i].tasks[j+index].setupTime > 0) {
                    x = x + (double)vehicles[i].tasks[j+index].setupTime*0.1;
                    os << "\\draw[fill=gray] (" << oldx << "," << y-0.20 << ") rectangle (" << x << "," << y+0.20 << ") ";
                    os << "node[below=5pt,left=12.5pt, right=0, text width=5em] {$ $};\n";
                }
                oldx = x;
                x = x + (double)vehicles[i].tasks[j+index].processingTime*0.1;
                os << "\\draw[fill={" << color << "}] (" << oldx << "," << y-0.20 << ") rectangle (" << x << "," << y+0.20 << ") ";
                os << "node[below=5pt,left=12.5pt, right=0, text width=5em] {$" << label << "$};\n";
            }   
        }
        if (setupAtEnd == 1) {
            double oldx = x;
            double setup = setupsToDepot[vehicles[i].tasks[tasksPerVehicle[i]+index-1].label];
            x = x + setup*0.1;
            os << "\\draw[fill=gray] (" << oldx << "," << y-0.20 << ") rectangle (" << x << "," << y+0.20 << ") ";
            os << "node[below=5pt,left=12.5pt, right=0, text width=5em] {$ $};\n";
        }
        index += tasksPerVehicle[i];
        y = y+1;
        if (x > width) {
            width = x;
        }
    }
    os << "\\draw[thick,->,black] (0,-1)--(" << width << ",-1) node[left, below=10pt] {$time$};\n";
    os << "\\foreach \\r in {0, 5,...," << width*10 <<"}\n";
    os << "\\draw (\\r*0.1,-1) node[inner sep=1pt,below=5pt,rectangle,fill=white] {$\\r$};\n";
}

void Solution::outputSolution(ostream& os) {
    if (makespan == 0) {
        return;
    }
    os << elapsed_time << endl;
    os << makespan << endl;
    int index = 0;
    for (int j = 0; j < num_machines; j++) {
        os << "-" << endl;
        for (int i = 0; i < tasksPerVehicle[j]; i++) {
            os << vehicles[j].tasks[i+index].label << " ";
        }
        
        if (vehicles[j].tasks[tasksPerVehicle[j]+index-1].label != -1 && tasksPerVehicle[j] > 0) {
            os << "-1 ";
        }

        os << endl;
        for (int i = 0; i < tasksPerVehicle[j]; i++) {
            os << vehicles[j].tasks[i+index].processingTime << " ";
        }
        if (vehicles[j].tasks[tasksPerVehicle[j]+index-1].label != -1 && tasksPerVehicle[j] > 0) {
            os << "0 ";
        }
        os << endl;
        for (int i = 0; i < tasksPerVehicle[j]; i++) {
            os << vehicles[j].tasks[i+index].setupTime << " ";
        }
        if (vehicles[j].tasks[tasksPerVehicle[j]+index-1].label != -1 && tasksPerVehicle[j] > 0 && setupAtEnd == 1) {
            os << setupsToDepot[vehicles[j].tasks[tasksPerVehicle[j]+index-1].label] << " ";
        } else if (vehicles[j].tasks[tasksPerVehicle[j]+index-1].label != -1 && tasksPerVehicle[j] > 0) {
            os << 0;
            
        }
        os << endl;
        for (int i = 0; i < tasksPerVehicle[j]; i++) {
            os << vehicles[j].tasks[i+index].startTime << " ";
        }
        if (vehicles[j].tasks[tasksPerVehicle[j]+index-1].label != -1 && tasksPerVehicle[j] > 0) {
            // stuff of last task
            int start = vehicles[j].tasks[tasksPerVehicle[j]+index-1].startTime;
            int duration = vehicles[j].tasks[tasksPerVehicle[j]+index-1].processingTime;
            int setup = setupsToDepot[vehicles[j].tasks[tasksPerVehicle[j]+index-1].label];
            if (setupAtEnd == 1) {
                os << start+duration+setup << " ";    
            } else {
                os << start+duration << " ";
            }
        }
        os << endl;
        index += tasksPerVehicle[j];
    }
}


