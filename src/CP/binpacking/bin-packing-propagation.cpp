/*
 *  Authors:
 *    Christian Schulte <schulte@gecode.org>
 *
 *  Copyright:
 *    Christian Schulte, 2008-2015
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */



/*********************************************************************
 ****************************   README   *****************************
 *********************************************************************
This file represents the bin-packing formulation of the RCPSP problem.
It contains predefined formulations of all the problems instances used.
Uncomment the instance to run and compile with the following, replacing $GECODE_HOME with location of gecode installation:
  execute the makefile with "make"
or run:
  g++ -I$GECODE_HOME -c bin-packing-propagation.cpp 
  g++ -std=c++11 -o binpacking -L$GECODE_HOME -Wl,-rpath,$GECODE_HOME bin-packing-propagation.o 
  -lgecodegist -lgecodeint -lgecodesearch -lgecodekernel -lgecodesupport -lgecodedriver 
  -lgecodeminimodel -lgecodeset


And run with:
  ./binpacking
 *********************************************************************/




#include <algorithm>

#include <gecode/driver.hh>
#include <gecode/minimodel.hh>

using namespace Gecode;

/*
Description of arguments:
// c = capacity of the bins
// n = number of elements
// size[n] = elements to pack in bins
*/

/*************************
 Instances:
 *************************/

// 10_5test
/*
const int vehicles = 5; // num vehicles
const int c = 50; // capacity
const int n = 10; // num tasks
const int size[n] = {
50,50,29,29,50,50,29,29,29,29};
*/

// 15_5test
/*const int vehicles = 5; // num vehicles
const int c = 50; // capacity
const int n = 15; // num tasks
const int size[n] = {
42,40,40,40,42,40,40,40,42,42,40,42,42,40,42};
*/

// 20_5test
/*const int vehicles = 5; // num vehicles
const int c = 50; // capacity
const int n = 20; // num tasks
const int size[n] = {
50,21,50,21,21,21,50,21,50,50,21,21,50,21,50,50,21,50,50,50};
*/

// 25_5test
/*const int vehicles = 5; // num vehicles
const int c = 50; // capacity
const int n = 25; // num tasks
const int size[n] = {
43,42,42,43,43,43,42,43,43,43,43,43,42,42,43,42,43,42,42,42,42,43,43,43,42};
*/

// 15_10test
/*const int vehicles = 10; // num vehicles
const int c = 50; // capacity
const int n = 15; // num tasks
const int size[n] = {
43,43,47,47,47,47,43,47,47,47,47,43,43,43,43};
*/

// 20_10test
/*const int vehicles = 10; // num vehicles
const int c = 50; // capacity
const int n = 20; // num tasks
const int size[n] = {
35,21,21,35,35,35,21,35,21,35,21,21,35,35,21,21,21,21,35,35};
*/

// 25_10test
/*
const int vehicles = 10; // num vehicles
const int c = 50; // capacity
const int n = 25; // num tasks
const int size[n] = {
28,38,38,38,28,28,38,28,28,38,38,38,38,38,28,28,38,28,38,28,28,38,38,38,28};
*/

// compute lower bound
int lower(void) {
  int s=0;
  for (int i=0; i<n; i++)
    s += size[i];
  return (s + c - 1) / c;
}
// compute upper bound
int upper(void) {
  int* free = new int[n];
  // initialize free capacity
  for (int i=0; i<n; i++)
    free[i] = c;
  int u=0;
  // pack items into free bins
  for (int i=0; i<n; i++) {
    int j=0;
    // find free bin
    while (free[j] < size[i])
      j++;
    free[j] -= size[i];
    u = std::max(u,j);
  }
  delete [] free;
  return u+1;
}
class BinPacking : public IntMinimizeScript {
protected:
  const int l;
  const int u;  
  IntVarArray load;
  IntVarArray bin;
  IntVar bins;
public:
  BinPacking(const Options& opt) 
    : IntMinimizeScript(opt),
      l(lower()), u(upper()),
      load(*this, u, 0, c), 
      bin(*this, n, 0, u-1), bins(*this, l, u) {
    // excess bins
    for (int i=1; i <= u; i++)
      rel(*this, (bins < i) == (load[i-1] == 0));
    IntArgs sizes(n, size);
    binpacking(*this, load, bin, sizes);
    // symmetry breaking
    for (int i=1; i<n; i++)
      if (size[i-1] == size[i])
        rel(*this, bin[i-1] <= bin[i]);
    // pack items that require a bin
    for (int i=0; (i < n) && (i < u) && (size[i] * 2 > c); i++)
      rel(*this, bin[i] == i);
    // branching
    branch(*this, bins, INT_VAL_MIN());
    branch(*this, bin, INT_VAR_NONE(), INT_VAL_MIN());
  }
  // Cost function
  virtual IntVar cost(void) const {
    return bins;
  }
  // Constructor for cloning s
  BinPacking(bool share, BinPacking& s) 
    : IntMinimizeScript(share,s), l(s.l), u(s.u) {
    load.update(*this, share, s.load);
    bin.update(*this, share, s.bin);
    bins.update(*this, share, s.bins);
  }
  // Copy during cloning
  virtual Space* copy(bool share) {
    return new BinPacking(share,*this);
  }
  // Print solution
  virtual void print(std::ostream& os) const {
    os << "Bins used: " << bins << " (from " << u << " bins)." << std::endl;
    os << "vehicles available: " << vehicles << ", so recharges needed are: ";
    if (bins.val()-vehicles > 0) {
      os << (bins.val()-vehicles) << std::endl;
    } else {
      os << 0 << std::endl;
    }

    for (int j=0; j<u; j++) {
      bool fst = true;
      os << "\t[" << j << "]={";
      for (int i=0; i<n; i++)
        if (bin[i].assigned() && (bin[i].val() == j)) {
          if (fst) {
            fst = false;
          } else {
            os << ",";
          }
          os << i;
        }
      os << "} #" << load[j] << std::endl;
    }
    if (!bin.assigned()) {
      os << std::endl 
         << "Unpacked items:" << std::endl;
      for (int i=0;i<n; i++)
        if (!bin[i].assigned())
          os << "\t[" << i << "] = " << bin[i] << std::endl;
    }
  }
};
int main(int argc, char* argv[]) {
  Options opt("BinPacking");
  opt.solutions(0);
  opt.parse(argc,argv);
  IntMinimizeScript::run<BinPacking,BAB,Options>(opt);
  return 0;
}
